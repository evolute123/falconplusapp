package com.eps.falconplus.epsfpcustomerapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class AcceptTerms extends AppCompatActivity{

    ImageView ivExit;
    Button btnAccept,btnReject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_terms);


        ivExit = (ImageView) findViewById(R.id.ivExit);
        if (ivExit != null)
            ivExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    exitScreen("");
                }
            });

        btnReject = (Button) findViewById(R.id.btnReject);
        if (btnReject != null)
            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    exitScreen(getResources().getString(R.string.Accept_Reject_Button_Message));
                }
            });
        btnAccept = (Button) findViewById(R.id.btnAccept);
        if (btnAccept != null)
            btnAccept.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = pref.edit();
                edit.putBoolean("AcceptTerms",true);
                edit.apply();
                startActivity(new Intent(AcceptTerms.this,PhoneSetup.class));
                overridePendingTransition( R.anim.left_in, R.anim.left_out);
                finish();
            }
        });
    }@Override
    public void onBackPressed() {
        exitScreen("");
    }

    private void exitScreen(String message){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(AcceptTerms.this);
        builder.setMessage((message.isEmpty() ? getResources().getString(R.string.Accept_Exit_Button) : message)).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
