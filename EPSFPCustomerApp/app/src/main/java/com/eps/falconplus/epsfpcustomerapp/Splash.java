package com.eps.falconplus.epsfpcustomerapp;

import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.os.Handler;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class Splash extends AppCompatActivity {


    private boolean _active = true;
    private int _splashTime = 2000;
    private static final  int	SPLASH_THREAD=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        /**
         * Message to the Handler
         */
        Message mMessage = new Message();
        mMessage.what =Splash.SPLASH_THREAD;
        Splash.this.handlersplash.sendMessage(mMessage);
    }

    /**
     * Splash thread
     */

    Thread splashTread = new Thread() {
        @Override
        public void run() {
            try {
                int waited = 0;
                while(_active && (waited < _splashTime)) {
                    sleep(100);
                    if(_active) {
                        waited += 100;

                    }
                }
            } catch(InterruptedException e) {
                e.printStackTrace();
            }finally {

                SharedPreferences pref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);
                {
                    if(!pref.getBoolean("AcceptTerms",false))
                    {
                        startActivity(new Intent(Splash.this,AcceptTerms.class));
                        overridePendingTransition( R.anim.left_in, R.anim.left_out);
                    }
                    else if(pref.getString("Phone","").length()==0)
                    {
                        startActivity(new Intent(Splash.this,PhoneSetup.class));
                        overridePendingTransition( R.anim.left_in, R.anim.left_out);
                    }else if(pref.getString("BankCodes","").length()==0){
                        Intent in = new Intent(Splash.this,BankSetup.class);
                        in.putExtra("SETUP",true);
                        in.putExtra("showType","select");
                        startActivity(in);
                        overridePendingTransition( R.anim.left_in, R.anim.left_out);
                    }else{
                        startActivity(new Intent(Splash.this,Transaction.class));
                    }
                    finish();

		/*	startActivity(new Intent(Splash.this, MainActivity.class));
			finish();*/ }

        }
        }
    };

    /**
     * Splash Handler
     */
    Handler handlersplash = new Handler() {

        public void handleMessage(android.os.Message msg) {

            switch (msg.what) {
                case SPLASH_THREAD:
                    splashTread.start();
                    break;
                default:
                    break;
            }
        };
    };
}
