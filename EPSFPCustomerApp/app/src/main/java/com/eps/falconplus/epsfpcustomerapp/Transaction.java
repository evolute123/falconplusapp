package com.eps.falconplus.epsfpcustomerapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eps.falconplus.Bluetooth.BluetoothComm;
import com.eps.falconplus.Bluetooth.BluetoothPair;
import com.eps.falconplus.Bluetooth.FalconDev;
import com.eps.falconplus.Bluetooth.HexString;
import com.eps.falconplus.server.EPSConstant;
import com.eps.falconplus.server.EPSServerCaller;
import com.eps.falconplus.server.EPSWebURL;
import com.eps.falconplus.server.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class Transaction extends AppCompatActivity implements View.OnClickListener {

    //UI Elements
    ImageView ivSettings, ivExit;
    TextView tvBlueTooth, tvDisplayName, tvAmount, tvOldTrans, tvTimer;
    Button btnSubmit, btnMakePayment,btnCash, btnDecline, btnReSubmit, btnSetTerminalId;
    //Button btnMakePaymentIFSC;
    RelativeLayout rlMakePayment, rlDisplayTransaction, rlOldTrans;

    boolean bUSSDTransactionStarted = false;
    boolean bUSSDWaitingReady = false;
    //Bluetooth Elements
    private static final String TAG = "EPS";

    public static String DEVICE_MAC = "";
    static BluetoothAdapter mBtAdapter;
    BluetoothComm btcomm;
    private boolean mbDiscoveryFinished = false;
    private boolean mbDeviceFound = false;
    private boolean mbBonded = false;
    int iRetVal = 0;
    private static int iDevFoundCnt = 1;
    private String mbPaymentType = "";

    //Transaction Related Fields
    boolean mOldTransFound = false;
    boolean ismTransCancelled = false;
    CountDownTimer cdTimer;
    long lcdTimerTotal;
    EPSConstant.TransactionState mTransState;
    String mSMSDetails = "";
    CancelThreadMonitor monitorThread;
    Handler mSmsHandler = new Handler();
    private static final int MAKE_PAYMENT_DIALOG_CTRL = -987;
    String mLastUsedTransactionId = "";
    String mTransDate = "";
    String mInvoiceNo = "";
    String mAmount = "";
    String mTransStatus = "";
    String mTID = "";
    String mMerchantID = "";
    String mMerchantName = "";
    String mBankCode = "";
    String mTransID = "";
    SharedPreferences mPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        ismTransCancelled = false;

        setContentView(R.layout.activity_transaction);

        //Setup Text Views
        tvBlueTooth = (TextView) findViewById(R.id.tvBluetooth);
        tvDisplayName = (TextView) findViewById(R.id.tvDisplayName);
        tvAmount = (TextView) findViewById(R.id.tvAmount);
        tvOldTrans = (TextView) findViewById(R.id.tvOldTrans);
        tvTimer = (TextView) findViewById(R.id.tvTimer);

        //Setup Buttons
        ivSettings = (ImageView) findViewById(R.id.ivSettings);
        if (ivSettings != null)
            ivSettings.setOnClickListener(this);

        ivExit = (ImageView) findViewById(R.id.ivExit);
        if (ivExit != null)
            ivExit.setOnClickListener(this);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        if (btnSubmit != null)
            btnSubmit.setOnClickListener(this);

        btnReSubmit = (Button) findViewById(R.id.btnReSubmit);
        if (btnReSubmit != null)
            btnReSubmit.setOnClickListener(this);

        btnMakePayment = (Button) findViewById(R.id.btnMakePayment);
        if (btnMakePayment != null)
            btnMakePayment.setOnClickListener(this);

//        btnMakePaymentIFSC = (Button) findViewById(R.id.btnMakePaymentIFSC);
//        if (btnMakePaymentIFSC != null)
//            btnMakePaymentIFSC.setOnClickListener(this);

        btnCash = (Button) findViewById(R.id.btnCash);
        if (btnCash != null)
            btnCash.setOnClickListener(this);

        btnDecline = (Button) findViewById(R.id.btnDecline);
        if (btnDecline != null)
            btnDecline.setOnClickListener(this);

        //Temporary field to test various TIDs
        if (btnSetTerminalId != null)
            btnSetTerminalId.setOnClickListener(this);

        //Setup Layouts
        rlMakePayment = (RelativeLayout) findViewById(R.id.rlMakePayment);
        rlDisplayTransaction = (RelativeLayout) findViewById(R.id.rlDisplayTransaction);
        rlOldTrans = (RelativeLayout) findViewById(R.id.rlOldTrans);
        displayLayouts("");

        mTransState = EPSConstant.TransactionState.InitialState;
        mOldTransFound = false;
        mPref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);

    }

    @Override
    public void onBackPressed() {
       exitScreen();
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkOldTransaction();
//        if (!mOldTransFound && mTransState == EPSConstant.TransactionState.InitialState) {
//            connectBT();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TRANSACTION","ON RESUME " + bUSSDTransactionStarted + " - " + bUSSDWaitingReady);
        if(bUSSDTransactionStarted && bUSSDWaitingReady && mTransState == EPSConstant.TransactionState.USSDStarted)
        {
            mTransState = EPSConstant.TransactionState.USSDFinished;
            bUSSDTransactionStarted =false;
            bUSSDWaitingReady=false;
            //new ATSmsCheck().execute();
        }
    }

    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        bUSSDWaitingReady = (bUSSDTransactionStarted && mTransState == EPSConstant.TransactionState.USSDStarted);
        Log.i("TRANSACTION","ON PAUSE CALLED " + bUSSDWaitingReady);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mTransState == EPSConstant.TransactionState.InitialState) {
            disconnectBTNoTrans("");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTransState == EPSConstant.TransactionState.InitialState) {
            disconnectBTNoTrans("");
        }
    }

    @Override
    public void onClick(View view) {
        mbPaymentType = "";
        if (view == ivSettings) {
            if (btcomm == null)
                startActivity(new Intent(Transaction.this, Settings.class));
        } else if (view == ivExit) {
            exitScreen();
        }
        else if (view == btnSubmit) {
            mTransState = EPSConstant.TransactionState.InitialState;
            checkOldTransaction();
            if (!mOldTransFound)
                connectBT();
        } else if (view == btnReSubmit) {
            checkOldTransaction();
        } else if (view == btnDecline) {
            tvTimer.setVisibility(View.INVISIBLE);
            if (cdTimer != null)
                cdTimer.cancel();
            mTransState = EPSConstant.TransactionState.TransactionDeclined;
            Log.e(TAG, "DeclineButtonOnClickListener:Value of mTransState: " + mTransState);
            SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String transactionDate = format.format(date);
            new ATUpdateTransaction(tpref.getString("transactionID", ""), (ismTransCancelled ? "cancelled" : "declined"), "", transactionDate,"IMPS", false, false).execute();
            if (btcomm.isMerchantCancelationMonitorActive())
                btcomm.vSTOP_MerchantCancelMonitor();
        }else if(view == btnCash)
        {
            tvTimer.setVisibility(View.INVISIBLE);
            if (cdTimer != null)
                cdTimer.cancel();
            mTransState = EPSConstant.TransactionState.CashTransaction;
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String transactionDate = format.format(date);
            SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = tpref.edit();
            edit.putString("transactionType", "CASH");
            String transID = tpref.getString("transactionID", "");
            String td = getResources().getString(R.string.Trans_Cash_Trans_Compete);
            String ts = "approved";
            edit.putString("paymentTimestamp", transactionDate);
            edit.putString("transactionDetails", td);
            edit.putString("transactionStatus", ts);
            edit.apply();
            new ATUpdateTransaction(transID,ts,td, transactionDate,"CASH", false, false).execute();
            if (btcomm.isMerchantCancelationMonitorActive())
                btcomm.vSTOP_MerchantCancelMonitor();
        } else if (view == btnMakePayment) {
            //if(view==btnMakePayment)
                mbPaymentType = "MMID";
            //else
            //    mbPaymentType = "IFSC";
            tvTimer.setVisibility(View.INVISIBLE);
            if (cdTimer != null)
                cdTimer.cancel();
            SharedPreferences cpref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);
            String bankid = cpref.getString("BankCodes", "");
            String bankacc = cpref.getString("BankAccs","");
            mTransState = EPSConstant.TransactionState.USSDStarted;
            if (bankid.contains(",")) {
                selectBank();
            } else {
                Log.e(TAG, "btnMakePaymentOnClickListener:Value of mTransState: " + mTransState);
                executeUSSD(bankid,bankacc);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Get Result from USSD
        bUSSDTransactionStarted = (requestCode == 121);
        Log.i("TRANSACTION","ON ACTIVITY RESULT " + requestCode);
    }

    //Private Methods

    private void exitScreen()
    {
        if (rlDisplayTransaction.getVisibility() == View.GONE) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            btdisconnector();
                            finish();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(Transaction.this);
            builder.setMessage(getResources().getString(R.string.Trans_Exit_Button)).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        } else {
            Utility.alert(getResources().getString(R.string.Trans_Exit_when_transaction_in_progress_head),getResources().getString(R.string.Trans_Exit_when_transaction_in_progress),Transaction.this);
        }
    }

    private void checkOldTransaction() {
        SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
        String transID = tpref.getString("transactionID", "");
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String transactionDate = format.format(date);
        Boolean bflag = transID.isEmpty();
        if (bflag) {
            //All Clear... We can go ahead for a new transaction;
            tvBlueTooth.setText("Search Device");
            displayLayouts("");
        } else if (rlDisplayTransaction.getVisibility() == View.GONE) {
            mOldTransFound = true;
            mLastUsedTransactionId = transID;
            if (Utility.isNetWorkAvailable(Transaction.this)) {
                String pt = tpref.getString("paymentTimestamp", "");
                String td = tpref.getString("transactionDetails", "");
                String ts = tpref.getString("transactionStatus", "");
                String tt = tpref.getString("transactionType","IMPS");
                if (ts.equalsIgnoreCase("approved")) {
                    new ATUpdateTransaction(transID, ts, td, pt,tt, true, true).execute();
                } else if (ts.isEmpty()) {
                    disconnectBTNoTrans(getResources().getString(R.string.Trans_Default_Trans_message));
                } else if (ts.equalsIgnoreCase("pending")) {
                    new ATUpdateTransaction(transID, "declined", "", transactionDate,tt, true, true).execute();
                } else {
                    new ATUpdateTransaction(transID, ts, "", transactionDate,tt, true, true).execute();
                }
                //tvOldTrans.setText(getResources().getString(R.string.Trans_Old_Trans_found));
                displayLayouts("old");
            } else {
                Utility.alert(getResources().getString(R.string.Trans_Internet_Failure_error_head),getResources().getString(R.string.Trans_Internet_Failure_error),Transaction.this);
            }
        }
    }

    private void selectBank() {
        final Dialog dialog = new Dialog(Transaction.this);
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.grid_view);
        dialog.setTitle(getResources().getString(R.string.Trans_USSD_Popup_for_multi_banks_title));
        // Prepare grid view
        GridView gridView = (GridView) dialog.findViewById(R.id.gridView);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        TextView tv = (TextView)dialog.findViewById(R.id.tvNote);
        tv.setText("");
        SharedPreferences pref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);
        final String pbankCodes = pref.getString("BankCodes", "");
        final String pbankAccs = pref.getString("BankAccs","");
        ImageAdapter adapter = new ImageAdapter(this, pbankCodes,"popup");
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                // adapter.onItemSelect(parent, v, position, id);
                String[] bankids = pbankCodes.split(",");
                String[] bankaccs = pbankAccs.split(",");
                executeUSSD(bankids[position],bankaccs[position]);
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (cdTimer != null) {
                    startTimer();
                }
                tvTimer.setVisibility(View.VISIBLE);
                mTransState = EPSConstant.TransactionState.WaitingForMakePayment;
            }
        });
        dialog.show();
    }

    private void displayLayouts(String rl) {
        rlDisplayTransaction.setVisibility(View.GONE);
        rlMakePayment.setVisibility(View.GONE);
        rlOldTrans.setVisibility(View.GONE);
        ivSettings.setVisibility(View.GONE);
        ivExit.setVisibility(View.GONE);
        if (rl.equalsIgnoreCase("old"))
            rlOldTrans.setVisibility(View.VISIBLE);
        else {
            if (rl.equalsIgnoreCase("display")) {
                rlDisplayTransaction.setVisibility(View.VISIBLE);
                tvTimer.setVisibility(View.VISIBLE);
                lcdTimerTotal = EPSConstant.APPLICATION_TRANSACTION_TIMEOUT * 1000;
                startTimer();
                mTransState = EPSConstant.TransactionState.WaitingForMakePayment;
            } else {
                rlMakePayment.setVisibility(View.VISIBLE);
                ivSettings.setVisibility(View.VISIBLE);
                ivExit.setVisibility(View.VISIBLE);
            }
        }
    }

    private void startTimer() {
        cdTimer = new CountDownTimer(lcdTimerTotal, 1000) {
            @Override
            public void onFinish() {
                if (btcomm.isMerchantCancelationMonitorActive())
                    btcomm.vSTOP_MerchantCancelMonitor();
                SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
                Date date = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String transactionDate = format.format(date);
                new ATUpdateTransaction(tpref.getString("transactionID", ""), "declined", "", transactionDate,"IMPS", false, false).execute();
            }

            @Override
            public void onTick(long l) {
                if (ismTransCancelled) {
                    cdTimer.cancel();
                } else {
                    lcdTimerTotal = l;
                    int i = (int) (l / 1000);
                    if (i == 1)
                        tvTimer.setText(getResources().getString(R.string.Trans_Timer_Complete));
                    else if (i < 11)
                        tvTimer.setText(getResources().getString(R.string.Trans_Timer_Progress1) + (i - 1));
                    else
                        tvTimer.setText(getResources().getString(R.string.Trans_Timer_Progress2) + (i - 1));
                }
            }
        }.start();
    }

    private void executeUSSD(String bankid, String accno) {
        final String fbankid = bankid;
        final String faccno = accno;

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:

                        break;
                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };
        final Dialog dlgMPIN = new Dialog(Transaction.this);
        dlgMPIN.setCanceledOnTouchOutside(true);
        dlgMPIN.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgMPIN.setContentView(R.layout.mpin_view);
        //dlgMPIN.setTitle(getResources().getString(R.string.Trans_USSD_Popup_for_multi_banks_title));
        // Prepare grid view
        final EditText etMPIN = (EditText) dlgMPIN.findViewById(R.id.etMPIN);
        Button btnOK = (Button) dlgMPIN.findViewById(R.id.btnOK);
        Button btnCancel = (Button) dlgMPIN.findViewById(R.id.btnCancel);
        TextView tv = (TextView)dlgMPIN.findViewById(R.id.tvBankName);
        ImageView iv = (ImageView) dlgMPIN.findViewById(R.id.ivBankLogo);

        tv.setText(EPSConstant.getBankName(bankid));
        iv.setImageResource(EPSConstant.getBankLogo(bankid));

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mpin = etMPIN.getText().toString();
                if(mpin.isEmpty()) {
                    Utility.alert("MPIN Error","Please enter your MPIN associated to your bank",Transaction.this);
                }else{
                    dlgMPIN.dismiss();
                    mTransState = EPSConstant.TransactionState.USSDStarted;
                    Transaction.this.mBankCode = fbankid;
                    Log.e(TAG, "DeclineButtonOnClickListener:Value of mTransState: " + mTransState);
                    SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
                    String ussd = "";
                    String amt = Transaction.this.mAmount;
                    if (mbPaymentType.equals("MMID"))
                        ussd = "*99*" + fbankid + "*3*" + tpref.getString("phoneNumber", "") + "*" + tpref.getString("mmid", "") + "*" + amt + "*0*" + mpin + "*" + faccno + "#";
                    else
                        ussd = "*99*" + fbankid + "*4*" + tpref.getString("bankIFSC", "") + "*" + tpref.getString("bankAccount", "") + "*" + amt + "#";
                    String uriString = "";
                    if (!ussd.startsWith("tel:"))
                        uriString += "tel:";
                    for (char c : ussd.toCharArray()) {
                        if (c == '#')
                            uriString += Uri.encode("#");
                        else
                            uriString += c;
                    }

                    //Utility.alert("USSD STRING",uriString,Transaction.this);
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uriString));
                    startActivityForResult(callIntent, 121);

                    //Start Listener
                    new ATSmsCheck().execute();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DialogInterface dlg = dlgMPIN;
                DialogInterface.OnClickListener dialogClickListener2 = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                if (cdTimer != null) {
                                    startTimer();
                                }
                                tvTimer.setVisibility(View.VISIBLE);
                                mTransState = EPSConstant.TransactionState.WaitingForMakePayment;
                                dialog.dismiss();
                                dlgMPIN.dismiss();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(Transaction.this);
                builder.setMessage("Are you sure you want to cancel the Transaction?").setPositiveButton("OK", dialogClickListener2)
                        .setNegativeButton("Cancel", dialogClickListener2).show();
            }
        });
        dlgMPIN.show();


//        String mpin = "1223";
//        mTransState = EPSConstant.TransactionState.USSDStarted;
//        Transaction.this.mBankCode = fbankid;
//        Log.e(TAG, "DeclineButtonOnClickListener:Value of mTransState: " + mTransState);
//        SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
//        String ussd = "";
//        String amt = Transaction.this.mAmount;
//        if (mbPaymentType.equals("MMID"))
//            ussd = "*99*" + fbankid + "*3*" + tpref.getString("phoneNumber", "") + "*" + tpref.getString("mmid", "") + "*" + amt + "*spirituspay*" + mpin + "*" + faccno + "#";
//        else
//            ussd = "*99*" + fbankid + "*4*" + tpref.getString("bankIFSC", "") + "*" + tpref.getString("bankAccount", "") + "*" + amt + "#";
//        String uriString = "";
//        if (!ussd.startsWith("tel:"))
//            uriString += "tel:";
//        for (char c : ussd.toCharArray()) {
//            if (c == '#')
//                uriString += Uri.encode("#");
//            else
//                uriString += c;
//        }
//
//        //Utility.alert("USSD STRING",uriString,Transaction.this);
//        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uriString));
//        startActivityForResult(callIntent, 121);

    }

    private void printMessage(String message){
        int iRet;
        if (btcomm!=null && btcomm.isConnect()){
            iRet = btcomm.iFlushPrinterBuffer();

            String sDt10 = "";
            String sTm08 = "";
            if(!Transaction.this.mTransDate.isEmpty()) {
                sDt10 = Transaction.this.mTransDate.split(" ")[0];
                String[] sdts = sDt10.split("-");
                sDt10 = sdts[2] + "-"+ sdts[1] + "-" + sdts[0];
                sTm08 = Transaction.this.mTransDate.split(" ")[1];
            }
            //// Please NOTE all LARGE_xxx Fonts are 24 chars per line and all SMALL_xxx Fonts are 42 chars per line
            //////////								 12345678901234567890123x567890123456789012
            if(!Transaction.this.mBankCode.isEmpty()) {
                String bankName = "";

                List<String> bcs = Arrays.asList(EPSConstant.mBankCodes);
                int item = bcs.indexOf(Transaction.this.mBankCode);
                bankName = EPSConstant.mBankNames[item];
                printc(FalconDev.Font.LARGE_BOLD, bankName);//Consumer Bank Name
            }
            if(!Transaction.this.mMerchantName.isEmpty())
                printc(FalconDev.Font.LARGE_NORMAL, Transaction.this.mMerchantName);//Merchant Display Name
            printc(FalconDev.Font.LARGE_NORMAL, "SALE");
            if(!sDt10.isEmpty())
                sDt10 = "DATE:"+sDt10;
            if(!sTm08.isEmpty())
                sTm08 = "TIME:"+sTm08;
            if(!sDt10.isEmpty())
                print(FalconDev.Font.SMALL_NORMAL, sDt10,sTm08  );
            else if(sDt10.isEmpty() && !sTm08.isEmpty())
                print(FalconDev.Font.SMALL_NORMAL,sTm08  );

            String mid = "";
            if(!Transaction.this.mMerchantID.isEmpty())
                mid = "MID:"+Transaction.this.mMerchantID;
            String tid = "";
            if(!Transaction.this.mTID.isEmpty())
                tid = "TID:"+Transaction.this.mTID;
            if(!mid.isEmpty())
                print(FalconDev.Font.SMALL_NORMAL, mid,tid);//Merchant ID and Terminal ID
            else if(mid.isEmpty() && !tid.isEmpty())
                print(FalconDev.Font.SMALL_NORMAL,tid);

            String RRN = "";

            if(Transaction.this.mTransStatus.equalsIgnoreCase("approved"))
            {
                //String testmsg = "Your a/c no. XXXXXXXXXX8103 is debited for Rs. 13.00 on 17-08-2016 and a/c linked to mobile 9886203255 credited (IMPS Ref no. 6021137830242).";
                String[] split = message.toLowerCase().split("imps ref no");
                if(split.length == 2 )
                {
                    RRN = split[1];
                    RRN = RRN.replace(")","").replace(".","");
                    RRN = RRN.trim();
                    RRN = "RRN:"+RRN;
                }
            }
            String invnum = "";
            if(!Transaction.this.mInvoiceNo.isEmpty())
                invnum = "INV NUM:"+Transaction.this.mInvoiceNo;
            if(!invnum.isEmpty())
                print(FalconDev.Font.SMALL_NORMAL, invnum,RRN);
            else if(invnum.isEmpty()&&!RRN.isEmpty())
                print(FalconDev.Font.SMALL_NORMAL,RRN);
            if(!mTransID.isEmpty())
                print(FalconDev.Font.SMALL_NORMAL, "TXN ID:",mTransID);
            mTransID="";
            print(FalconDev.Font.SMALL_NORMAL, "                                          ");
            if(!Transaction.this.mTransStatus.isEmpty())
                print(FalconDev.Font.LARGE_NORMAL, "TXN STATUS:",Transaction.this.mTransStatus.toUpperCase());//or DECLINED
            print(FalconDev.Font.SMALL_NORMAL, "                                          ");
            String amt = Transaction.this.mAmount;
            if(!amt.isEmpty())
                print(FalconDev.Font.LARGE_BOLD,   "AMOUNT : RS",amt+".00");
            print(FalconDev.Font.SMALL_NORMAL, "                                          ");
            // No need to worry on formating the SMS for printing as it must be printed as it is...
            if (message.toLowerCase().contains("imps ref no")) {
                print(FalconDev.Font.SMALL_NORMAL, message);
                print(FalconDev.Font.SMALL_NORMAL, "                                          ");
                print(FalconDev.Font.LARGE_NORMAL, "     PIN VERIFIED OK    ");
                print(FalconDev.Font.LARGE_NORMAL, " SIGNATURE NOT REQUIRED ");
                print(FalconDev.Font.SMALL_NORMAL, "  I AGREE TO PAY AS PER BANK'S AGREEMENT  ");
            }
            else
            {
                print(FalconDev.Font.LARGE_NORMAL, message);
            }
            print(FalconDev.Font.LARGE_NORMAL, "        THANK YOU       ");
            print(FalconDev.Font.LARGE_NORMAL, "  *** CUSTOMER COPY *** ");
            print(FalconDev.Font.LARGE_BOLD,"\n\n\n");
            iRet = btcomm.iPrintData();
            Log.i(TAG, "Resp iDiagnose : "+sCheckReturnCode(iRet));
        }else{
            Log.e(TAG, "btnTestCmd Some Bug.... ");
        }
    }
    private void print(FalconDev.Font font,String s)
    {
        int limit=24;
        if(font == FalconDev.Font.SMALL_NORMAL)
            limit = 42;
        int index = 0;
        while (index < s.length()) {
            btcomm.iAddPrintData(font,s.substring(index, Math.min(index + limit,s.length())));
            index += limit;
        }
    }
    private void print(FalconDev.Font font,String s1,String s2)
    {
        int limit=24;
        if(font == FalconDev.Font.SMALL_NORMAL)
            limit = 42;
        int totlen = s1.length() + s2.length();
        int space = limit-totlen;
        StringBuffer outputBuffer = new StringBuffer(space);
        for (int i = 0; i < space; i++){
            outputBuffer.append(" ");
        }
        btcomm.iAddPrintData(font,s1+outputBuffer.toString()+s2);
    }
    private void printc(FalconDev.Font font,String s){
        int limit=24;
        if(font == FalconDev.Font.SMALL_NORMAL)
            limit = 42;
        int index = 0;
        int len = s.length();
        if(len <= limit)
        {
            int space = Math.round((limit-len)/2);
            StringBuffer outputBuffer = new StringBuffer(space);
            for (int i = 0; i < space; i++){
                outputBuffer.append(" ");
            }
            String prntxt = outputBuffer.toString()+s+outputBuffer.toString();
            btcomm.iAddPrintData(font,prntxt);
        }
        else {
            String[] words = s.split(" ");
            ArrayList<String> lines = new ArrayList<String>();
            String line = "";
            for(String word:words)
            {
                String tmpline = line + " " + word;
                if(tmpline.length() > limit)
                {
                    lines.add(line);
                    line = word;
                }
                else
                {
                    line = tmpline;
                }
            }
            lines.add(line);
            for(String l : lines)
            {
                int space = Math.round((limit-l.length())/2);
                StringBuffer outputBuffer = new StringBuffer(space);
                for (int i = 0; i < space; i++){
                    outputBuffer.append(" ");
                }
                String prntxt = outputBuffer.toString()+l+outputBuffer.toString();
                btcomm.iAddPrintData(font,prntxt);
            }
//            while (index < len) {
//                if ((len-index)>limit) {
//                    String prntxt = s.substring(index, index + limit);
//                    btcomm.iAddPrintData(font, prntxt);
//                }else {
//                    int space = (limit-(len-index))/2;
//                    StringBuffer outputBuffer = new StringBuffer(space);
//                    for (int i = 0; i < space; i++) {
//                        outputBuffer.append(" ");
//                    }
//                    String prntxt = outputBuffer.toString() + s.substring(index,s.length()) + outputBuffer.toString();
//                    btcomm.iAddPrintData(font,prntxt);
//                }
//                index += limit;
//            }
        }
    }


    private void printMessageOld(String message) {
        String ptrnmsg = "\n\n";
        if(!Transaction.this.mTransDate.isEmpty()) {
            ptrnmsg = ptrnmsg + "DATE: " + Transaction.this.mTransDate.split(" ")[0] + "\n";
            ptrnmsg = ptrnmsg + "TIME: " + Transaction.this.mTransDate.split(" ")[1] + "\n";
        }
        ptrnmsg = ptrnmsg +"INV NO.: "+ Transaction.this.mInvoiceNo+"\n";
        String rupee = getResources().getString(R.string.Trans_Rs_Code);
        String amt = Transaction.this.mAmount;
        if(amt.isEmpty())
            amt="0";
        ptrnmsg = ptrnmsg +"AMOUNT: Rs."+amt+"\n";
        ptrnmsg = ptrnmsg +"STATUS: "+Transaction.this.mTransStatus.toUpperCase()+"\n";
        ptrnmsg = ptrnmsg +"MESSAGE: \n"+message+"\n\n";
        ptrnmsg = ptrnmsg +"-----------------------\n";
        if(Transaction.this.mTransStatus.equalsIgnoreCase("approved")) {
            ptrnmsg = ptrnmsg + "I AGREE TO PAY THE ABOVETOTAL AMOUNT ACCORDING       TO AGREEMENT\n";
        }
        ptrnmsg = ptrnmsg + "        Thank You\n";
        ptrnmsg = ptrnmsg +"-----------------------\n";
        //int x = btcomm.iPrintData(FalconDev.Font.LARGE_NORMAL, ptrnmsg + "\n\n\n\n\n\n");
        Transaction.this.mTransDate="";
        Transaction.this.mAmount="";
        Transaction.this.mInvoiceNo="";
   //     Log.v("Transaction Print : ", "" + x);
    }

    private void disconnectBTNoTrans(String message) {
        //Update UI
//        tvBlueTooth.setText(message);
        tvBlueTooth.setText("");
        if (!message.isEmpty())
            Utility.alert("", message, Transaction.this);
        displayLayouts("");
        if (btcomm != null) {
            if (btcomm.isMerchantCancelationMonitorActive())
                btcomm.vSTOP_MerchantCancelMonitor();
            btcomm.iSendTransactionStatus(FalconDev.Trans_Stat.Declined, "".getBytes());
            printMessage(message);
        }
       // btdisconnector();
    }

    private void btdisconnector()
    {
        if(btcomm != null)
        {
            btcomm.closeConn();
            btcomm = null;
        }
        try {
            BluetoothDevice tmpBDevice = mBtAdapter.getRemoteDevice(DEVICE_MAC);
            Log.e(TAG, "Resp iSendTransactionStatus : " + sCheckReturnCode(iRetVal));
            if (tmpBDevice != null && mbDeviceFound) {
                BluetoothPair.removeBond(tmpBDevice);
            }
        } catch (Exception e) {
            Log.e(TAG, "btnUnPair Exception.....");
            e.printStackTrace();
        }
    }

    private void disconnectBT(String message, FalconDev.Trans_Stat tStat) {
        //Update UI
//        tvBlueTooth.setText(message);
        tvBlueTooth.setText("");
        if (!message.isEmpty())
            Utility.alert("", message, Transaction.this);
        displayLayouts("");
        SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
        String transactionID = tpref.getString("transactionID", "");
        mTransID = "";
        if (btcomm != null) {
            if (btcomm.isMerchantCancelationMonitorActive())
                btcomm.vSTOP_MerchantCancelMonitor();
            String tid = "";
            if (!mLastUsedTransactionId.isEmpty()) {
                tid = mLastUsedTransactionId;
                mLastUsedTransactionId = "";
            } else if (!transactionID.isEmpty()) {
                tid = transactionID;
            }
            mTransID = tid;
            if (tStat != null) {
                btcomm.iSendTransactionStatus(tStat, tid.getBytes());
                printMessage(message);
            }
        }
       //btdisconnector();
    }

    private void connectBT() {
        DEVICE_MAC = mPref.getString("DeviceMac",EPSConstant.DEVICE_MAC1);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
//        // If the adapter is null, then Bluetooth is not supported
        if (mBtAdapter == null) {
            tvBlueTooth.setText(getResources().getString(R.string.Trans_Bluetooth_Failed));
            return;
        }
//        iDevFoundCnt = 1;
//        try {
//            Log.d(TAG, "btnUnPair Unpairing / Removing Bond");
//            Set<BluetoothDevice> tmpBDevices = mBtAdapter.getBondedDevices();
//            for (BluetoothDevice bt : tmpBDevices) {
//                if (bt.getName().startsWith("ESFP")) {
//                    BluetoothPair.removeBond(bt);
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        if (btcomm != null && btcomm.isConnect()) {
//            // if already connected close the existing connection
//            btcomm.closeConn();
//        }
        tvBlueTooth.setText("");
        if (Utility.isNetWorkAvailable(Transaction.this)) {
            new ATCreateTransaction().execute();
        } else
            Utility.alert(getResources().getString(R.string.Trans_Internet_Failure_when_start_Transaction_head), getResources().getString(R.string.Trans_Internet_Failure_when_start_Transaction), Transaction.this);
    }

    private static String sCheckReturnCode(int iRetVal) {
        String s = "<" + iRetVal + "> ";
        if (iRetVal == FalconDev.SUCCESS) {
            return s + "FalconDev.SUCCESS";
        } else if (iRetVal == FalconDev.FAILURE) {
            return s + "FalconDev.FAILURE";
        } else if (iRetVal == FalconDev.NO_RESPONSE) {
            return s + "FalconDev.NO_RESPONSE";
        } else if (iRetVal == FalconDev.DEVICE_TIMEOUT) {
            return s + "FalconDev.DEVICE_TIMEOUT";
        } else if (iRetVal == FalconDev.INPUT_ERROR) {
            return s + "FalconDev.INPUT_ERROR";
        } else if (iRetVal == FalconDev.PACKET_ERROR) {
            return s + "FalconDev.PACKET_ERROR";

        } else if (iRetVal == FalconDev.LIMIT_EXCEEDED) {
            return s + "FalconDev.LIMIT_EXCEEDED";
        } else if (iRetVal == FalconDev.NO_DATA) {
            return s + "FalconDev.NO_DATA";
        } else if (iRetVal == FalconDev.PAPER_OUT) {
            return s + "FalconDev.PAPER_OUT";
        } else if (iRetVal == FalconDev.PLATEN_OPEN) {
            return s + "FalconDev.PLATEN_OPEN";
        } else if (iRetVal == FalconDev.HEADTEMP_HIGH) {
            return s + "FalconDev.HEADTEMP_HIGH";
        } else if (iRetVal == FalconDev.HEADTEMP_LOW) {
            return s + "FalconDev.HEADTEMP_LOW";
        } else if (iRetVal == FalconDev.IMPROPER_VOLTAGE) {
            return s + "FalconDev.IMPROPER_VOLTAGE";
        } else if (iRetVal == FalconDev.FILE_ERROR) {
            return s + "FalconDev.FILE_ERROR";

        } else if (iRetVal == FalconDev.SEND_RECV_EXCEPTION) {
            return s + "FalconDev.SEND_RECV_EXCEPTION";
        } else if (iRetVal == FalconDev.MONITOR_CANCELED_BY_APP) {
            return s + "FalconDev.MONITOR_CANCELED_BY_APP";
        } else if (iRetVal == BluetoothComm.TRANSACTION_NOT_INITIATED) {
            return s + "BluetoothComm.TRANSACTION_NOT_INITIATED";
        } else if (iRetVal == BluetoothComm.MERCHANT_CANCEL_MONITOR_STARTED) {
            return s + "BluetoothComm.MERCHANT_CANCEL_MONITOR_STARTED";
        } else if (iRetVal == BluetoothComm.MERCHANT_CANCEL_MONITOR_NOT_STARTED) {
            return s + "BluetoothComm.MERCHANT_CANCEL_MONITOR_NOT_STARTED";
        } else if (iRetVal == BluetoothComm.MERCHANT_CANCELED_THE_TRANSACTION) {
            return s + "BluetoothComm.MERCHANT_CANCELED_THE_TRANSACTION";
        } else {
            return s + "Unknown Error";
        }
    }

    //Handlers and Threads
    private Handler mCancelMessageHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                Log.e(TAG, "handle message....");
                switch (msg.what) {
                    case MAKE_PAYMENT_DIALOG_CTRL:
                        if (rlDisplayTransaction != null && rlDisplayTransaction.getVisibility() == View.VISIBLE) {
                            SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
                            Date date = new Date();
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String transactionDate = format.format(date);
                            new ATUpdateTransaction(tpref.getString("transactionID", ""), (ismTransCancelled ? "cancelled" : "declined"), "", transactionDate,"IMPS", false, false).execute();
                        } else {
                            Log.e(TAG, "rlMakePayent not accessible..... " + sCheckReturnCode(msg.arg1));
                        }
                        break;
                }
            } catch (Exception e) {
                Log.e(TAG, "Handler Excepttion");
                Log.e(TAG, "Ecp details... " + e.getMessage());
                e.printStackTrace();
            }


        }
    };

    private class CancelThreadMonitor extends Thread {
        @Override
        public void run() {
            btcomm.vSTART_MerchantCancelMonitor(new BluetoothComm.MercantCancelListner() {
                @Override
                public void onCancelled(int iResponse) {
                    Log.e(TAG, "iResponse : " + sCheckReturnCode(iResponse));
                    SystemClock.sleep(1000);
                    Log.e(TAG, "After Sleep .....");
                    btcomm.vSTOP_MerchantCancelMonitor();
                    SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
                    if (cdTimer != null)
                        cdTimer.cancel();
                    if (iResponse == BluetoothComm.MERCHANT_CANCELED_THE_TRANSACTION) {
                        ismTransCancelled = true;
                        if (mTransState == EPSConstant.TransactionState.USSDStarted) {
                            btcomm.iSendCancellationFeedback(FalconDev.Trans_Stat.CANCEL_Rejected, tpref.getString("transactionID", "").getBytes());
                        } else if (mTransState == EPSConstant.TransactionState.USSDFinished) {
                            btcomm.iSendCancellationFeedback(FalconDev.Trans_Stat.CANCEL_Accepted, tpref.getString("transactionID", "").getBytes());
                        } else {
                            btcomm.iSendCancellationFeedback(FalconDev.Trans_Stat.CANCEL_Accepted, tpref.getString("transactionID", "").getBytes());
                            mCancelMessageHandler.obtainMessage(MAKE_PAYMENT_DIALOG_CTRL, iResponse, 0).sendToTarget();
                        }
                    } else {
                        Log.e(TAG, "ELSE - MERCHANT_CANCELED_THE_TRANSACTION");
                    }


                }
            }, 15 * 60 * 1000);// Maximum Wait TIME FOR CANCELLATION 15 min
        }
    }

    //AsyncTasks
    private class ATCreateTransaction extends AsyncTask<String, String, String> {

        private ProgressDialog mpd = null;
        private Boolean mProgressCancelled = false;

        @Override
        public void onPreExecute() {
            this.mpd = new ProgressDialog(Transaction.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
//            this.mpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    mProgressCancelled = true;
//                    dialog.dismiss();
//                }
//            });
            mProgressCancelled = false;
            this.mpd.show();
        }

        @Override
        protected void onProgressUpdate(String... progUpdate) {
            super.onProgressUpdate(progUpdate);
            this.mpd.setMessage(progUpdate[0]);
        }

        @Override
        protected String doInBackground(String... params) {
            publishProgress(getResources().getString(R.string.Trans_Open_BT));
            Log.d(TAG, "StartScanConnectTask doInBackground");
            Log.d(TAG, "StartScanConnectTask Starting BT");
            int iWait = EPSConstant.BT_WAIT_TIME * 1000;
 //           mBtAdapter.disable();
            /* BT isEnable */
//            SystemClock.sleep(500);
            if(!mBtAdapter.isEnabled()) {
                mBtAdapter.enable();
//            SystemClock.sleep(500);
                //Wait miSLEEP_TIME seconds, start the Bluetooth device before you start scanning
                while (iWait > 0) {
                    if (!mBtAdapter.isEnabled())
                        iWait -= EPSConstant.BT_SLEEP_TIME; //miSLEEP_TIME;
                    else
                        break;
                    SystemClock.sleep(EPSConstant.BT_SLEEP_TIME);
                }
                if (iWait <= 0)
                    return EPSConstant.BT_FAILED_TO_START;
            }
            mTransState = EPSConstant.TransactionState.BTSwitchedOn;
            if (mProgressCancelled) {
                return EPSConstant.PROGRESS_CANCELLED;
            }
                        /* Register Receiver*/
//            mbDiscoveryFinished = false;
//            mbDeviceFound = false;
//            registerReceiver(mBRTimeoutBT, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
//            registerReceiver(mBRFindBT, new IntentFilter(BluetoothDevice.ACTION_FOUND));
//
//            Log.d(TAG, "StartScanConnectTask BT Started, Registering Receivers");
//            mBtAdapter.startDiscovery();//start scan
//            publishProgress(getResources().getString(R.string.Trans_Scan_BT));
//            Log.d(TAG, "StartScanConnectTask Receivers registered & discovery is started ");
//            iWait = EPSConstant.BT_WAIT_TIME * 1000;// Waits for miWAIT_TIME seconds
//            //Wait miSLEEP_TIME seconds, start the Bluetooth device before you start scanning
//            while (iWait > 0) {
//                if (mbDiscoveryFinished || mbDeviceFound || mProgressCancelled) {
//                    break; //return RET_SCAN_DEVICE_FINISHED;
//                } else {
//                    iWait -= EPSConstant.BT_SLEEP_TIME; //miSLEEP_TIME;
//                }
//                SystemClock.sleep(EPSConstant.BT_SLEEP_TIME);
//            }
//            if (!mbDeviceFound)
//                return EPSConstant.BT_SCAN_DONE; //RET_SCAN_DEVICE_FINISHED;
//            if (iWait <= 0) {
//                Log.e(TAG, "StartScanConnectTask Returning due to timeout...");
//                return EPSConstant.BT_SCAN_TIMEOUT; //RET_SCAN_DEVICE_TIMEOUT;
//            }
//            if (mBtAdapter.isDiscovering()) {
//                mBtAdapter.cancelDiscovery();
//            }
            BluetoothDevice BDevice;
            publishProgress(getResources().getString(R.string.Trans_Pair_BT));
            try {
                registerReceiver(mBRPairBT, new IntentFilter(BluetoothPair.PAIRING_REQUEST));
                registerReceiver(mBRPairBT, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
                BDevice = mBtAdapter.getRemoteDevice(DEVICE_MAC);// arg0[0] is MAC address
                if (BDevice.getBondState() == BluetoothDevice.BOND_BONDED) {
                    Log.d(TAG, "StartScanConnectTask already paired");
                    mbBonded = true;
                } else {
                    Log.d(TAG, "StartScanConnectTask Not paired hence pairing");
                    if (BluetoothPair.createBond(BDevice)) {
                        Log.d(TAG, "StartScanConnectTask Pairing Done");

                    } else
                        Log.d(TAG, "StartScanConnectTask Pairing Unsuccessful");
                    mbBonded = false;
                }
                iWait = /*miWAIT_TIME*/ EPSConstant.BT_WAIT_TIME * 1000;
                while (!mbBonded && iWait > 0 && !mProgressCancelled) {
                    SystemClock.sleep(/*miSLEEP_TIME*/EPSConstant.BT_SLEEP_TIME);
                    iWait -= EPSConstant.BT_SLEEP_TIME /*miSLEEP_TIME*/;
                }
            } catch (Exception e1) {
                Log.e(TAG, "StartScanConnectTask create Bond failed!");
                e1.printStackTrace();
                return EPSConstant.BT_RET_BOND_FAIL;
            } finally {
                unregisterReceiver(mBRPairBT);
            }
            if (mProgressCancelled) {
                return EPSConstant.PROGRESS_CANCELLED;
            }

//            Log.d(TAG, "StartScanConnectTask Closing connections if any");
//            if (btcomm != null && btcomm.isConnect()) {
//                // if already connected close the existing connection
//                btcomm.closeConn();
//            }

            if(btcomm == null) {
                Log.d(TAG, "StartScanConnectTask Initiating connection to : " + DEVICE_MAC);
                btcomm = new BluetoothComm(DEVICE_MAC);

                if (btcomm.createConn()) {
                    // Continue...
                    Log.d(TAG, "StartScanConnectTask Successfully Connected to : " + EPSConstant.DEVICE_NAME);
                    // return EPSConstant.CONN_SUCCESS;
                } else {
                    return EPSConstant.BT_CONN_FAIL;
                }
            }

            mTransState = EPSConstant.TransactionState.BTPaired;
            if (mProgressCancelled) {
                return EPSConstant.PROGRESS_CANCELLED;
            }
            publishProgress(getResources().getString(R.string.Trans_Enter_amount_in_device));

            String amount = "";
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Transaction.this.mTransDate = format.format(date);
            mTransState = EPSConstant.TransactionState.TransactionInitiatedOnDevice;
            Log.e(TAG, "AtCreateTransactionAsyncTask:Value of mTransState: " + mTransState);

            byte[] bDetails = btcomm.bInitiateTransaction();
            Transaction.this.mInvoiceNo = "";
            Transaction.this.mAmount = "";
            Transaction.this.mTransStatus = "not initiated";
            Transaction.this.mTID = "";
            Transaction.this.mMerchantID = "";
            Transaction.this.mMerchantName = "";
            Transaction.this.mBankCode = "";

            iRetVal = btcomm.iGetReturnCode();if (bDetails != null) {
                btcomm.setTransactionInitiated(true);
                Log.d(TAG, "Resp Initate transaction 1 : " + HexString.bufferToHex(bDetails));
                Log.i(TAG, "Resp Initate Transaction 2: " + new String(bDetails));
                List<String> packetData = Arrays.asList((new String(bDetails)).split(":"));
                amount = packetData.get(2);
                //HAVE TO REMOVE THIS HACK . IA HAS TO BE PLACED IN THE DEVICE NOT HERE
                String tid = packetData.get(0);
                if(tid.startsWith("I"))
                    Transaction.this.mTID = tid;
                else
                    Transaction.this.mTID = "IA"+tid;
                //*********************************************************************
                //terminalID = "530057";
                Transaction.this.mInvoiceNo = packetData.get(1);
                Transaction.this.mAmount = amount;
            } else {
                Log.i(TAG, "Resp Initate Transaction 3: " + sCheckReturnCode(iRetVal));
                if (iRetVal == FalconDev.DEVICE_TIMEOUT) {
                    return EPSConstant.DEVICE_TRANSACTION_TIMEOUT;
                } else if (iRetVal == FalconDev.TRANSACTION_CANCELED) {
                    return EPSConstant.DEVICE_CANCELLED;
                }
            }


            publishProgress(getResources().getString(R.string.Trans_Send_Trans_to_server));
            String serverStatus = "";

            mTransState = EPSConstant.TransactionState.TransactionInitiatedOnServer;
            Log.e(TAG, "AtCreateTransactionAsyncTask:Value of mTransState: " + mTransState);
            Log.e(TAG, "AtCreateTransactionAsyncTask:Value of mTransState: " + mTransState);
            if (Utility.isNetWorkAvailable(Transaction.this)) {
                try {
                    SharedPreferences pref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);
                    HashMap<String, String> headers = new HashMap<>();
                    HashMap<String, String> reqparams = new HashMap<>();
                    headers.put("User-Agent", "EPSFalconPlus/1.0");
                    headers.put("AUTHTOKEN", pref.getString("CustID", ""));
                    reqparams.put("amount", amount);
                    reqparams.put("transactionDate", Transaction.this.mTransDate);
                    reqparams.put("terminalID", Transaction.this.mTID);
                    reqparams.put("invoiceNumber", Transaction.this.mInvoiceNo);
                    EPSServerCaller api = new EPSServerCaller(getAssets());
                    String result = api.doPost(EPSWebURL.CREATE_TRANSACTION, reqparams, headers);
                    int responseCode = api.getLastResponseCode();

                    //GET RESPONSE FROM SERVER
                    JSONObject json = new JSONObject(result);
                    if (responseCode == 200) {
                        serverStatus = json.optString("status");
                        String code = json.optString("code");
                        //String serverMessage = json.optString("message");
                        if (serverStatus.equalsIgnoreCase(EPSConstant.SERVER_SUCCESS)) {

                            SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
                            SharedPreferences.Editor tedit = tpref.edit();
                            JSONObject trans = json.getJSONObject("transaction");
                            tedit.putString("transactionID", trans.optString("id"));
                            String serverTransactionID = trans.optString("id");
                            mLastUsedTransactionId = serverTransactionID;
                            tedit.putString("invoiceNumber", trans.optString("invoiceNumber"));
                            tedit.putString("amount", trans.optString("amount"));
                            tedit.putString("transactionType", trans.optString("transactionType"));
                            tedit.putString("transactionTime", trans.optString("transactionTimestamp"));
                            tedit.putString("paymentTimestamp", trans.optString("paymentTimestamp"));
                            tedit.putString("transactionDetails", trans.optString("transactionDetails"));
                            tedit.putString("transactionStatus", trans.optString("transactionStatus"));
                            JSONObject device = json.getJSONObject("device");
                            tedit.putString("terminalID", device.optString("terminalID"));
                            tedit.putString("displayName", device.optString("displayName"));
                            tedit.putString("phoneNumber", device.optString("phoneNumber"));
                            tedit.putString("mmid", device.optString("mmid"));
                            tedit.putString("merchantID", device.optString("merchantID"));
                            tedit.putString("merchantName", device.optString("merchantName"));
                            tedit.putString("bankIFSC", device.optString("bankIFSC"));
                            tedit.putString("bankAccount", device.optString("bankAccount"));
                            tedit.apply();
                            Transaction.this.mTransStatus = trans.optString("transactionStatus");
                            Transaction.this.mMerchantID = device.optString("merchantID");
                            Transaction.this.mMerchantName = device.optString("merchantName");
                            Log.e(TAG, "Transaction pending sent to device");
                            int x = btcomm.iSendTransactionStatus(FalconDev.Trans_Stat.Pending, serverTransactionID.getBytes());
                            Log.e(TAG, "Transaction resp : " + sCheckReturnCode(x));

                        } else if (code.equalsIgnoreCase(EPSConstant.DEVICE_INVALID)) {
                            serverStatus = EPSConstant.DEVICE_INVALID;
                            Log.e(TAG, "DEVICE_INVALID");
                            btcomm.iSendTransactionStatus(FalconDev.Trans_Stat.Declined, "".getBytes());

                        } else {
                            Log.e(TAG, "Transaction Declined sent to device");
                            int x = btcomm.iSendTransactionStatus(FalconDev.Trans_Stat.Declined, "".getBytes());
                            Log.e(TAG, "Transaction resp : " + sCheckReturnCode(x));
                            serverStatus = EPSConstant.SERVER_TRANSACTION_FAILED;
                        }
                    }
                    else
                    {
                        serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
                    }
                } catch (Exception e) {
                    serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
                }
            } else {
                serverStatus = EPSConstant.SERVER_INTERNET_FAILED;
            }
            publishProgress(getResources().getString(R.string.Trans_Trans_created_in_server));

            //RETURN STATUS FOR ERROR HANDLING
            if (serverStatus.equalsIgnoreCase(EPSConstant.SERVER_SUCCESS))
                //Transaction got created and completed. Next step is to execute payment
                return EPSConstant.CREATE_TRANSACTION_COMPLETE;
            else
                return serverStatus;
        }

        @Override
        public void onPostExecute(String result) {
            if (EPSConstant.CREATE_TRANSACTION_COMPLETE.equalsIgnoreCase(result)) {
                monitorThread = new CancelThreadMonitor();
                if (monitorThread.isAlive())
                    monitorThread.interrupt();
                monitorThread.start();
                Log.e(TAG, ">>>> before Td start Monitor Thread state  : " + String.valueOf(monitorThread.getState()));

                SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
                tvDisplayName.setText(tpref.getString("displayName", ""));
                String rupee = getResources().getString(R.string.Trans_Rs_Code);
                tvAmount.setText(rupee + Transaction.this.mAmount+".00");
                displayLayouts("display");
            } else if (result.equalsIgnoreCase(EPSConstant.PROGRESS_CANCELLED)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Trans_process_cancelled));
            } else if (result.equalsIgnoreCase(EPSConstant.BT_FAILED_TO_START)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_BT_not_Found_error));
            } else if (result.equalsIgnoreCase(EPSConstant.BT_SCAN_DONE)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_BT_scan_failed));
            } else if (result.equalsIgnoreCase(EPSConstant.BT_SCAN_TIMEOUT)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_BT_Scan_timed_out));
            } else if (result.equalsIgnoreCase(EPSConstant.BT_RET_BOND_FAIL)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_BT_Bonding_failed));
            } else if (result.equalsIgnoreCase(EPSConstant.BT_CONNECTION_FAILED)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_BT_Connection_failed));
            } else if (result.equalsIgnoreCase(EPSConstant.BT_CONN_FAIL)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_BT_Connection_failed));
            } else if (result.equalsIgnoreCase(EPSConstant.DEVICE_TRANSACTION_TIMEOUT)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Device_Trans_Timed_out));
            } else if (result.equalsIgnoreCase(EPSConstant.DEVICE_INVALID)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Device_is_not_valid));
            } else if (result.equalsIgnoreCase(EPSConstant.DEVICE_CANCELLED)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Merchant_Cancelled));
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INTERNET_FAILED)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Internet_Failed));
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_CONNECTION_FAILED)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Server_Connection_Failed));
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INVALID_REQUEST)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Server_invalid_request));
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_AUTH_FAILED)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Server_Auth_Error));
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_DEVICE_NOT_VALID)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Device_is_not_valid_in_server));
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_TRANSACTION_FAILED)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Server_Trans_failed));
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_FORMAT_ERROR)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Server_Format_Error));
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_UNKNOWN)) {
                disconnectBTNoTrans(getResources().getString(R.string.Trans_Unknown_Server_error));
            } else {
                Log.e(TAG, "What is happenenig.... : " + result);
            }
            //Reset Transaction Cancel Status
            ismTransCancelled = false;
            if (this.mpd.isShowing())
                this.mpd.dismiss();
        }
    }

    public class ATUpdateTransaction extends AsyncTask<String, String, String> {
        String mCode;
        String mStatus;
        String mMessage;
        String mTransStatus;
        String mTransactionID;
        String mTransactionDetails;
        HashMap<String, String> reqparams = new HashMap<>();
        Boolean mForceDisconnect = false;
        private ProgressDialog mpd = null;

        public ATUpdateTransaction(String transactionID, String status, String details, String paymentDate,String transType, Boolean forceUpdate, Boolean forceDisconnect) {
            mTransactionID = transactionID;
            mTransStatus = status;
            reqparams.put("transactionID", transactionID);
            reqparams.put("status", status);
            reqparams.put("details", details);
            reqparams.put("paymentDate", paymentDate);
            reqparams.put("forceUpdate", forceUpdate.toString());
            reqparams.put("transactionType",transType);
            Transaction.this.mTransStatus=status;
            mForceDisconnect = forceDisconnect;
            mTransactionDetails = details;
            this.mpd = new ProgressDialog(Transaction.this);
            this.mpd.setMessage(getResources().getString(R.string.Trans_update_progress));
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            mCode = "";
            mStatus = "FAILED";
            mMessage = "";
            try {
                if (Utility.isNetWorkAvailable(Transaction.this)) {
                    SharedPreferences pref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("User-Agent", "EPSFalconPlus/1.0");
                    headers.put("AUTHTOKEN", pref.getString("CustID", ""));
                    EPSServerCaller api = new EPSServerCaller(getAssets());
                    String result = api.doPost(EPSWebURL.UPDATE_TRANSACTION, reqparams, headers);
                    int responseCode = api.getLastResponseCode();
                    if (responseCode == 200) {
                        try {
                            JSONObject json = new JSONObject(result);
                            mStatus = json.optString("status");
                            mCode = json.optString("code");
                            mMessage = json.optString("message");
                        } catch (JSONException e) {
                            return "CODE_ERROR";
                        }
                    } else {
                        return EPSConstant.SERVER_CONNECTION_FAILED;
                    }
                } else {
                    return EPSConstant.SERVER_INTERNET_FAILED;
                }
            } catch (Exception ex) {
                return "UNKNOWN";
            }
            return mStatus;
        }

        @Override
        protected void onProgressUpdate(String... progUpdate) {
            super.onProgressUpdate(progUpdate);
            this.mpd.setMessage(progUpdate[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //mDialog_progress.dismiss();
            if (mCode.isEmpty())
                mCode = result;
            try {

                if (mStatus.equalsIgnoreCase("SUCCESS") || mForceDisconnect) {
                    SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
                    SharedPreferences.Editor tedit = tpref.edit();
                    tedit.clear();
                    tedit.apply();
                    //Disconnect from BT
                    Log.e(TAG, "Disconnecting here.... mTransStatus");
                    if (mOldTransFound) {
                        //disconnectBT(getResources().getString(R.string.Trans_Previous_Trans_Update), FalconDev.Trans_Stat.Success);
                        disconnectBT("", FalconDev.Trans_Stat.Success);
                        mOldTransFound = false;
                    } else if (mTransStatus.equalsIgnoreCase("declined"))
                        disconnectBT(getResources().getString(R.string.Trans_Decline_trans), FalconDev.Trans_Stat.Declined);
                    else if (mTransStatus.equalsIgnoreCase("cancelled"))
                        disconnectBT(getResources().getString(R.string.Trans_Merchant_cancelled_Trans), FalconDev.Trans_Stat.Declined);
                    else if (mTransStatus.equalsIgnoreCase("approved"))
                        disconnectBT(mTransactionDetails, FalconDev.Trans_Stat.Success);
                    else if (mTransStatus.equalsIgnoreCase("timeout"))
                        disconnectBT(getResources().getString(R.string.Trans_Trans_timed_out), FalconDev.Trans_Stat.Timeout);
                    else
                        disconnectBT(getResources().getString(R.string.Trans_Trans_completed), FalconDev.Trans_Stat.Success);
                } else if (mCode.equalsIgnoreCase("TRANSACTION_NOT_FOUND")) {
                    //Utility.alert("Transaction Error", "No Transaction found matching the Code", Transaction.this);
                    disconnectBT(getResources().getString(R.string.Trans_Format_Error), FalconDev.Trans_Stat.Timeout);
                } else if (mCode.equalsIgnoreCase("TRANSACTION_NOT_PENDING")) {
                    //Utility.alert("Transaction Error", "No Pending Transaction found matching the Code", Transaction.this);
                    disconnectBT(getResources().getString(R.string.Trans_Format_Error), FalconDev.Trans_Stat.Timeout);
                } else if (mCode.equalsIgnoreCase("FORMAT_ERROR")) {
                    //Utility.alert("Format Error", mMessage, Transaction.this);
                    disconnectBT(getResources().getString(R.string.Trans_Format_Error), FalconDev.Trans_Stat.Timeout);
                } else if (mCode.equalsIgnoreCase("UNKNOWN")) {
                    //Utility.alert("Error", "Something went wrong and not able to complete the request. Please try after some time", Transaction.this);
                    disconnectBT(getResources().getString(R.string.Trans_Server_Connection_Error), FalconDev.Trans_Stat.Timeout);
                } else if (mCode.equalsIgnoreCase("CODE_ERROR")) {
                    //Utility.alert("Error", "Improper information received from Server. Please retry.", Transaction.this);
                    disconnectBT(getResources().getString(R.string.Trans_Invalid_Error), FalconDev.Trans_Stat.Timeout);
                } else if (mCode.equalsIgnoreCase(EPSConstant.SERVER_INTERNET_FAILED)) {
                    //Utility.alert("Network Error", "Internet Failed. Could not update the server", Transaction.this);
                    disconnectBT(getResources().getString(R.string.Trans_Internet_Failed_Error), FalconDev.Trans_Stat.Timeout);
                } else if (mCode.equalsIgnoreCase(EPSConstant.SERVER_CONNECTION_FAILED)) {
                    disconnectBT(getResources().getString(R.string.Trans_Server_Connection_Error), FalconDev.Trans_Stat.Timeout);
                } else {
                    Utility.alert(getResources().getString(R.string.Trans_Unknown_error_head),getResources().getString(R.string.Trans_Unknown_error),Transaction.this);
                }
            } catch (Exception e) {
                Utility.alert(getResources().getString(R.string.Trans_Unknown_error_head),getResources().getString(R.string.Trans_Unknown_error),Transaction.this);
            }

            if (this.mpd.isShowing())
                this.mpd.dismiss();
        }
    }

    private class ATSmsCheck extends AsyncTask<String, String, String> {
        ProgressDialog mpd = null;
        Boolean forceCancel = false;
        Boolean allowCancel = false;
        String Message1 = getResources().getString(R.string.Trans_SMS_Waiting);
        String Message2 = getResources().getString(R.string.Trans_SMS_Waiting_first_20_secs);
        String Message3 = getResources().getString(R.string.Trans_SMS_Waiting_after_20_secs);
        long iWait = 0;
        @Override
        public void onPreExecute() {
            this.mpd = new ProgressDialog(Transaction.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.Trans_Cancel_Waiting_Button), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                        Utility.alert(getResources().getString(R.string.Trans_Cancel_Waiting_popup_head),getResources().getString(R.string.Trans_Cancel_Waiting_popup), Transaction.this);
                        forceCancel = true;
                        dialog.dismiss();
                }
            });
            this.mpd.show();
            forceCancel = false;
        }
        @Override
        protected void onProgressUpdate(String... progUpdate) {
            super.onProgressUpdate(progUpdate);
            this.mpd.setMessage(progUpdate[0]);
            if(progUpdate[0].equalsIgnoreCase(Message1))
                this.mpd.getButton(ProgressDialog.BUTTON_POSITIVE).setEnabled(false);
            else if(progUpdate[0].equalsIgnoreCase(Message3))
                this.mpd.getButton(ProgressDialog.BUTTON_POSITIVE).setEnabled(true);
        }

        @Override
        protected String doInBackground(String... strings) {
            mSMSDetails = "";
            publishProgress(Message1);
            IntentFilter smsFilter = new IntentFilter();
            smsFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
            registerReceiver(mSmsReceiver, smsFilter);

            long iTotal = EPSConstant.SMS_WAIT_TIME * 1000;
            long icancellimit = EPSConstant.SMS_NO_CANCEL*1000;
            while (iWait < iTotal) {
                if(iWait > icancellimit) {
                    if(!allowCancel) {
                        allowCancel = true;
                        publishProgress(Message3);
                    }
                }
                else{
                    publishProgress(Message2.concat(" "+Math.round((icancellimit-iWait)/1000)).concat(" seconds"));
                }
                if (!mSMSDetails.isEmpty() || forceCancel) {
                    Log.e(TAG, "SMS 3 Recd : \n" + mSMSDetails);
                    break;
                } else {
                    iWait += EPSConstant.SMS_SLEEP_TIME;
                }
                SystemClock.sleep(EPSConstant.SMS_SLEEP_TIME);
                Log.e("TAG", "Waiting for ...." + iWait);
            }
            if (btcomm.isMerchantCancelationMonitorActive())
                btcomm.vSTOP_MerchantCancelMonitor();
            unregisterReceiver(mSmsReceiver);
            return null;
        }

        @Override
        public void onPostExecute(String result) {
            if (this.mpd.isShowing())
                this.mpd.dismiss();
            SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor tedit = tpref.edit();
            String transID = tpref.getString("transactionID", "");
            String td = "";
            String ts = "";
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // modified the format
            String pt = format.format(date);
            if (!transID.isEmpty()) {
                if (!mSMSDetails.isEmpty()) {
                    //Take actions on SMS Received
                    td = mSMSDetails;
                    ts = "approved";
                } else if (ismTransCancelled) {
                    ts = "cancelled";
                } else if (forceCancel) {
                    ts = "declined";
                } else
                    //Other Scenarios
                    ts = "timeout";
            }
            tedit.putString("paymentTimestamp", pt);
            tedit.putString("transactionDetails", td);
            tedit.putString("transactionStatus", ts);
            tedit.apply();
            if (ts.equalsIgnoreCase("approved")) {
                new ATUpdateTransaction(transID, ts, td, pt,"IMPS", true, false).execute();
            } else {
                new ATUpdateTransaction(transID, ts, "", pt,"IMPS", false, false).execute();
            }
        }
    }

    //Broadcast Receivers
    private BroadcastReceiver mSmsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mSMSDetails.isEmpty()) {
                Bundle myBundle = intent.getExtras();
                if (myBundle != null) {
                    Object[] pdus = (Object[]) myBundle.get("pdus");
                    if (pdus != null) {
                        for (Object pdu : pdus) {
                            SmsMessage sms;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                String format = myBundle.getString("format");
                                sms = SmsMessage.createFromPdu((byte[]) pdu, format);
                            } else {
                                sms = SmsMessage.createFromPdu((byte[]) pdu);
                            }
                            //VALIDATE MESSAGE
                            //Search for Debited and Credited keywords where the debited index is lower than credited index
                            //Search for "IMPS Ref No" keyword
                            String message = sms.getMessageBody();
                            String header = sms.getOriginatingAddress();
                            //All SMS Gateways are from IM-, DM-, TM-,DZ-, etc,
                            //message = "Your a/c no. XXXXXXXXXX1074 is debited for Rs. 10.00 on 07-09-16 and a/c linked to mobile 9XXXXXX009 credited (IMPS Ref no 625112983583)";
                            if(header.indexOf('-')==2) {
                                int dpos = message.indexOf("debited");
                                int cpos = message.indexOf("credited");
                                SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
                                //Get Amount
                                String amt = Transaction.this.mAmount;
                                //Get Merchant Phone Number
                                String mphone = tpref.getString("phoneNumber", "");
                                String mbankacc = tpref.getString("bankAccount", "");
                                Boolean bValid = false;
                                if (mbPaymentType.equals("MMID"))
                                    bValid = validateDetails(message, mphone);
                                else
                                    bValid = validateDetails(message, mbankacc);
                                if (dpos < cpos && message.toLowerCase().indexOf("imps ref no") > 0 && message.indexOf(amt) > 0 && bValid) {
                                    mSMSDetails = message;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private boolean validateDetails(String message, String acc)
        {
            //split message into words
            int acclen = acc.length();
            String words[] = message.split(" ");
            ArrayList<String> accLengthWords = new ArrayList<String>();
            //get only the words which has is of same size of account number
            Boolean bFound = false;
            for (String word : words) {
                if (word.length() == acclen)
                    accLengthWords.add(word);
            }
            if(accLengthWords.size() > 0 ) {
                char[] numbers = acc.toLowerCase().toCharArray();
                for (String word : accLengthWords) {
                    char[] letters = word.toLowerCase().toCharArray();
                    boolean bValid = false;
                    for (int x = 0; x < acclen; x++) {
                        bValid = true;
                        if (letters[x] == numbers[x] || letters[x] == 'x') {

                        } else {
                            bValid = false;
                            break;
                        }
                    }
                    if (bValid) {
                        bFound = true;
                        break;
                    }
                }
            }
            return bFound;
        }
    };

    private BroadcastReceiver mBRPairBT = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                mbBonded = (device.getBondState() == BluetoothDevice.BOND_BONDED);
            }
        }
    };

    private BroadcastReceiver mBRFindBT = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
			/* bluetooth device profiles*/
            Log.i(TAG, "mBRFindBT Bluetooth device found : " + iDevFoundCnt);
            iDevFoundCnt++;
			/* get the search results */
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			/* create found device profiles to htDeviceInfo*/
            Log.d(TAG, "mBRFindBT MAC  : " + device.getAddress());
            if (device.getBondState() == BluetoothDevice.BOND_BONDED)
                Log.d(TAG, "mBRFindBT BOND : Paired");
            else
                Log.d(TAG, "mBRFindBT BOND : NOT Paired");
            int cntr = 10;
            while (null == device.getName()) {
                Log.i(TAG, "mBRFindBT Try : " + cntr);
                if (cntr == 0) {
                    Log.e(TAG, "mBRFindBT DEVICE NAME CANNOT BE IDENTIFIED for MAC : " + device.getAddress());
                    break;
                }
                SystemClock.sleep(300);
                cntr--;
            }
            String sIdentifiedDevName = device.getName();
            Log.e(TAG, "mBRFindBT Device Name is " + sIdentifiedDevName + " for MAC : " + device.getAddress());
            if (EPSConstant.DEVICE_NAME.equals(sIdentifiedDevName)) {
                Log.d(TAG, "mBRFindBT Device Identified");
                DEVICE_MAC = device.getAddress();
                mbDeviceFound = true;
            }
        }
    };

    private BroadcastReceiver mBRTimeoutBT = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "mBRTimeoutBT Bluetooth scanning is finished");
            mbDiscoveryFinished = true; //set scan is finished
            unregisterReceiver(mBRFindBT);
            unregisterReceiver(mBRTimeoutBT);
            if (mbDeviceFound) {
                Log.d(TAG, "mBRTimeoutBT Not required as device is found");
                mBtAdapter.cancelDiscovery(); // Discovery is being stopped as device is found

            } else {
                Log.d(TAG, "mBRTimeoutBT Device NOT Found");
            }
        }
    };
}
