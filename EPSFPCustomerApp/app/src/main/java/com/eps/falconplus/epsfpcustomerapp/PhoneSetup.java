package com.eps.falconplus.epsfpcustomerapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.eps.falconplus.server.EPSAuthenticator;
import com.eps.falconplus.server.EPSServerCaller;
import com.eps.falconplus.server.EPSWebURL;
import com.eps.falconplus.server.Utility;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Random;

public class PhoneSetup extends AppCompatActivity implements View.OnClickListener {

    Button bInstructions,bOTP;
    ImageView ivExit;
    EditText etPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_phone_setup);

        bInstructions = (Button) findViewById(R.id.btnInstructions);
        bInstructions.setOnClickListener(this);
        bOTP = (Button) findViewById(R.id.btnOTP);
        bOTP.setOnClickListener(this);
        etPhone = (EditText)findViewById(R.id.etPhone);
        etPhone.setInputType(InputType.TYPE_CLASS_NUMBER);
        etPhone.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        ivExit = (ImageView) findViewById(R.id.ivExit);
        ivExit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == bInstructions)
        {
            startActivity(new Intent(PhoneSetup.this,Instructions.class));
            overridePendingTransition( R.anim.left_in, R.anim.left_out);
        }
        else if(view == ivExit)
        {
            exitScreen();
        }
        else {
            if (view == bOTP) {
                String etp = etPhone.getText().toString();
                if (etp.isEmpty() || etp.length()!=10)
                    Utility.alert(getResources().getString(R.string.Phone_Empty_phone_number_text_head), getResources().getString(R.string.Phone_Empty_phone_number_text),PhoneSetup.this);
                else {
                    //GENERATE 6 DIGIT OTP
                    Random ran = new Random();
                    int nOTP= (100000 + ran.nextInt(900000));
                    String strOTP = ""+nOTP;
                    //SEND OTP TO SERVER
                    //Toast.makeText(getApplicationContext(),strOTP,Toast.LENGTH_LONG).show();
                    if(Utility.isNetWorkAvailable(PhoneSetup.this))
                    {
                        new AsyncTaskSendOTP(etp,strOTP).execute();
                   }
                    else
                    {
                        Utility.alert(getResources().getString(R.string.Phone_Internet_Failure_error_head),getResources().getString(R.string.Phone_Internet_Failure_error),PhoneSetup.this);
                    }
                }
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==121){
            finish();

        }
    }
    private void exitScreen(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(PhoneSetup.this);
        builder.setMessage(getResources().getString(R.string.Phone_Exit_Button)).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    class AsyncTaskSendOTP extends AsyncTask<String, String, String>
    {
        String mPhone;
        String mOTP;
        String mCode;
        String mStatus;
        String mMessage;
        EPSServerCaller api;
        ProgressDialog mpd = null;

        AsyncTaskSendOTP(String phone, String otp){
            this.mpd = new ProgressDialog(PhoneSetup.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.setMessage("Connecting to Server. Please wait...");
            this.mpd.show();
            mPhone = phone;
            mOTP = otp;
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                mCode = "";
                mStatus = "";
                mMessage="";
                HashMap<String,String> reqparams = new HashMap<String,String>();
                HashMap<String,String> headers = new HashMap<String,String>();
                headers.put("User-Agent",EPSWebURL.FP_USER_AGENT);
                reqparams.put("phoneNumber",mPhone);
                reqparams.put("otpCode",mOTP);
                api = new EPSServerCaller(getAssets());
                String result = api.doPost(EPSWebURL.SEND_OTP,reqparams,headers);
                int responseCode = api.getLastResponseCode();
                if (responseCode == 200) {
                    parseResponse(result);
                } else {
                   mCode = "INTERNET_FAILED";
                }
            } catch (Throwable ex) {
                mCode="UNKNOWN";
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.mpd.isShowing())
                this.mpd.dismiss();

            if(mStatus.equalsIgnoreCase("success")){
                Intent in = new Intent(PhoneSetup.this, SendOTP.class);
                in.putExtra("Phone",mPhone);
                in.putExtra("OTP",mOTP);
                startActivityForResult(in,121);
                overridePendingTransition( R.anim.left_in, R.anim.left_out);
            }else if(mCode.equalsIgnoreCase("FORMAT_ERROR"))
                Utility.alert(getResources().getString(R.string.Phone_Data_Format_Error_head),getResources().getString(R.string.Phone_Data_Format_Error),PhoneSetup.this);
            else if(mCode.equalsIgnoreCase("SMS_FAILED"))
                Utility.alert(getResources().getString(R.string.Phone_SMS_sending_error_head),getResources().getString(R.string.Phone_SMS_sending_error),PhoneSetup.this);
            else if(mCode.equalsIgnoreCase("GATEWAY_ERROR"))
                Utility.alert(getResources().getString(R.string.Phone_SMS_Gateway_error_head),getResources().getString(R.string.Phone_SMS_Gateway_error),PhoneSetup.this);
            else if(mCode.equalsIgnoreCase("INTERNET_FAILED"))
                Utility.alert(getResources().getString(R.string.Phone_Internet_Failure_error_head),getResources().getString(R.string.Phone_Internet_Failure_error),PhoneSetup.this);
            else if(mCode.equalsIgnoreCase("UNKNOWN"))
                Utility.alert(getResources().getString(R.string.Phone_Unknown_error_head),getResources().getString(R.string.Phone_Unknown_error),PhoneSetup.this);
            else
                Utility.alert(getResources().getString(R.string.Phone_Unknown_error_head),getResources().getString(R.string.Phone_Unknown_error),PhoneSetup.this);
        }
        private void parseResponse(String resp)
        {
            try {
                JSONObject json = new JSONObject(resp);
                mStatus = json.optString("status");
                mCode = json.optString("code");
                mMessage = json.optString("message");
            } catch (JSONException e) {e.printStackTrace();}
        }

    }
}
