package com.eps.falconplus.server;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satish_kota on 24-05-2016.
 */
public class EPSTransactionObj implements Parcelable
{
    private String TransactionID="";
    private String TerminalID="";
    private String Amount="";
    private String DisplayName="";
    private String PhoneNumber="";
    private String mmid="";
    private String InvoiceNumber="";
    private String TransactionType="";
    private String TransactionTime="";
    private String PaymentTimestamp="";
    private String TransactionDetails="";
    private String TransactionStatus="";


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getTransactionID());
        parcel.writeString(getTerminalID());
        parcel.writeString(getAmount());
        parcel.writeString(getDisplayName());
        parcel.writeString(getPhoneNumber());
        parcel.writeString(getMmid());
        parcel.writeString(getInvoiceNumber());
        parcel.writeString(getTransactionType());
        parcel.writeString(getTransactionTime());
        parcel.writeString(getPaymentTimestamp());
        parcel.writeString(getTransactionDetails());
        parcel.writeString(getTransactionStatus());
    }

    private EPSTransactionObj(Parcel in) {

        setTransactionID(in.readString());
        setTerminalID(in.readString());
        setAmount(in.readString());
        setDisplayName(in.readString());
        setPhoneNumber(in.readString());
        setMmid(in.readString());
        setInvoiceNumber(in.readString());
        setTransactionType(in.readString());
        setTransactionTime(in.readString());
        setPaymentTimestamp(in.readString());
        setTransactionDetails(in.readString());
        setTransactionStatus(in.readString());
    }

    public EPSTransactionObj() {
        // TODO Auto-generated constructor stub
    }

    public static final Parcelable.Creator<EPSTransactionObj> CREATOR=new Parcelable.Creator<EPSTransactionObj>()
    {

        public EPSTransactionObj createFromParcel(Parcel in)
        {
            return new EPSTransactionObj(in);
        }

        @Override
        public EPSTransactionObj[] newArray(int size)
        {
            return new EPSTransactionObj[size];
        }

    };

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }

    public String getTerminalID() {
        return TerminalID;
    }

    public void setTerminalID(String terminalID) {
        TerminalID = terminalID;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getMmid() {
        return mmid;
    }

    public void setMmid(String mmid) {
        this.mmid = mmid;
    }

    public String getInvoiceNumber() {
        return InvoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        InvoiceNumber = invoiceNumber;
    }

    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String transactionType) {
        TransactionType = transactionType;
    }

    public String getTransactionTime() {
        return TransactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        TransactionTime = transactionTime;
    }

    public String getPaymentTimestamp() {
        return PaymentTimestamp;
    }

    public void setPaymentTimestamp(String paymentTimestamp) {
        PaymentTimestamp = paymentTimestamp;
    }

    public String getTransactionDetails() {
        return TransactionDetails;
    }

    public void setTransactionDetails(String transactionDetails) {
        TransactionDetails = transactionDetails;
    }

    public String getTransactionStatus() {
        return TransactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        TransactionStatus = transactionStatus;
    }
}
