package com.eps.falconplus.server;

import com.eps.falconplus.epsfpcustomerapp.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by satish_kota on 26-05-2016.
 */
public class EPSConstant {
    public static final String PROGRESS_CANCELLED = "PROGRESS_BAR_CANCELLED";
    public static final String DEVICE_INVALID = "DEVICE_NOT_VALID";
    public static final String DEVICE_TRANSACTION_TIMEOUT = "DEVICE_TRANSACTION_TIMEOUT";
    public static final String BT_FAILED_TO_START = "BT_FAILED_TO_START";
    public static final String BT_SCAN_DONE = "BT_SCAN_DONE";
    public static final String BT_SCAN_TIMEOUT = "BT_SCAN_TIMEOUT";
    public static final String BT_CONNECTION_FAILED = "BT_CONNECTION_FAILED";
    public static final String DEVICE_CANCELLED = "CANCELLED";
    public static final String CREATE_TRANSACTION_COMPLETE = "SUCCESS";
    public static final String SERVER_SUCCESS = "SUCCESS";
    public static final String SERVER_UNKNOWN = "UNKNOWN";
    public static final String SERVER_INTERNET_FAILED = "INTERNET_FAILED";
    public static final String SERVER_CONNECTION_FAILED = "CONNECTION_FAILED";
    public static final String SERVER_INVALID_REQUEST = "INVALID_REQUEST";
    public static final String SERVER_AUTH_FAILED = "AUTHENTICATION_FAILED";
    public static final String SERVER_DEVICE_NOT_VALID = "DEVICE_NOT_VALID";
    public static final String SERVER_TRANSACTION_FAILED = "TRANSACTION_FAILED";
    public static final String SERVER_FORMAT_ERROR = "FORMAT_ERROR";
    public static final String SERVER_FAILED = "SERVER_FAILED";
    public static final String DEVICE_NAME = "ESFP0002"; // ESYS12345       ESFP0002
    public static final int BT_WAIT_TIME = 20;
    public static final int BT_SLEEP_TIME = 100;
    public static final int SMS_WAIT_TIME = 60; //120
    public static final int SMS_SLEEP_TIME = 100;
    public static final int SMS_NO_CANCEL = 20;

    public static final int APPLICATION_TRANSACTION_TIMEOUT = 61;

    public static final String BT_CONN_FAIL = "CONN_FAIL";//0x04;
    public static final String BT_CONN_SUCCESS = "CONN_SUCCESS"; //0x05;
    public static final String BT_RET_BOND_FAIL = "RET_BOND_FAIL"; //0x06;

    public static final String SMS_TIMEOUT = "SMS_TIMEOUT";
    public static final String SMS_SUCCESS = "SMS_SUCCESS";


    public static String DEVICE_MAC1 = "00:04:3E:94:BC:8D";//"00:04:3E:94:BA:78";
    public static String DEVICE_MAC2 = "00:04:3E:94:BA:78";

    public enum TransactionState {
        InitialState,
        BTSwitchedOn,
        BTPaired,
        TransactionInitiatedOnDevice,
        TransactionInitiatedOnServer,
        WaitingForMakePayment,
        TransactionDeclined,
        CashTransaction,
        USSDStarted,
        USSDFinished,
        SMSReceived,
        USSDTimeout
    }
    public static String getBankName(String code)
    {
        List<String> bcs = Arrays.asList(mBankCodes);
        int pos = bcs.indexOf(code);
        return mBankNames[pos];
    }
    public static Integer getBankLogo(String code)
    {
        List<String> bcs = Arrays.asList(mBankCodes);
        int pos = bcs.indexOf(code);
        return mThumbIds[pos];
    }
    public static final String[] mBankCodes = {"41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91"};
    public static final String[] mBankIFSC = {"SBIN","PUNB","HDFC","ICIC","UTIB","CNRB","BKID","BARB","IBKL","UBIN","CBIN","IOBA","ORBC","ALLA","SYNB","UCBA","CORP","IDIB","ANDB","SBHY","MAHB","STBP","UTBI","VIJB","BKDN","YESB","SBTR","KKBK","INDB","SBBJ","PSIB","FDRL","SBMY","SIBL","KVBL","KARB","TMBL","DCBL","RATN","NTBL","JSBP","MSNU","NKGS","SRCB","ASBL","BMBL","ABHY","PMCB","HCBL","GSCB","KCCB"};
    public static final String[] mBankNames = {"STATE BANK OF INDIA","PUNJAB NATIONAL BANK","HDFC BANK","ICICI BANK","AXIS BANK","CANARA BANK","BANK OF INDIA","BANK OF BARODA","IDBI BANK","UNION BANK OF INDIA","CENTRAL BANK OF INDIA","INDIAN OVERSEAS BANK","ORIENTAL BANK OF COMMERCE","ALLAHABAD BANK","SYNDICATE BANK","UCO BANK","CORPORATION BANK","INDIAN BANK","ANDHRA BANK","STATE BANK OF HYDERABAD","BANK OF MAHARASHTRA","STATE BANK OF PATIALA","UNITED BANK OF INDIA","VIJAYA BANK","DENA BANK","YES BANK","STATE BANK OF TRAVANCORE","KOTAK BANK","INDUSIND BANK","STATE BANK OF BIKENER AND JAIPUR","PUNJAB & SINDH BANK","FEDERAL BANK","STATE BANK OF MYSORE","SOUTH INDIAN BANK","KARUR VYSYA BANK","KARNATAKA BANK","TAMILNAD MERCANTILE BANK","DEVELOPMENT CREDIT BANK","RATNAKAR BANK","NAINITAL BANK","JANATA BANK","MEHSANA URBAN CO-OP BANK","NKGS BANK","SARASWAT BANK","APNA SAHAKARI BANK","BHARATIYA MAHILA BANK","ABHYUDAYA BANK","PUNJAB & MAHARASHTRA CO-OP BANK","HASTI CO-OP BANK","GUJARAT STATE CO-OP BANK","KALUPUR BANK"};
    private static final Integer[] mThumbIds = {
            R.drawable.bc41,R.drawable.bc42,R.drawable.bc43,R.drawable.bc44,R.drawable.bc45,R.drawable.bc46,R.drawable.bc47,R.drawable.bc48,R.drawable.bc49,R.drawable.bc50,R.drawable.bc51,R.drawable.bc52,R.drawable.bc53,R.drawable.bc54,R.drawable.bc55,R.drawable.bc56,R.drawable.bc57,R.drawable.bc58,R.drawable.bc59,R.drawable.bc60,R.drawable.bc61,R.drawable.bc62,R.drawable.bc63,R.drawable.bc64,R.drawable.bc65,R.drawable.bc66,R.drawable.bc67,R.drawable.bc68,R.drawable.bc69,R.drawable.bc70,R.drawable.bc71,R.drawable.bc72,R.drawable.bc73,R.drawable.bc74,R.drawable.bc75,R.drawable.bc76,R.drawable.bc77,R.drawable.bc78,R.drawable.bc79,R.drawable.bc80,R.drawable.bc81,R.drawable.bc82,R.drawable.bc83,R.drawable.bc84,R.drawable.bc85,R.drawable.bc86,R.drawable.bc87,R.drawable.bc88,R.drawable.bc89,R.drawable.bc90,R.drawable.bc91
    };
}
