package com.eps.falconplus.epsfpcustomerapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Information extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        ImageView ivExit = (ImageView) findViewById(R.id.ivExit);
        if (ivExit != null)
            ivExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                    overridePendingTransition( R.anim.right_in, R.anim.right_out);
                }
            });

        Button btnInstructions = (Button) findViewById(R.id.btnInstructions);
        if(btnInstructions != null)
            btnInstructions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,Instructions.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });

        Button btnFAQ = (Button) findViewById(R.id.btnFAQ);
        if(btnFAQ != null)
            btnFAQ.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,FAQ.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });

        Button btnTAC = (Button) findViewById(R.id.btnTAC);
        if(btnTAC != null)
            btnTAC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,TermsConditions.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });

        Button btnPrivacy = (Button) findViewById(R.id.btnPrivacy);
        if(btnPrivacy != null)
            btnPrivacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,PrivacyPolicy.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });

        Button btnContact = (Button) findViewById(R.id.btnContact);
        if(btnContact != null)
            btnContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,Contact.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.right_in, R.anim.right_out);

    }
}
