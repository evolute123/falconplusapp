package com.eps.falconplus.epsfpcustomerapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.eps.falconplus.server.EPSConstant;

public class Settings extends AppCompatActivity implements View.OnClickListener{

    ImageView ivBack, ivInfo;
    RelativeLayout rlYourBanks,rlAddBanks, rlBalEnq,rlCngMPin,rlShowMMID;
    Switch swSelectDevice;
    SharedPreferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ivBack = (ImageView) findViewById(R.id.ivBack);
        if (ivBack != null)
            ivBack.setOnClickListener(this);

        ivInfo = (ImageView) findViewById(R.id.ivInfo);
        if (ivInfo != null)
            ivInfo.setOnClickListener(this);

        rlYourBanks = (RelativeLayout) findViewById(R.id.rlYourBanks);
        if (rlYourBanks != null)
            rlYourBanks.setOnClickListener(this);

        rlAddBanks = (RelativeLayout) findViewById(R.id.rlAddBanks);
        if (rlAddBanks != null)
            rlAddBanks.setOnClickListener(this);

        rlBalEnq = (RelativeLayout) findViewById(R.id.rlBalEnq);
        if (rlBalEnq != null)
            rlBalEnq.setOnClickListener(this);

        rlCngMPin = (RelativeLayout) findViewById(R.id.rlCngMPin);
        if (rlCngMPin != null)
            rlCngMPin.setOnClickListener(this);

        rlShowMMID = (RelativeLayout) findViewById(R.id.rlShowMMID);
        if (rlShowMMID != null)
            rlShowMMID.setOnClickListener(this);

        mPref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);

        swSelectDevice = (Switch) findViewById(R.id.swSelectDevice);
        String dm = mPref.getString("DeviceMac",EPSConstant.DEVICE_MAC1);
        swSelectDevice.setChecked(!EPSConstant.DEVICE_MAC1.equalsIgnoreCase(dm));
        swSelectDevice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor edit = mPref.edit();
                String DM = EPSConstant.DEVICE_MAC1;
                if(isChecked)
                    DM = EPSConstant.DEVICE_MAC2;
                edit.putString("DeviceMac",DM);
                edit.apply();
            }
        });

    }

    @Override
    public void onClick(View view) {
        if(view == ivBack)
        {
            finish();
            overridePendingTransition( R.anim.right_in, R.anim.right_out);
        } else if(view == ivInfo) {
            startActivity(new Intent(Settings.this,Information.class));
            overridePendingTransition( R.anim.left_in, R.anim.left_out);
        } else if(view == rlAddBanks){
            Intent in = new Intent(Settings.this, BankSetup.class);
            in.putExtra("showType","select");
            startActivity(in);
            overridePendingTransition( R.anim.left_in, R.anim.left_out);
        } else if(view == rlYourBanks){
            Intent in = new Intent(Settings.this, BankSetup.class);
            in.putExtra("showType","display");
            startActivity(in);
            overridePendingTransition( R.anim.left_in, R.anim.left_out);
        } else if(view == rlBalEnq){
            runUSSD("1");
        } else if(view == rlCngMPin){
            runUSSD("7");
        } else if(view == rlShowMMID){
            runUSSD("6");
        }
    }

    private void runUSSD(String Code){
        SharedPreferences cpref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);
        String bankid = cpref.getString("BankCodes", "");
        if (bankid.contains(",")) {
            selectBank(Code);
        } else {
            executeUSSD(bankid,Code);
        }
    }

    private void selectBank(String Code) {
        final Dialog dialog = new Dialog(Settings.this);
        final String ussdcode = Code;
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.grid_view);
        dialog.setTitle(getResources().getString(R.string.Settings_Popup_Title));
        // Prepare grid view
        GridView gridView = (GridView) dialog.findViewById(R.id.gridView);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        final String pbankCodes = mPref.getString("BankCodes", "");
        ImageAdapter adapter = new ImageAdapter(this, pbankCodes, "popup");
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                // adapter.onItemSelect(parent, v, position, id);
                String[] bankids = pbankCodes.split(",");
                executeUSSD(bankids[position],ussdcode);
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void executeUSSD(String bankid, String Code) {
        SharedPreferences tpref = getSharedPreferences("TransPreferences", Context.MODE_PRIVATE);
        String ussd = "*99*" + bankid + "*" + Code  + "#";
        String uriString = "";
        if (!ussd.startsWith("tel:"))
            uriString += "tel:";
        for (char c : ussd.toCharArray()) {
            if (c == '#')
                uriString += Uri.encode("#");
            else
                uriString += c;
        }
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uriString));
        startActivityForResult(callIntent, 121);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.right_in, R.anim.right_out);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Get Result from USSD
        if (requestCode == 121) {
            //Toast.makeText(getApplicationContext(),"ACTIVITY RESULT CALLED IN USSD", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
