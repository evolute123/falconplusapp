package com.eps.falconplus.Bluetooth;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import android.os.SystemClock;
import android.util.Log;

public class FalconDev {

	private static final String TAG = "Prowess FalconDev";
	private InputStream inSt = null;
	private OutputStream outSt = null;

	private static byte STX 			= (byte)0x8A;
	private static byte ETX 			= (byte)0x04;
	private static byte FalconMode 		= (byte)0x92;
	// Falcon-Plus
	private static byte InitTrans		= (byte)0x2A;
	private static byte TransStatus		= (byte)0x2B;
	private static byte StoreTermID		= (byte)0x2C;
	private static byte SendTermStatus	= (byte)0x2D;
	private static byte DispMerchtStat	= (byte)0x2E;
	private static byte ConfigMerchtID	= (byte)0x2F;
	private static byte ChDispName		= (byte)0x21;
	private static byte AmountMaxLimit	= (byte)0x3A;
	private static byte StoreMMID_PhNo	= (byte)0x3B;
	private static byte Logo_1_Print	= (byte)0x0B;
	private static byte Logo_2_Print	= (byte)0x0C;
	private static byte Logo_3_Print	= (byte)0x0D;
	private static byte Logo_4_Print	= (byte)0x0E;
	// Falcon-Ezy
	private static byte Diagnose		= (byte)0x01;
	private static byte TestPrint 		= (byte)0x02;
	private static byte PaperFeed 		= (byte)0x03;
	private static byte BarCode 		= (byte)0x05;
	private static byte ResetDevice		= (byte)0x11;
	private static byte IdentityCmd		= (byte)0x12;
	private static byte GetFirmware		= (byte)0x10;
	private static byte GetSerialNumber	= (byte)0x14;
	private static byte BatteryStatus	= (byte)0x13;
	private static byte AddData 		= (byte)0x04;
	private static byte GraphicsPrint	= (byte)0x06;
	private static byte ImagePrint		= (byte)0x0F;

	private static final long lInfoWaitTime10Min = 10*60*1000; // 10 Minutes
	private static final long l5SecWaitTime = 5*1000; // 5 Seconds

	public static final int SUCCESS = -11;
	public static final int FAILURE = -12;
	public static final int INPUT_ERROR = -13;
	public static final int PACKET_ERROR = -14;
	public static final int NO_RESPONSE = -15;
	public static final int DEVICE_TIMEOUT = -16;
	public static final int SEND_RECV_EXCEPTION = -17;
	public static final int MONITOR_CANCELED_BY_APP = -18;
	public static final int NO_TERMINAL_ID = -19;
	public static final int TRANSACTION_CANCELED = -20;

	public static final int FONT_ORIENTATION_MISMATCH = -71;
	public static final int LIMIT_EXCEEDED = -72;
	public static final int NO_DATA = -73;
	public static final int PAPER_OUT = -74;
	public static final int PLATEN_OPEN = -75;
	public static final int HEADTEMP_HIGH = -76;
	public static final int HEADTEMP_LOW = -77;
	public static final int IMPROPER_VOLTAGE = -78;
	public static final int FILE_ERROR = -79;

	private static final int MAX_LEN_TerminalID = 6;
	private static final int MAX_LEN_MerchantID = 6;
	private static final int MAX_LEN_TransactionID = 12;
	private static final int MAX_LEN_DisplayName = 16;
	private static final int MAX_LEN_Amount = 8;

	private class TextFont {
		public byte font;
		public String textData;
		public boolean smallfont;
		public byte[] bBinData = null;
		public boolean blTextInput = true;
	}
	private static Vector<TextFont> vTextFont;

	public FalconDev(InputStream inSt, OutputStream outSt) {
		this.inSt = inSt;
		this.outSt = outSt;
		vTextFont = new Vector<TextFont>();
	}

	public int iReset() {
		byte[] cmd = makeCommand(ResetDevice,null);
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt, l5SecWaitTime);
		/*if (bOut.length==1) {
			return bOut;
		} else {
			Log.e(TAG, "Error Occoured.. "+HexString.bufferToHex(bOut));
			return null;
		}*/
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}

	}

	public int iDiagnose () {
		byte[] cmd = makeCommand(Diagnose,null);
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt, l5SecWaitTime);
		/*if (bOut.length==1) {
			return bOut;
		} else {
			Log.e(TAG, "Error Occoured.. "+HexString.bufferToHex(bOut));
			return null;
		}*/
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}

	}
	private int iReturnCode = -999;
	public byte[] bGetFirmwareNo () {
		iReturnCode = -999;
		byte[] cmd = makeCommand(GetFirmware,null);
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt, l5SecWaitTime);
		if (bOut.length>1){
			iReturnCode = SUCCESS;
			return bOut;
		}else if(bOut.length==1){
			/*if (bOut[0]==(byte)0x80)
				iReturnCode = SUCCESS;
			else */
			if(bOut[0]==(byte)0x40)
				iReturnCode = FAILURE;
			else if(bOut[0]==(byte)0x60)
				iReturnCode = PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				iReturnCode = NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				iReturnCode = DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				iReturnCode = SEND_RECV_EXCEPTION;
			else
				iReturnCode = FAILURE;
			return null;
		}else {
			Log.e(TAG, "Error Occoured.. "+HexString.bufferToHex(bOut));
			iReturnCode = FAILURE;
			return null;
		}
	}
	public byte[] bInitiateTransaction () {
		iReturnCode = -999;
		byte[] cmd = makeCommand(InitTrans,null);
		Log.e(TAG, "bInitiateTransaction cmd..."+HexString.bufferToHex(cmd));
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt, lInfoWaitTime10Min);
		if (bOut.length>1){
			iReturnCode = SUCCESS;
			return bOut;
		}else if(bOut.length==1){
			/*if (bOut[0]==(byte)0x80)
				iReturnCode = SUCCESS;
			else */
			if(bOut[0]==(byte)0x40)
				iReturnCode = FAILURE;
			else if(bOut[0]==(byte)0x60)
				iReturnCode = PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				iReturnCode = NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				iReturnCode = DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x01)
				iReturnCode = NO_TERMINAL_ID;
			else if(bOut[0]==(byte)0x03)
				iReturnCode = TRANSACTION_CANCELED;
			else if(bOut[0]==(byte)0x06)
				iReturnCode = SEND_RECV_EXCEPTION;
			else
				iReturnCode = FAILURE;
			return null;
		}else {
			Log.e(TAG, "Error Occoured.. "+HexString.bufferToHex(bOut));
			iReturnCode = FAILURE;
			return null;
		}
	}

	public int iGetReturnCode(){
		return iReturnCode;
	}

	/*public enum Trans_Stat {Pending,Success,MerchantCancel,Declined,Timeout};*/
	public enum Trans_Stat {Pending,Success,Declined,Timeout,CANCEL_Accepted,CANCEL_Rejected};

	public int iSendTransactionStatus (Trans_Stat trstat, byte[] bTransID) {
		//We dont know the Transaction ID length to validate it
		if (bTransID==null||bTransID.length==0)//bTransID.length!=MAX_LEN_TransactionID
			return INPUT_ERROR;
		byte byt = 0;
		if (trstat==Trans_Stat.Pending) {
			byt = (byte)0x01;
		} else if(trstat==Trans_Stat.Success) {
			byt = (byte)0x02;
		/*}else if(trstat==Trans_Stat.MerchantCancel){
			byt = (byte)0x03;*/
		} else if(trstat==Trans_Stat.Declined) {
			byt = (byte)0x04;
		} else if(trstat==Trans_Stat.Timeout) {
			byt = (byte)0x05;
		} else if(trstat==Trans_Stat.CANCEL_Accepted) {
			byt = (byte)0x06;
		} else if(trstat==Trans_Stat.CANCEL_Rejected) {
			byt = (byte)0x07;
		} else {
			Log.e(TAG, "Invalid State Input");
			return INPUT_ERROR;
		}
		ByteBuffer buff = new ByteBuffer();
		buff.appendBytes(bTransID);
		buff.appendByte(byt);
		byte[] cmd = makeCommand(TransStatus,buff.getBuffer());
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt,l5SecWaitTime);
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}
		//return FAILURE;
	}

	public int iStoreTerminalID (byte[] bTerminalID) {
		if (bTerminalID==null||bTerminalID.length!=MAX_LEN_TerminalID){
			Log.e(TAG, "bStoreTerminalID Invalid Input ");
			return INPUT_ERROR;
		}
		byte[] cmd = makeCommand(StoreTermID,bTerminalID);
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt,l5SecWaitTime);
		/*if (bOut.length==1){
			return bOut;
		}else{
			Log.e(TAG, "Error Occoured.. "+HexString.bufferToHex(bOut));
			//return null;
		}
		return bOut;*/
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}
	}

	public enum Terminal_Stat {Pending,Success,Inactive,Deactive};

	public int iSendTerminalStatus (Terminal_Stat trstat,byte[] bTermID) {
		if (bTermID==null||bTermID.length!=MAX_LEN_TerminalID)
			return INPUT_ERROR;
		byte byt = 0;
		if (trstat==Terminal_Stat.Pending) {
			byt = (byte)0x01;
		} else if(trstat==Terminal_Stat.Success) {
			byt = (byte)0x02;
		} else if(trstat==Terminal_Stat.Inactive) {
			byt = (byte)0x03;
		} else if(trstat==Terminal_Stat.Deactive) {
			byt = (byte)0x04;
		} else {
			Log.e(TAG, "Invalid State Input");
			return INPUT_ERROR;
		}
		ByteBuffer buff = new ByteBuffer();
		buff.appendBytes(bTermID);
		buff.appendByte(byt);
		byte[] cmd = makeCommand(SendTermStatus,buff.getBuffer());
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt,l5SecWaitTime);
		/*if (bOut.length==1){
			return bOut;
		}else{
			Log.e(TAG, "Error Occoured.. "+HexString.bufferToHex(bOut));
		}
		return bOut;*/
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}
	}

	public enum Merchant_Stat {Pending,Active,Inactive,Closed,Cancelled};

	public int iDisplayMerchentStatus (Merchant_Stat trstat, byte[] MerchantID) {
		if (MerchantID==null||MerchantID.length!=MAX_LEN_MerchantID)
			return INPUT_ERROR;
		byte byt = 0;
		if (trstat==Merchant_Stat.Pending) {
			byt = (byte)0x01;
		} else if(trstat==Merchant_Stat.Active) {
			byt = (byte)0x02;
		} else if(trstat==Merchant_Stat.Inactive) {
			byt = (byte)0x03;
		} else if(trstat==Merchant_Stat.Closed) {
			byt = (byte)0x04;
		} else if(trstat==Merchant_Stat.Cancelled) {
			byt = (byte)0x05;
		} else {
			Log.e(TAG, "Invalid State Input");
			return INPUT_ERROR;
		}
		ByteBuffer buff = new ByteBuffer();
		buff.appendBytes(MerchantID);
		buff.appendByte(byt);
		byte[] cmd = makeCommand(DispMerchtStat,new byte[] {byt});
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt,l5SecWaitTime);
		/*if (bOut.length==1){
			return bOut;
		}else{
			Log.e(TAG, "Error Occoured.. "+HexString.bufferToHex(bOut));
		}
		return bOut;*/
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}
	}

	public int iConfigMerchantID (byte[] bMerchantID) {
		if (bMerchantID==null||bMerchantID.length!=MAX_LEN_MerchantID){
			Log.e(TAG, "bConfigMerchantID Invalid Input");
			return INPUT_ERROR;
		}
		byte[] cmd = makeCommand(ConfigMerchtID,bMerchantID);
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt,l5SecWaitTime);
		/*if (bOut.length==1){
			return bOut;
		}else{
			Log.e(TAG, "Error Occoured.. "+HexString.bufferToHex(bOut));
		}
		return bOut;*/
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}
	}

	public int iChangeDispName (byte[] bDispName) {
		if (bDispName==null||bDispName.length!=MAX_LEN_DisplayName){
			Log.e(TAG, "bChangeDispName Invalid Input");
			return INPUT_ERROR;
		}
		byte[] cmd = makeCommand(ChDispName,bDispName);
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt,l5SecWaitTime);
		/*if (bOut.length==1){
			return bOut;
		}else{
			Log.e(TAG, "Error Occoured.. "+HexString.bufferToHex(bOut));
		}
		return bOut;*/
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}
	}

	public int iSetAmountLimit (byte[] bMaxAmount) {
		if (bMaxAmount==null || bMaxAmount.length != MAX_LEN_Amount){
			Log.e(TAG, "iSetAmountLimit Invalid Input");
			return INPUT_ERROR;
		}
		byte[] cmd = makeCommand(AmountMaxLimit,bMaxAmount);
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt,l5SecWaitTime);
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}
	}

	public enum Select {Logo_1,Logo_2,Logo_3,Logo_4};
	/**
	 * To print the stored logos in the device
	 * @param select to select the logos
	 * <br>{@link Select#Logo_1}
	 * <br>{@link Select#Logo_2}
	 * <br>{@link Select#Logo_3}
	 * <br>{@link Select#Logo_4}
	 * @return {@linkplain Setup#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iPrintStoredLogo (Select select) {
		byte[] cmd  ;
		if (select==Select.Logo_1)
			cmd = makeCommand(Logo_1_Print, null);
		else if (select == Select.Logo_2)
			cmd = makeCommand(Logo_2_Print, null);
		else if (select == Select.Logo_3)
			cmd = makeCommand(Logo_3_Print, null);
		else if (select == Select.Logo_4)
			cmd = makeCommand(Logo_4_Print, null);
		else
			return INPUT_ERROR;
		byte[] bOut = bSendRecvCommand(cmd, outSt, inSt,l5SecWaitTime);
		if (bOut!=null && bOut.length==1){
			if (bOut[0]==(byte)0x80)
				return SUCCESS;
			else if(bOut[0]==(byte)0x40)
				return FAILURE;
			else if(bOut[0]==(byte)0x60)
				return PACKET_ERROR;
			else if(bOut[0]==(byte)0x14)
				return NO_RESPONSE;
			else if(bOut[0]==(byte)0x05)
				return DEVICE_TIMEOUT;
			else if(bOut[0]==(byte)0x06)
				return SEND_RECV_EXCEPTION;
			else
				return FAILURE;
		} else {
			Log.e(TAG, "Error Occoured.. ");
			return FAILURE;
		}
	}

	private byte[] makeCommand(byte cmdType,byte[] cmdData){
		try {
			ByteBuffer cmdBuffer = new ByteBuffer();
			cmdBuffer.appendByte(STX);
			cmdBuffer.appendByte(FalconMode);
			cmdBuffer.appendByte(cmdType);
			int len =0;
			if (cmdData != null)
				len = cmdData.length;
			byte len_0 = (byte) (0x000000FF & len);
			byte len_1 = (byte) (0x000000FF & (len>>8));
			cmdBuffer.appendByte(len_0);
			cmdBuffer.appendByte(len_1);
			if(len>0){
				cmdBuffer.appendBytes(cmdData);
			}
			cmdBuffer.appendByte(ETX);
			byte[] cmdFinal = cmdBuffer.getBuffer();
			return cmdFinal;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	private byte[] bSendRecvCommand(byte[] bCommand, OutputStream outSt, InputStream inSt,long lTimeout){
		try {
			byte[] bRespBytes = new byte[500];
			int iResLen=0;
			synchronized (inSt){
				outSt.flush();
				inSt.skip(inSt.available());
				String str = HexString.bufferToHex(bCommand);
				Log.d(TAG, str);
				outSt.write(bCommand);
				SystemClock.sleep(500);
				startTimer(lTimeout);
				Log.d(TAG, "Response Length : "+inSt.available());
				do {
					if (toWaitTillTimeOutForStopRecv){
						cancelTimer();
						//return NO_RESPONSE;
						return new byte[] {(byte)0x14};
					}
					SystemClock.sleep(100);
				} while(inSt.available()<=0);
				cancelTimer();
				/*bRespBytes = iReadNoofBytes(7, inSt);
				if (bRespBytes==null)
					return Setup.FAILURE;*/
				iResLen=inSt.read(bRespBytes);
			}

			if (bRespBytes[0]!=STX || bRespBytes[1]!=FalconMode ) {
				Log.e(TAG, "Invalid Data");
				//return FAILURE;
			}
			Log.d(TAG, "Response Read : "+HexString.bufferToHex(bRespBytes,0,iResLen));
			//int result=inSt.read(bRespBytes);
			//buffer.appendBytes(bytebuff);
			//Log.d(TAG, "Response data: "+HexString.bufferToHex(bytebuff,0,result));
			if (bRespBytes[3]==(byte)0x80){
				int l = bRespBytes[4] & 0x000000FF;
				int h = bRespBytes[5] & 0x000000FF;
				int iDataLen = h*256+l;
				if (iDataLen>1){
					Log.i(TAG, "Received 0x80 with data Len: "+iDataLen);
					byte[] bOut = new byte[iDataLen];
					System.arraycopy(bRespBytes, 6, bOut, 0, iDataLen);
					return bOut;
				}else {
					Log.i(TAG, "Received 0x80 Without data");
					return new byte[] {bRespBytes[3]};
				}
			} else if (bRespBytes[3]==(byte)0x05){
				Log.i(TAG, "Device Timeout Occourred");
				return new byte[] {bRespBytes[3]};//0x05
			} else if (bRespBytes[3]==(byte)0x60){
				Log.i(TAG, "Packet Error Occourred");
				return new byte[] {bRespBytes[3]};//0x05
			} else {
				return new byte[] {bRespBytes[3]};
			}
		} catch (Exception e) {
			e.printStackTrace();
			//return FAILURE;
			return new byte[] {(byte)0x06};
		}
	}

	private static boolean getFontOrientation(byte font) {
		Log.d(TAG, "getFontOrientation Font : "+font);
		int x= 0x000000FF&font;
		if ((x&(byte)0x000000D0) == 0x000000D0) {
			Log.d(TAG, "getFontOrientation True: "+x);
			return true;
		} else {
			Log.d(TAG, "getFontOrientation False : "+x);
			return false;
		}
	}

	/**
	 * To add the data to printer buffer.
	 * Large data along with required font can be sent by the user
	 * @param bFont Specifies the font type. The supported fonts are,<br>
	 * {@linkplain Font#LARGE_BOLD}<br>{@linkplain Font#LARGE_NORMAL}<br>
	 * {@linkplain Font#SMALL_BOLD}<br>{@linkplain Font#SMALL_NORMAL}<br>
	 * {@linkplain Font#UL_LARGE_BOLD}<br>{@linkplain Font#UL_LARGE_NORMAL}<br>
	 * {@linkplain Font#UL_SMALL_BOLD}<br>{@linkplain Font#UL_SMALL_NORMAL}<br>
	 */
	 /*{@linkplain Font#LARGE_BOLD_180}<br>{@linkplain Font#LARGE_NORMAL_180}<br>
	 * {@linkplain Font#SMALL_BOLD_180}<br>{@linkplain Font#SMALL_NORMAL_180}<br>
	 * {@linkplain Font#UL_LARGE_BOLD_180}<br>{@linkplain Font#UL_LARGE_NORMAL_180}<br>
	 * {@linkplain Font#UL_SMALL_BOLD_180}<br>{@linkplain Font#UL_SMALL_NORMAL_180}<br>
	 * @param stTextData Text data to be printed
	 * @return {@linkplain Setup#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iAddData(Font bFont, String stTextData) {
		Log.d(TAG, "iAddData");
		if (stTextData==null||stTextData.length()==0)
			return INPUT_ERROR;
		boolean smallfont=false;
		byte FONT_TYPE = 0;

		boolean revFont180=false;
		if (bFont == Font.LARGE_NORMAL ) {//FONT_LARGE_NORMAL) {
			smallfont = false;		FONT_TYPE = (byte) 0X90;	revFont180 = false;
		} else if (bFont == Font.LARGE_BOLD ) {//FONT_LARGE_BOLD) {
			smallfont = false;		FONT_TYPE = (byte) 0X91;	revFont180 = false;
		} else if (bFont == Font.SMALL_NORMAL ) {//FONT_SMALL_NORMAL) {
			smallfont = true;		FONT_TYPE = (byte) 0X92;	revFont180 = false;
		} else if (bFont == Font.SMALL_BOLD ) {//FONT_SMALL_BOLD) {
			smallfont = true;		FONT_TYPE = (byte) 0X93;	revFont180 = false;
		} else if (bFont == Font.UL_LARGE_NORMAL ) {//FONT_UL_LARGE_NORMAL) {
			smallfont = false;		FONT_TYPE = (byte) 0X94;	revFont180 = false;
		} else if (bFont == Font.UL_LARGE_BOLD ) {//FONT_UL_LARGE_BOLD) {
			smallfont = false;		FONT_TYPE = (byte) 0X95;	revFont180 = false;
		} else if (bFont == Font.UL_SMALL_NORMAL ) {//FONT_UL_SMALL_NORMAL) {
			smallfont = true;		FONT_TYPE = (byte) 0X96;	revFont180 = false;
		} else if (bFont == Font.UL_SMALL_BOLD ) {//FONT_UL_SMALL_BOLD) {
			smallfont = true;		FONT_TYPE = (byte) 0X97;	revFont180 = false;
		} /*else if (bFont == Font.LARGE_NORMAL_180 ) {//FONT_180_LARGE_NORMAL) {
			smallfont = false;		FONT_TYPE = (byte) 0XD0;	revFont180 = true;
		} else if (bFont == Font.LARGE_BOLD_180 ) {//FONT_180_LARGE_BOLD) {
			smallfont = false;		FONT_TYPE = (byte) 0XD1;	revFont180 = true;
		} else if (bFont == Font.SMALL_NORMAL_180 ) {//FONT_180_SMALL_NORMAL) {
			smallfont = true;		FONT_TYPE = (byte) 0XD2;	revFont180 = true;
		} else if (bFont == Font.SMALL_BOLD_180 ) {//FONT_180_SMALL_BOLD) {
			smallfont = true;		FONT_TYPE = (byte) 0XD3;	revFont180 = true;
		} else if (bFont == Font.UL_LARGE_NORMAL_180 ) {//FONT_180_UL_LARGE_NORMAL) {
			smallfont = false;		FONT_TYPE = (byte) 0XD4;	revFont180 = true;
		} else if (bFont == Font.UL_LARGE_BOLD_180 ) {//FONT_180_UL_LARGE_BOLD) {
			smallfont = false;		FONT_TYPE = (byte) 0XD5;	revFont180 = true;
		} else if (bFont == Font.UL_SMALL_NORMAL_180 ) {//FONT_180_UL_SMALL_NORMAL) {
			smallfont = true;		FONT_TYPE = (byte) 0XD6;	revFont180 = true;
		} else if (bFont == Font.UL_SMALL_BOLD_180 ) {//FONT_180_UL_SMALL_BOLD) {
			smallfont = true;		FONT_TYPE = (byte) 0XD7;	revFont180 = true;
		}*/

		TextFont tempPrintingTextFont = new TextFont();
		tempPrintingTextFont.font = FONT_TYPE;
		tempPrintingTextFont.textData = stTextData;
		tempPrintingTextFont.smallfont = smallfont;
		tempPrintingTextFont.blTextInput = true;
		Log.d(TAG, "Added data : \n"+stTextData);
		if (!vTextFont.isEmpty()){
			int sz = vTextFont.size()-1;
			TextFont oldContent = (TextFont)vTextFont.get(sz);
			byte oldFont = oldContent.font;
			boolean oldOrientation = getFontOrientation(oldFont);
			byte currFont = tempPrintingTextFont.font;
			boolean currOrientation = getFontOrientation(currFont);
			if (oldOrientation != currOrientation){
				Log.d(TAG, "ORIENTATION MISMATCH");
				return FONT_ORIENTATION_MISMATCH;
			} else {
				Log.d(TAG, "-------- SAME ORIENTATION ---------");
			}
		}
		vTextFont.addElement(tempPrintingTextFont);//adding element of data
		return SUCCESS;
	}

	/**
	 * To add the binary data to buffer including symbols.<br>
	 * The data length should not exceed 24/42 bytes as per the LARGE/SMALL fonts respectively.
	 * Only one line data can be added to buffer in one call to the API.
	 * For multiple lines multiple calls are required.
	 * @param bFont Specifies the font type. The supported fonts are,<br>
	 * {@linkplain Font#LARGE_BOLD}<br>{@linkplain Font#LARGE_NORMAL}<br>
	 * {@linkplain Font#SMALL_BOLD}<br>{@linkplain Font#SMALL_NORMAL}<br>
	 * {@linkplain Font#UL_LARGE_BOLD}<br>{@linkplain Font#UL_LARGE_NORMAL}<br>
	 * {@linkplain Font#UL_SMALL_BOLD}<br>{@linkplain Font#UL_SMALL_NORMAL}<br>
	 * @param bData Binary Data to be printed with symbols
	 * @return {@linkplain Setup#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iAddData(Font bFont, byte[] bData) {
		Log.d(TAG, "iAddData Binary");
		if (bData==null||bData.length==0)
			return INPUT_ERROR;

		boolean smallfont=false;
		byte FONT_TYPE = 0;

		boolean revFont180=false;
		if (bFont == Font.LARGE_NORMAL ) {//FONT_LARGE_NORMAL) {
			smallfont = false;		FONT_TYPE = (byte) 0X90;	revFont180 = false;
		} else if (bFont == Font.LARGE_BOLD ) {//FONT_LARGE_BOLD) {
			smallfont = false;		FONT_TYPE = (byte) 0X91;	revFont180 = false;
		} else if (bFont == Font.SMALL_NORMAL ) {//FONT_SMALL_NORMAL) {
			smallfont = true;		FONT_TYPE = (byte) 0X92;	revFont180 = false;
		} else if (bFont == Font.SMALL_BOLD ) {//FONT_SMALL_BOLD) {
			smallfont = true;		FONT_TYPE = (byte) 0X93;	revFont180 = false;
		} else if (bFont == Font.UL_LARGE_NORMAL ) {//FONT_UL_LARGE_NORMAL) {
			smallfont = false;		FONT_TYPE = (byte) 0X94;	revFont180 = false;
		} else if (bFont == Font.UL_LARGE_BOLD ) {//FONT_UL_LARGE_BOLD) {
			smallfont = false;		FONT_TYPE = (byte) 0X95;	revFont180 = false;
		} else if (bFont == Font.UL_SMALL_NORMAL ) {//FONT_UL_SMALL_NORMAL) {
			smallfont = true;		FONT_TYPE = (byte) 0X96;	revFont180 = false;
		} else if (bFont == Font.UL_SMALL_BOLD ) {//FONT_UL_SMALL_BOLD) {
			smallfont = true;		FONT_TYPE = (byte) 0X97;	revFont180 = false;
		}/* else if (bFont == Font.LARGE_NORMAL_180 ) {//FONT_180_LARGE_NORMAL) {
			smallfont = false;		FONT_TYPE = (byte) 0XD0;	revFont180 = true;
		} else if (bFont == Font.LARGE_BOLD_180 ) {//FONT_180_LARGE_BOLD) {
			smallfont = false;		FONT_TYPE = (byte) 0XD1;	revFont180 = true;
		} else if (bFont == Font.SMALL_NORMAL_180 ) {//FONT_180_SMALL_NORMAL) {
			smallfont = true;		FONT_TYPE = (byte) 0XD2;	revFont180 = true;
		} else if (bFont == Font.SMALL_BOLD_180 ) {//FONT_180_SMALL_BOLD) {
			smallfont = true;		FONT_TYPE = (byte) 0XD3;	revFont180 = true;
		} else if (bFont == Font.UL_LARGE_NORMAL_180 ) {//FONT_180_UL_LARGE_NORMAL) {
			smallfont = false;		FONT_TYPE = (byte) 0XD4;	revFont180 = true;
		} else if (bFont == Font.UL_LARGE_BOLD_180 ) {//FONT_180_UL_LARGE_BOLD) {
			smallfont = false;		FONT_TYPE = (byte) 0XD5;	revFont180 = true;
		} else if (bFont == Font.UL_SMALL_NORMAL_180 ) {//FONT_180_UL_SMALL_NORMAL) {
			smallfont = true;		FONT_TYPE = (byte) 0XD6;	revFont180 = true;
		} else if (bFont == Font.UL_SMALL_BOLD_180 ) {//FONT_180_UL_SMALL_BOLD) {
			smallfont = true;		FONT_TYPE = (byte) 0XD7;	revFont180 = true;
		}*/

		if (smallfont && bData.length>42)
			return LIMIT_EXCEEDED;
		else if (!smallfont && bData.length>24)
			return LIMIT_EXCEEDED;

		TextFont tempPrintingTextFont = new TextFont();
		tempPrintingTextFont.font = FONT_TYPE;
		//tempPrintingTextFont.textData = stTextData;
		//  123456789012345678901234567890123456789012
		byte[] bSp42 = "                                          ".getBytes();
		byte[] bSp24 = "                        ".getBytes();
		int len = 0;
		if (smallfont) {
			Log.d(TAG, "Binary Small Font 42");
			len = 42;
			tempPrintingTextFont.bBinData = new byte[len];
			System.arraycopy(bSp42, 0, tempPrintingTextFont.bBinData, 0, len);
		} else {
			Log.d(TAG, "Binary Small Font 24");
			len = 24;
			tempPrintingTextFont.bBinData = new byte[len];
			System.arraycopy(bSp24, 0, tempPrintingTextFont.bBinData, 0, len);
		}
		System.arraycopy(bData, 0, tempPrintingTextFont.bBinData, 0, bData.length);
		//tempPrintingTextFont.bBinData = bTextData;
		tempPrintingTextFont.smallfont = smallfont;
		tempPrintingTextFont.blTextInput = false;
		Log.d(TAG, "Added BIN data : \n"+HexString.bufferToHex(tempPrintingTextFont.bBinData));
		if (!vTextFont.isEmpty()) {
			int sz = vTextFont.size()-1;
			TextFont oldContent = (TextFont)vTextFont.get(sz);
			byte oldFont = oldContent.font;
			boolean oldOrientation = getFontOrientation(oldFont);
			byte currFont = tempPrintingTextFont.font;
			boolean currOrientation = getFontOrientation(currFont);
			if (oldOrientation != currOrientation){
				//	return OrientationMismatch;
				Log.d(TAG, "ORIENTATION MISMATCH");
				return FONT_ORIENTATION_MISMATCH;
			} else {
				Log.d(TAG, "-------- SAME ORIENTATION ---------");
			}
		}
		vTextFont.addElement(tempPrintingTextFont);//adding element of data
		return SUCCESS;
	}


	/**
	 * To select the Font supported by the printer. They are,<br>
	 * {@linkplain Font#LARGE_BOLD}<br>{@linkplain Font#LARGE_NORMAL}<br>
	 * {@linkplain Font#SMALL_BOLD}<br>{@linkplain Font#SMALL_NORMAL}<br>
	 * {@linkplain Font#UL_LARGE_BOLD}<br>{@linkplain Font#UL_LARGE_NORMAL}<br>
	 * {@linkplain Font#UL_SMALL_BOLD}<br>{@linkplain Font#UL_SMALL_NORMAL}<br>
	 */
/*	 * {@linkplain Font#LARGE_BOLD_180}<br>{@linkplain Font#LARGE_NORMAL_180}<br>
	 * {@linkplain Font#SMALL_BOLD_180}<br>{@linkplain Font#SMALL_NORMAL_180}<br>
	 * {@linkplain Font#UL_LARGE_BOLD_180}<br>{@linkplain Font#UL_LARGE_NORMAL_180}<br>
	 * {@linkplain Font#UL_SMALL_BOLD_180}<br>{@linkplain Font#UL_SMALL_NORMAL_180}<br>
	 */
	public enum Font {
		/**
		 * Large normal font 24 chars per line
		 */
		LARGE_NORMAL, 			//= (byte) 0x01;
		/**
		 * Large bold font 24 chars per line
		 */
		LARGE_BOLD, 			//= (byte) 0x02;
		/**
		 * Small normal font 42 chars per line
		 */
		SMALL_NORMAL, 			//= (byte) 0x03;
		/**
		 * Small bold font 42 chars per line
		 */
		SMALL_BOLD, 			//= (byte) 0x04;
		/**
		 * Large normal font with under line 24 chars per line
		 */
		UL_LARGE_NORMAL,	 	//= (byte) 0x05;
		/**
		 * Large bold font with under line 24 chars per line
		 */
		UL_LARGE_BOLD, 			//= (byte) 0x06;
		/**
		 * Small normal font with under line 42 chars per line
		 */
		UL_SMALL_NORMAL, 		//= (byte) 0x07;
		/**
		 * Small bold font with under line 42 chars per line
		 */
		UL_SMALL_BOLD, 			//= (byte) 0x08;
		/* *
		 * Large normal font with 180 rotation 24 chars per line
		 */
		//LARGE_NORMAL_180,	 	//= (byte) 0x09;
		/* *
		 * Large bold font with 180 rotation 24 chars per line
		 */
		//LARGE_BOLD_180, 		//= (byte) 0x0A;
		/* *
		 * Small normal font with 180 rotation 42 chars per line
		 */
		//SMALL_NORMAL_180, 		//= (byte) 0x0B;
		/* *
		 * Small bold font with 180 rotation 42 chars per line
		 */
		//SMALL_BOLD_180, 		//= (byte) 0x0C;
		/* *
		 * Large normal font with 180 rotation and under line 24 chars per line
		 */
		//UL_LARGE_NORMAL_180, 	//= (byte) 0x0D;
		/* *
		 * Large bold font with 180 rotation and under line 24 chars per line
		 */
		//UL_LARGE_BOLD_180,		//= (byte) 0x0E;
		/* *
		 * Small normal font with 180 rotation and under line 42 chars per line
		 */
		//UL_SMALL_NORMAL_180,	//= (byte) 0x0F;
		/* *
		 * Small bold font with 180 rotation and under line 42 chars per line
		 */
		//UL_SMALL_BOLD_180 		//= (byte) 0x10;
	}

	/**
	 * Clears the content from the buffer
	 * @return {@linkplain Setup#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iFlushBuf() {
		Log.d(TAG, "iFlushBuf");
		vTextFont.removeAllElements();
		Log.d(TAG,"REMOVE ALL ELEMENTS CLEAR " );
		return SUCCESS;
	}

	/**
	 * Starts the printing the data stored in the buffer
	 * @param iNoofPrints Number of times to print
	 * @return {@linkplain Setup#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iStartPrinting(int iNoofPrints){
		Log.d(TAG, "iStartPrinting");
		return  iContinuosPrinting1(iNoofPrints);
	}

	private int iContinuosPrinting1(int Noofprints) {
		int ret = 0;
		if (vTextFont.isEmpty()){
			return NO_DATA;
		}
		//if (Noofprints>1){
		for (int i=0; i<Noofprints; i++) {
			TextFont tf = (TextFont)vTextFont.get(0);
			ret=loopToPrint1();
			if (ret!=SUCCESS)
				break;
		}
		//} else {
		//	ret = loopToPrint();
		//}
		return ret;
	}


	private static byte[] appenbyteFrnt(byte container, byte[] data) {
		byte[] dat = new byte[data.length + 1 + 1];
		dat[0] = container;
		dat[1] = 01;
		System.arraycopy(data, 0, dat, 2, data.length);
		return dat;
	}

	private static String[] splitStringEvery(String s, int interval) {
		if (s.length()==0){
			String s1[] = new String[1];
			s1[0] = "" ;
			for (int x=0;x<interval;x++)
				s1[0] = s1[0].concat(" ");//s1[0] = s1[0].concat("^");
			return s1;
		}
		int arrayLength = (int) Math.ceil(((s.length() / (double)interval)));
		String[] result = new String[arrayLength];

		int j = 0;
		int lastIndex = result.length - 1;
		for (int i = 0; i < lastIndex; i++) {
			result[i] = s.substring(j, j + interval);
			j += interval;
		} //Add the last bit
		result[lastIndex] = s.substring(j);
		if (result[lastIndex].length()!=interval){
			//padData(lines, padLength)
	    	/*
	    	 byte[] temp = ((String) lines.elementAt(i)).getBytes();
			int len = temp.length;
			len = (len > padLength) ? padLength : len;
			System.arraycopy(temp, 0, data[i], 0, len);
			for (int j = (padLength - 1); j >= len; j--) {
				data[i][j] = (byte) 0x20;
			}
	    	 */
			byte[] lastString = new byte[interval];
			byte[] temp = result[lastIndex].getBytes();
			int len = temp.length;
			len = (len > interval) ? interval : len;
			System.arraycopy(temp, 0, lastString, 0, len);
			for (int k = (interval - 1); k >= len; k--) {
				lastString[k] = (byte) 0x20;
			}
			result[lastIndex] = new String(lastString);
		}

		return result;
	}
	private int loopToPrint1() {
		int totalSize = vTextFont.size();
		//byte[][] packetsToPrint=null;
		if (vTextFont.isEmpty()){
			return NO_DATA;
		}
		int k = 0;
		Log.d(TAG,">>Total Elements: "+totalSize);
		try {
			ArrayList<byte[]> arrList = new ArrayList<byte[]>();
			for(int ci=0;ci<vTextFont.size();ci++){
				Log.d(TAG,">>> ");

				TextFont tmpTextFont = (TextFont)vTextFont.get(ci);
				byte fnt =  tmpTextFont.font;
				if (tmpTextFont.blTextInput==false) {
					byte[] bTmp1 = appenbyteFrnt(fnt, tmpTextFont.bBinData);
					arrList.add(bTmp1);
					Log.d(TAG,">>> >> > CONTINUING DUE TO BINARY DATA ");
					continue;
				}

				String st[] = tmpTextFont.textData.split("\n", -1);
				boolean smallfont=  tmpTextFont.smallfont;
				//boolean rev = currTxtFnt.elementAt(ci).rev;

				boolean rev = getFontOrientation(tmpTextFont.font);
				Log.d(TAG,"     >>> Current Font <"+HexString.bufferToHex(new byte[]{fnt})+">, Orientation rev: "+rev);
				if (!rev){
					for (int li=0;li<st.length;li++){
						String[] sEachLine ;
						if (smallfont)
							sEachLine = splitStringEvery(st[li], 42);
						else
							sEachLine = splitStringEvery(st[li], 24);
						for (int ix=0;ix<sEachLine.length;ix++) {
							byte[] bTmp = sEachLine[ix].getBytes();
							bTmp = appenbyteFrnt(fnt, bTmp);
							Log.d(TAG,">>> >> > "+sEachLine[ix]);
							arrList.add(bTmp);
							Log.d(TAG,">>> "+HexString.bufferToHex(bTmp));
						}
					}
				} else {
					for (int li=(st.length-1);li>=0;li--){
						String[] sEachLine ;
						if (smallfont)
							sEachLine = splitStringEvery(st[li], 42);
						else
							sEachLine = splitStringEvery(st[li], 24);
						for(int ix=(sEachLine.length-1);ix>=0;ix--) {
							byte[] bTmp = sEachLine[ix].getBytes();
							bTmp = appenbyteFrnt(fnt, bTmp);
							Log.d(TAG,"<<< << < "+sEachLine[ix]);
							arrList.add(bTmp);
							Log.d(TAG,"<<< "+HexString.bufferToHex(bTmp));
						}
					}
				}
			}
			Log.d(TAG,"");
			Log.d(TAG,"<><><><><><><><><><><><><><><><><><><><><><><><><><><><>");
			Log.d(TAG,"");

			byte[][] arrPackets = new byte[arrList.size()][];

			for (int i=0;i<arrList.size();i++) {
				arrPackets[i] = arrList.get(i);
				Log.d(TAG,"===>>> <"+i+">: "+new String(arrPackets[i],2,arrPackets[i].length-2));
			}
			Log.d(TAG,"");
			int totini=0;
			for (int i=0;i<arrPackets.length;i++) {
				Log.d(TAG,":::>>> Len: "+arrPackets[i].length+" "+ HexString.bufferToHex(arrPackets[i])+", "+(i+1));
				totini+=arrPackets[i].length;
			}
			Log.d(TAG,"Total Len: "+totini);
			Log.d(TAG,"");
			Log.d(TAG,"<><><><><><><><><><><><><><><><><><><><><><><><><><><><>");
			Log.d(TAG,"");

			int iMaxPackets=50;
			if (arrPackets.length<=iMaxPackets) {
				ByteBuffer tbuf = new ByteBuffer();
				for (byte[] tb:arrPackets)
					tbuf.appendBytes(tb);
				byte[] prnData = tbuf.getBuffer();
				prnData =  makeCommand(AddData, prnData);
				//byte[] toPrint = makeCommand((byte)0x15, tbuf.getBuffer());
				Log.d(TAG,">>>>>>>>>> "+HexString.bufferToHex(prnData));
				byte[][] bb = new byte[1][];
				bb[0] = prnData;
				int f = sendPrinterPacketsData(bb);
				return f;
			} else {
				Log.d(TAG,":::>>> Multiple Packets: "+arrPackets.length+" Total Length: "+totini);
				int q = arrPackets.length/iMaxPackets;
				int r = arrPackets.length%iMaxPackets;
				int lim=q;
				if (r>0)
					lim++;
				Log.d(TAG,":::>>> q:"+q+", r:"+r+", lim:"+lim);
				byte[][] packets = new byte[lim][];
				//int totlen = arrPackets.length;
				int i=0;
				int j=0;

				ByteBuffer bTmpBuf = new ByteBuffer();
				bTmpBuf.appendBytes(arrPackets[0]);
				int totnow=0;
				String sLogIndex="";
				do {
					if (i==0) {
						i++;
						continue;
					}
					//System.out.printf(" i:%3d",i);
					String s = String.format(" i:%3d",i);
					sLogIndex = sLogIndex.concat(s);
					if (i%iMaxPackets==0){
						packets[j++] = bTmpBuf.getBuffer();
						//System.out.printf(" >r:%2d Len: %d\n",r,packets[j-1].length);
						String s1 = String.format(" >r:%2d Len: %d\n",r,packets[j-1].length);
						sLogIndex = sLogIndex.concat(s1);
						totnow += packets[j-1].length;
						packets[j-1] =  makeCommand(AddData, packets[j-1]);
						Log.d(TAG,">> "+sLogIndex+", cmdLen: "+packets[j-1].length);
						sLogIndex="";
						bTmpBuf = new ByteBuffer();
					}
					bTmpBuf.appendBytes(arrPackets[i]);
					if (i==(arrPackets.length-1)){
						packets[j++] = bTmpBuf.getBuffer();
						//System.out.printf(" >>r:%2d Len: %d\n",r,packets[j-1].length);
						String s1 = String.format(" >r:%2d Len: %d\n",r,packets[j-1].length);
						sLogIndex = sLogIndex.concat(s1);
						totnow += packets[j-1].length;
						packets[j-1] =  makeCommand(AddData, packets[j-1]);
						Log.d(TAG,">> "+sLogIndex+", cmdLen: "+packets[j-1].length);
						sLogIndex="";
						bTmpBuf = new ByteBuffer();
					}
					i++;
				} while (i<arrPackets.length);

				if (j==lim){
					Log.d(TAG,":::>>> Total Now: "+totnow);
					Log.d(TAG,":::>>> Everthing looks ok....! " +j);
				} else {
					Log.d(TAG,":::>>> Everthing NOT ok....! " +j);
				}
				k = sendPrinterPacketsData(packets);
				return k;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	private int sendPrinterPacketsData(byte[][] packets ) {
		int x = FAILURE;
		for(int j=0;j<packets.length;j++){
			Log.d(TAG, "Current packet : "+HexString.bufferToHex(packets[j]));
			x = iSendRecvCommandFast(packets[j],l5SecWaitTime);
			if (x != SUCCESS)
				return x;
		}
		return x;
	}

	private int iSendRecvCommandFast(byte[] bCommand,long lTimeOut){
		byte[] bRespBytes = new byte[15];
		int iResLen=0;
		toWaitTillTimeOutForStopRecv = false;
		try {
			outSt.flush();
			inSt.skip(inSt.available());
			outSt.write(bCommand);
			SystemClock.sleep(100);//200asdsadhsadkjsahdkjh
			startTimer(lTimeOut);
			do {
				if (toWaitTillTimeOutForStopRecv){
					cancelTimer();
					return NO_RESPONSE;
				}
				SystemClock.sleep(200);
			} while(inSt.available()<=0);

			cancelTimer();

			Log.d(TAG, "Response Length : "+inSt.available());
			SystemClock.sleep(25);
			iResLen = inSt.read(bRespBytes);
			Log.d(TAG, "Response : "+HexString.bufferToHex(bRespBytes));//,0,iResLen));
			/*if (iResLen<4)
				return Setup.FAILURE;*/
			if (bRespBytes[0]!=STX || bRespBytes[1]!=FalconMode)
				return FAILURE;
			int iRet=iVerifyRetCode(bRespBytes[3]);
			return iRet;
		} catch (Exception e) {
			e.printStackTrace();
			return FAILURE;
		}
	}

	private int iVerifyRetCode(byte bRespRecv) {
		Log.d(TAG,"iVerifyRetCode");
		if(bRespRecv==(byte)0x80){
			return SUCCESS;
		} else if (bRespRecv==(byte)0x60){
			vDoDelay();
			return PACKET_ERROR;
		} else if (bRespRecv==(byte)0x41){
			vDoDelay();
			return PAPER_OUT;
		} else if (bRespRecv==(byte)0x42){
			vDoDelay();
			return PLATEN_OPEN;
		} else if (bRespRecv==(byte)0x48){
			vDoDelay();
			return HEADTEMP_HIGH;
		} else if (bRespRecv==(byte)0x44){
			vDoDelay();
			return HEADTEMP_LOW;
		} else if (bRespRecv==(byte)0x50){
			vDoDelay();
			return IMPROPER_VOLTAGE;
		} else
			return FAILURE;
	}
	private void vDoDelay(){
		try {
			SystemClock.sleep(2000);
		} catch (Exception e) {
			return;
		}
	}

	private int get2Len(byte[] data, int pos, boolean isMSBLSB) {
		byte[] len = new byte[2];
		if (isMSBLSB) {
			len[0] = data[pos];
			len[1] = data[pos + 1];
		} else {
			len[0] = data[pos + 1];
			len[1] = data[pos];
		}
		return Integer.parseInt(HexString.bufferToHex(len), 16);
	}

	private int get4Len(byte[] data, int pos, boolean isMSBLSB) {
		byte[] len = new byte[4];
		if (isMSBLSB) {
			len[0] = data[pos];
			len[1] = data[pos + 1];
			len[2] = data[pos + 2];
			len[3] = data[pos + 3];
		} else {
			len[0] = data[pos + 3];
			len[1] = data[pos + 2];
			len[2] = data[pos + 1];
			len[3] = data[pos];
		}
		return Integer.parseInt(HexString.bufferToHex(len), 16);
	}

	private class BmpInfo {
		int headerType=0;
		int headerSize=0;
		int offSet=0;
		int infosize=0;
		int infoWidth=0;
		int infoHieght=0;
		boolean bit24=true;
		int infoBits=0;
		int imageSize=0;
	}

	private BmpInfo getBitmapInformation(InputStream inStream) {
		byte[] headerBytes = new byte[14];
		byte[] infoHeaderBytes = new byte[40];
		InputStream is = null;
		BmpInfo bmpInfo = null;
		try {
			bmpInfo = new BmpInfo();
			is = inStream;
			int count = is.read(headerBytes, 0, 14);
			if ((count <= 0) && (count != 14)) {
				is.close();
				return null;
			}
			byte[] type = new byte[2];
			System.arraycopy(headerBytes, 0, type, 0, 2);
			bmpInfo.headerType = get2Len(type, 0, false);
			byte[] size = new byte[4];
			System.arraycopy(headerBytes, 2, size, 0, 4);
			bmpInfo.headerSize = get4Len(size, 0, false);
			byte[] offset = new byte[4];
			System.arraycopy(headerBytes, 10, offset, 0, 4);
			bmpInfo.offSet = get4Len(offset, 0, false);
			if (19778 != bmpInfo.headerType) {
				is.close();
				return null;
			}
			int infoCount = is.read(infoHeaderBytes, 0, 40);
			if ((infoCount <= 0) && (infoCount != 40)) {
				is.close();
				return null;
			}
			//Log.d("Image Info", "Data is :\n"+HexString.bufferToHex(infoHeaderBytes));
			byte[] sizeInfo = new byte[4];
			System.arraycopy(infoHeaderBytes, 0, sizeInfo, 0, 4);
			bmpInfo.infosize = get4Len(sizeInfo, 0, false);
			byte[] width = new byte[4];
			System.arraycopy(infoHeaderBytes, 4, width, 0, 4);
			bmpInfo.infoWidth = get4Len(width, 0, false);
			byte[] hieght = new byte[4];
			System.arraycopy(infoHeaderBytes, 8, hieght, 0, 4);
			bmpInfo.infoHieght = get4Len(hieght, 0, false);
			byte[] bits = new byte[2];
			System.arraycopy(infoHeaderBytes, 14, bits, 0, 2);
			//Log.e("Image Info", "Size is :\n"+HexString.bufferToHex(bits));
			bmpInfo.bit24=false;
			if (bits[0]==(byte)0x18 && bits[1]==(byte)0x00){
				bmpInfo.bit24=true;
				//Log.e("Image Info", "Bybye 1\n");
			}else if (bits[0]==(byte)0x20 && bits[1]==(byte)0x00){
				bmpInfo.bit24=false;
				//Log.e("Image Info", "Bybye 2\n");
			}else{
				//Log.e("Image Info", "Bybye 3\n");
				is.close();
				//fIn.close();
				return null;
			}
			bmpInfo.infoBits = get2Len(bits, 0, false);
			byte[] sizeImage = new byte[4];
			System.arraycopy(infoHeaderBytes, 20, sizeImage, 0, 4);
			bmpInfo.imageSize = get4Len(sizeImage, 0, false);
			/*is.close();
			fIn.close();*/
		} catch (Exception ex) {
			//ex.printStackTrace();
			try {
				inStream.close();
			} catch (Exception e) { }
			return null;
		}
		return bmpInfo;
	}

	int iGetMax (int r,int g, int b){
		if (r>g)
			if (r>b)
				return r;
			else
				return b;
		else
		if (g>b)
			return g;
		else
			return b;

	}
	int iGetMin (int r,int g, int b){
		if (r<g)
			if (r<b)
				return r;
			else
				return b;
		else
		if (g<b)
			return g;
		else
			return b;

	}
	int iGetLuminance (int r,int g, int b){
		int ret = (iGetMax(r, g, b)+iGetMin(r, g, b))/2;
		return ret;
	}

	private int iGetShade3x3(int iAvg){
		int iAd=0,x=0;
		if(iAvg<=24){
			x=0;
		} else if (iAvg>24+iAd  && iAvg<=48+iAd){
			x=1;
		} else if (iAvg>48+iAd  && iAvg<=72+iAd){
			x=2;
		} else if (iAvg>72+iAd  && iAvg<=96+iAd){
			x=3;
		} else if (iAvg>96+iAd  && iAvg<=120+iAd){
			x=4;
		} else if (iAvg>120+iAd && iAvg<=144+iAd){
			x=5;
		} else if (iAvg>144+iAd && iAvg<=168+iAd){
			x=6;
		} else if (iAvg>168+iAd && iAvg<=216+iAd){
			x=7;
		} else if (iAvg>216+iAd && iAvg<=240+iAd){
			x=8;
		} else {// iAvg>240 && iAvg<=255
			x=9;
		}
		return x;
	}

	private int[] iGetPixelMatrix3x3(int iRd11, int iRd12, int iRd13, int iRd21, int iRd22,
									 int iRd23, int iRd31, int iRd32, int iRd33){
		//Log.d(TAG, "Px: "+iRd11+", "+iRd12+", "+iRd13+", "+iRd21+", "+iRd22+", "+iRd23+", "+iRd31+", "+iRd32+", "+iRd33);
		int iDitherMat[] = { 1,8,4, 7,6,3, 5,2,9 };
		int[] iOut = new int[3];
		int iSetBit1=0,iSetBit2=0,iSetBit3=0;
		if(iRd11<=iDitherMat[0]){//1
			iSetBit1=1;
		}
		if(iRd12<=iDitherMat[1]){//8
			iSetBit2=1;
		}
		if(iRd13<=iDitherMat[2]){//4
			iSetBit3=1;
		}
		int ix = 0;
		if (iSetBit1==1)
			ix|=0x00800000;
		if (iSetBit2==1)
			ix|=0x00800000>>1;
		if (iSetBit3==1)
			ix|=0x00800000>>2;
		iOut[0] = ix;

		ix = 0;iSetBit1=0;iSetBit2=0;iSetBit3=0;
		if(iRd21<=iDitherMat[3]){//7
			iSetBit1=1;
		}
		if(iRd22<=iDitherMat[4]){//6
			iSetBit2=1;
		}
		if(iRd23<=iDitherMat[5]){//3
			iSetBit3=1;
		}
		if (iSetBit1==1)
			ix|=0x00800000;
		if (iSetBit2==1)
			ix|=0x00800000>>1;
		if (iSetBit3==1)
			ix|=0x00800000>>2;
		iOut[1] = ix;


		ix = 0;iSetBit1=0;iSetBit2=0;iSetBit3=0;
		if(iRd31<=iDitherMat[6]){//5
			iSetBit1=1;
		}
		if(iRd32<=iDitherMat[7]){//2
			iSetBit2=1;
		}
		if(iRd33<=iDitherMat[8]){//9
			iSetBit3=1;
		}
		if (iSetBit1==1)
			ix|=0x00800000;
		if (iSetBit2==1)
			ix|=0x00800000>>1;
		if (iSetBit3==1)
			ix|=0x00800000>>2;
		iOut[2] = ix;
		return iOut;
	}


	private int convertNew(InputStream inStream,boolean blGreyScale, BmpInfo bmpInfo) {
		Log.d(TAG,">>>>>>>>> convertAndPrintNew...");
		//byte[][] rev = null;
		int bitMask = 0X80;
		int tempByte = 0x00;
		int[] pixel = new int[4];
		InputStream is1 = null;
		try {
			if(bmpInfo.infoWidth!=384){
				Log.d(TAG,"Width is not supported : "+bmpInfo.infoWidth);
				return FILE_ERROR;
			}
			is1=inStream;
			int[] bmpBuffer = new int[bmpInfo.infoWidth * bmpInfo.infoHieght / 8];
			int j = 0;
			int bytecnt=0;
			if (bmpInfo.bit24) {
				bytecnt=3;
			} else {
				bytecnt=4;
			}
			Log.d(TAG,">>>>>>>> AFTER new size.....< 2 >....!");

			byte[] buf = new byte[8192];
			int numRead = 0;
			Log.d(TAG,">>>>>>>> AFTER new size.....< 2a >....!");

			ByteBuffer buffy = new ByteBuffer();
			while ((numRead = is1.read(buf)) >= 0) {
				buffy.appendBytes(buf,numRead);
			}
			int cntIndex=0;
			byte[] bFinalData  = buffy.getBuffer();

			Log.d(TAG,">>>>>>>> AFTER new size.....< 2b >....!");
			int[] iGreyData = new int[bmpInfo.infoWidth * bmpInfo.infoHieght];
			for (int i = 0; i < bmpInfo.infoWidth * bmpInfo.infoHieght; i++) {
				//int total = (infoWidth * infoHieght);
				//Log.d(TAG,"Image Info Size: "+total);
				pixel[0] = bFinalData[cntIndex];
				pixel[1] = bFinalData[cntIndex+1];
				pixel[2] = bFinalData[cntIndex+2];
				cntIndex+=bytecnt;
				////////////////////////////grey add start/////////////////////////////////
				if (blGreyScale){
					int grayscalevalue = iGetLuminance((pixel[0]&0xFF),(pixel[1]&0xFF),(pixel[2]&0xFF));
					if (grayscalevalue<0) grayscalevalue = 0;
					if (grayscalevalue>255) grayscalevalue= 255;
					iGreyData[i] = grayscalevalue;
				} else {
					//////////////////////////// grey add end /////////////////////////////////

					if (((pixel[0] ^ 0xFF) > 100) && ((pixel[1] ^ 0xFF) > 100)
							&& ((pixel[2] ^ 0xFF) > 100)) {
						tempByte |= bitMask;
					}

					bitMask = (bitMask >> 1);
					if (bitMask == 0x00) {
						bmpBuffer[j++] = tempByte;
						tempByte = 0x00;
						bitMask = 0x80;
					}
				}
			}

			/////////////////////////////////////////////////////////////////
			if (blGreyScale){
				try {	//TODO
					int iRd11=0,iRd12=0,iRd13=0,iRd21=0,iRd22=0,iRd23=0,iRd31=0,iRd32=0,iRd33=0;
					//int iPx000=0x00000000,iPx001=0x00200000,iPx010=0x00400000,iPx011=0x00600000;
					//int iPx100=0x00800000,iPx101=0x00A00000,iPx110=0x00C00000,iPx111=0x00E00000;
					int iTmp3Bit01=0,iTmp3Bit02=0,iTmp3Bit03=0;

					int iht = bmpInfo.infoHieght;
					if (bmpInfo.infoHieght%3!=0)
						iht+=(3-bmpInfo.infoHieght%3);
					int[] bRealData = new int[iht*384];//8*48=384
					bmpBuffer = new int[iht*48];
					System.arraycopy(iGreyData, 0, bRealData, 0, iGreyData.length);
					ByteBuffer bufBig = new ByteBuffer();

					for (int i=0,kx=0;kx<bmpInfo.infoHieght/3;kx++){//,i+=3) { //with height
						ByteBuffer bufLn1 = new ByteBuffer();
						ByteBuffer bufLn2 = new ByteBuffer();
						ByteBuffer bufLn3 = new ByteBuffer();
						int bitCntByte=0,iTmp24Bit01=0,iTmp24Bit02=0,iTmp24Bit03=0;
						iTmp3Bit01=0;iTmp3Bit02=0;iTmp3Bit03=0;
						for (int jx=0;jx<bmpInfo.infoWidth;jx+=3) { //with width
							iRd11=0;iRd12=0;iRd13=0;iRd21=0;iRd22=0;iRd23=0;iRd31=0;iRd32=0;iRd33=0;
							//iRd11=bRealData[i]    ;iRd12=bRealData[i+1]  ;iRd13=bRealData[i+2];
							//iRd21=bRealData[i+384];iRd22=bRealData[i+385];iRd23=bRealData[i+386];
							//iRd31=bRealData[i+768];iRd32=bRealData[i+766];iRd33=bRealData[i+767];
							iRd11 = iGetShade3x3(bRealData[i]);		//11
							iRd12 = iGetShade3x3(bRealData[i+1]);	//12
							iRd13 = iGetShade3x3(bRealData[i+2]);	//13
							iRd21 = iGetShade3x3(bRealData[i+384]);	//21
							iRd22 = iGetShade3x3(bRealData[i+385]);	//22
							iRd23 = iGetShade3x3(bRealData[i+386]);	//23
							iRd31 = iGetShade3x3(bRealData[i+768]);	//31
							iRd32 = iGetShade3x3(bRealData[i+766]);	//32
							iRd33 = iGetShade3x3(bRealData[i+767]);	//33
							i+=3;
							int[] iPixelBits = iGetPixelMatrix3x3(iRd11, iRd12, iRd13, iRd21, iRd22, iRd23, iRd31, iRd32, iRd33);
							iTmp3Bit01=iPixelBits[0];
							iTmp3Bit02=iPixelBits[1];
							iTmp3Bit03=iPixelBits[2];

							int iShift = bitCntByte%24;

							switch(iShift){
								case 0: //iShift=0;
									iTmp24Bit01|=iTmp3Bit01>>iShift;
									iTmp24Bit02|=iTmp3Bit02>>iShift;
									iTmp24Bit03|=iTmp3Bit03>>iShift;
									break;
								case 3: //iShift=3;
									iTmp24Bit01|=iTmp3Bit01>>iShift;
									iTmp24Bit02|=iTmp3Bit02>>iShift;
									iTmp24Bit03|=iTmp3Bit03>>iShift;
									break;
								case 6: //iShift=6;
									iTmp24Bit01|=iTmp3Bit01>>iShift;
									iTmp24Bit02|=iTmp3Bit02>>iShift;
									iTmp24Bit03|=iTmp3Bit03>>iShift;
									break;
								case 9: //iShift=9;
									iTmp24Bit01|=iTmp3Bit01>>iShift;
									iTmp24Bit02|=iTmp3Bit02>>iShift;
									iTmp24Bit03|=iTmp3Bit03>>iShift;
									break;
								case 12: //iShift=12;
									iTmp24Bit01|=iTmp3Bit01>>iShift;
									iTmp24Bit02|=iTmp3Bit02>>iShift;
									iTmp24Bit03|=iTmp3Bit03>>iShift;
									break;
								case 15: //iShift=15;
									iTmp24Bit01|=iTmp3Bit01>>iShift;
									iTmp24Bit02|=iTmp3Bit02>>iShift;
									iTmp24Bit03|=iTmp3Bit03>>iShift;
									break;
								case 18: //iShift=18;
									iTmp24Bit01|=iTmp3Bit01>>iShift;
									iTmp24Bit02|=iTmp3Bit02>>iShift;
									iTmp24Bit03|=iTmp3Bit03>>iShift;
									break;
								case 21: //iShift=21;
									iTmp24Bit01|=iTmp3Bit01>>iShift;
									iTmp24Bit02|=iTmp3Bit02>>iShift;
									iTmp24Bit03|=iTmp3Bit03>>iShift;
									break;
							}
							bitCntByte+=3;

							if(bitCntByte==24){
								byte bL1 = (byte)(0x000000FF&(iTmp24Bit01>>16));
								byte bL2 = (byte)(0x000000FF&(iTmp24Bit02>>16));
								byte bL3 = (byte)(0x000000FF&(iTmp24Bit03>>16));
								bufLn1.appendByte(bL1);
								bufLn2.appendByte(bL2);
								bufLn3.appendByte(bL3);
								bL1 = (byte)(0x000000FF&(iTmp24Bit01>>8));
								bL2 = (byte)(0x000000FF&(iTmp24Bit02>>8));
								bL3 = (byte)(0x000000FF&(iTmp24Bit03>>8));
								bufLn1.appendByte(bL1);
								bufLn2.appendByte(bL2);
								bufLn3.appendByte(bL3);
								bL1 = (byte)(0x000000FF&iTmp24Bit01);
								bL2 = (byte)(0x000000FF&iTmp24Bit02);
								bL3 = (byte)(0x000000FF&iTmp24Bit03);
								bufLn1.appendByte(bL1);
								bufLn2.appendByte(bL2);
								bufLn3.appendByte(bL3);
								bitCntByte=0;
								iTmp24Bit01=0;
								iTmp24Bit02=0;
								iTmp24Bit03=0;
							}
						}
						i+=768;//i+=384;
						byte[] bLine1 = bufLn1.getBuffer();
						byte[] bLine2 = bufLn2.getBuffer();
						byte[] bLine3 = bufLn3.getBuffer();
						Log.d(TAG, "DITHER bLine1 = :"+bLine1.length);
						Log.d(TAG, "DITHER bLine2 = :"+bLine2.length);
						Log.d(TAG, "DITHER bLine3 = :"+bLine3.length);
						bufBig.appendBytes(bLine1);
						bufBig.appendBytes(bLine2);
						bufBig.appendBytes(bLine3);
					}
					byte[] bBmpData = bufBig.getBuffer();
					Log.d(TAG, "DITHER Ends.... : "+bBmpData.length+", LenExp: "+bmpBuffer.length);
					for(int i=0;i<bBmpData.length;i++){
						bmpBuffer[i] = 0x000000FF&bBmpData[i];
					}

				} catch (Exception e1) {
					Log.d(TAG, "DIETHER RELATIVE IDEA FAILED....!");
					//e1.printStackTrace();
					if (is1 != null) {
						is1.close();
					}
					return FAILURE;
				}
			}
			/////////////////////////////////////////////////////////////////
			try {
				if (is1 != null) {
					is1.close();
				}
			} catch (Exception e) {
				Log.d(TAG,"Image Pack Exception in packet formation\n");
			}
			int x=bmpBuffer.length%48;
			Log.d(TAG, "~~~~~~~~~~~~~~~~~~~~");
			Log.d(TAG, "infoWidth : "+bmpInfo.infoWidth+", infoHeight : "+bmpInfo.infoHieght);
			Log.d(TAG, "The bmpbuffer after decoding - Length: "+bmpBuffer.length+
					", is multiple of 48....> "+x);
			Log.d(TAG, "~~~~~~~~~~~~~~~~~~~~");
			if (x!=0) {
				return FILE_ERROR;
			}
			byte[] byteBmpBufer1 = new byte[bmpBuffer.length];
			byte[] byteBmpBufer = new byte[bmpBuffer.length];

			for (int i=0;i<bmpBuffer.length;i++){
				byteBmpBufer1[i] = (byte)(0x000000FF & bmpBuffer[i]);
			}
			Log.d(TAG, "The Decoded Content:\n"+HexString.bufferToHex(byteBmpBufer1));
			Log.d(TAG, "~~~~~~~~~~~~~~~~~~~~");
			int k=0;
			Log.d(TAG, "TRY....\n");
			for (int i = (bmpInfo.infoHieght - 1); i >= 0; i--){
				for (j = 0; j < (bmpInfo.infoWidth / 8); j++) {
					byteBmpBufer[k++] = byteBmpBufer1[(i * (bmpInfo.infoWidth / 8)) + j];
				}
			}

			Log.d(TAG, "\n OlD is stoppedTRY....\n");
			Log.d(TAG, "The Command is: ");
			if (byteBmpBufer.length<=9600){
				byte[] toPrint = makeCommand(ImagePrint, byteBmpBufer);
				Log.d(TAG, ">>>>>>>>>> "+HexString.bufferToHex(toPrint));
				//int f = iSendRecvCommand(toPrint);
				int f = iSendRecvCommandFast(toPrint,l5SecWaitTime*3);
				return f;
			} else {
				Log.d(TAG, "BigFile: "+byteBmpBufer.length);
				int q = byteBmpBufer.length/9600;
				int r = byteBmpBufer.length%9600;
				int lim=q;
				if (r>0)
					lim++;
				byte[][] packets = new byte[lim][];
				int totlen=byteBmpBufer.length;
				for (int i=0;i<lim;i++){
					int currlen =0;
					if (totlen>9600)
						currlen = 9600;
					else
						currlen = totlen;
					packets[i]=new byte[currlen];
					System.arraycopy(byteBmpBufer, (9600*i), packets[i], 0, currlen);
					totlen -= 9600;
					Log.d(TAG, "totlen: "+totlen);
				}
				int iRet=FAILURE;
				for (int i=0;i<lim;i++){
					byte[] toPrint = makeCommand(ImagePrint, packets[i]);
					Log.d(TAG, ">>>>>>>>>> i: "+(i+1));
					Log.d(TAG, ">>>>>>>>>> "+HexString.bufferToHex(toPrint,0,100));

					//iRet = iSendRecvCommand(toPrint);
					iRet = iSendRecvCommandFast(toPrint,l5SecWaitTime*3);
					if (iRet!=SUCCESS)
						return iRet;
				}
				return iRet;
			}
		} catch (IOException e) {
			Log.d(TAG, "Exception File IO occoured");
			e.printStackTrace();
			return FILE_ERROR;
			//return null;
		} catch (Exception ex) {
			ex.printStackTrace();
			return FAILURE;
			//return null;
		}
	}

	public enum ImageType {
		BLACK_AND_WHITE,
		GREYSCALE
	}

	public int iImagePrint(InputStream infileis, ImageType blDitherWay) {
		BmpInfo bmpInfo = getBitmapInformation(infileis);
		if (bmpInfo == null) {
			return FILE_ERROR;
		}
		Log.d(TAG, "Got the file INFO........!");
		if (blDitherWay==ImageType.BLACK_AND_WHITE)
			return convertNew(infileis,false,bmpInfo);
		else if (blDitherWay==ImageType.GREYSCALE)
			return convertNew(infileis,true,bmpInfo);
		else
			return INPUT_ERROR;
	}

	public int iLoadBmpPackets(InputStream infileis, Select select) {
		BmpInfo bmpInfo = getBitmapInformation(infileis);
		if (bmpInfo == null) {
			return FILE_ERROR;
		}
		Log.d(TAG, "Got the file INFO........!");
		byte[][] bLoadPack =null;
		String st ="07";
		switch (select){
			case Logo_1:
				bLoadPack = convertAndPrintNew(infileis, bmpInfo,(byte)0x07);
				st = "07";
				break;
			case Logo_2:
				bLoadPack = convertAndPrintNew(infileis, bmpInfo,(byte)0x08);
				st = "08";
				break;
			case Logo_3:
				bLoadPack = convertAndPrintNew(infileis, bmpInfo,(byte)0x09);
				st = "09";
				break;
			case Logo_4:
				bLoadPack = convertAndPrintNew(infileis, bmpInfo,(byte)0x0A);
				st = "0A";
				break;
			default : return INPUT_ERROR;
		}
		if (bLoadPack==null) {
			return FAILURE;
		}
		if (bLoadPack.length==10){
			byte[][] bb = bLoadPack;
			for (int ix=0;ix<bb.length;ix++){
				Setup.Loge(TAG, "1 Packet ix:"+(ix+1)+">> "+HexString.bufferToHex(bb[ix]));
			}
			int z = sendPrinterPacketsData(bLoadPack);
			//return SUCCESS;
			return z;
		} else if (bLoadPack.length<10){
			byte[][] bb = new byte[10][];
			String s = "8A92"+st+"0002FF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FF00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004";
			int i =0;
			for (;i< bLoadPack.length;i++)
				bb[i]=bLoadPack[i];
			for (;i< 10;i++)
				bb[i]=HexString.hexToBuffer(s);
			for (int ix=0;ix<bb.length;ix++){
				Setup.Loge(TAG, "2 Packet ix:"+(ix+1)+">> "+HexString.bufferToHex(bb[ix]));
			}
			int z = sendPrinterPacketsData(bb);
			//return SUCCESS;
			return z;
		} else {
			Log.e(TAG, "Large Image to Store");
			return INPUT_ERROR;
		}
		/*if (select==ImageType.BLACK_AND_WHITE)
			return convertNew(infileis,false,bmpInfo);
		else if (blDitherWay==ImageType.GREYSCALE)
			return convertNew(infileis,true,bmpInfo);
		else
			return INPUT_ERROR;*/
	}

	private byte[][] convertAndPrintNew(InputStream isStream,BmpInfo bmpInfo,byte bLogoPacket) {
		Setup.Logd(TAG,">>>>>>>>> convertAndPrintNew...");
		byte[][] rev = null;
		int bitMask = 0X80;
		int tempByte = 0x00;
		int[] pixel = new int[4];
		InputStream is1 = null;
		try {
			//File sdcard = Environment.getExternalStorageDirectory();
			/*File file1 = fileName;//new File(sdcard,fileName);
			FileInputStream fIn1 = new FileInputStream(file1);
			is1=fIn1;*/
			is1=isStream;
			int[] bmpBuffer = new int[bmpInfo.infoWidth * bmpInfo.infoHieght / 8];
			//Log.d("Image Info", "About to start printing.......!\n");
			//long skippedlenght = is1.skip(54);
			int j = 0;
			int bytecnt=0;
			if (bmpInfo.bit24){
				bytecnt=3;
			}else{
				bytecnt=4;
			}
			//  Log.e("Image Info", "Not Yet printing.......!\n" + bit24+" , "+bytecnt);
			Setup.Logd(TAG,">>>>>>>> AFTER new size.....< 2 >....!");
			byte[] buf = new byte[8192];
			int numRead = 0;
			Setup.Logd(TAG,">>>>>>>> AFTER new size.....< 2a >....!");
			ByteBuffer buffy = new ByteBuffer();
			while ((numRead = is1.read(buf)) >= 0) {
				buffy.appendBytes(buf,numRead);
			}
			int cntIndex=0;
			byte[] bFinalData  = buffy.getBuffer();
			Setup.Logd(TAG,">>>>>>>> AFTER new size.....< 2b >....!");

			for (int i = 0; i < bmpInfo.infoWidth * bmpInfo.infoHieght; i++) {
				//int total = (infoWidth * infoHieght);
				//Setup.Logd(TAG,"Image Info Size: "+total);
				pixel[0] = bFinalData[cntIndex];
				pixel[1] = bFinalData[cntIndex+1];
				pixel[2] = bFinalData[cntIndex+2];
				cntIndex+=bytecnt;
				/*for (int a=0; a<bytecnt; a++) {
					try {
						int count = is1.read();
						char c = (char) count;
						if (count == -1) {
							break;
						} else {
							pixel[a] = (int) count;
						}
					} catch (Exception e) {
						// Log.d("Image Info", "Exception"+e);
						//return PR_FAIL;
						returnCode = Setup.FAILURE;
						return null;
					}
				}*/
				//Setup.Logd(TAG,"Image Info Came up to here......\n"+bit24);
				//if (bit24){
				if (((pixel[0] ^ 0xFF) > 100) && ((pixel[1] ^ 0xFF) > 100)
						&& ((pixel[2] ^ 0xFF) > 100)) {
					tempByte |= bitMask;
				}
				/*}
				else
				{
					Setup.Logd(TAG,"Image Info Byte 1\n");
					if (((pixel[0] ^ 0xFF) > 100)
							&& ((pixel[1] ^ 0xFF) > 100) && ((pixel[2] ^ 0xFF) > 100)) {
						tempByte |= bitMask;
					}
				}*/
				bitMask = (bitMask >> 1);
				if (bitMask == 0x00) {
					bmpBuffer[j++] = tempByte;
					tempByte = 0x00;
					bitMask = 0x80;
				}
			}
			try {
				if (is1 != null) {
					is1.close();
				}
			} catch (Exception e) {
				Setup.Loge(TAG,"Image Pack Exception in packet formation\n");
				iReturnCode = FAILURE;
				return null;
			}
			int x=bmpBuffer.length%48;
			Setup.Logd(TAG, "~~~~~~~~~~~~~~~~~~~~");
			Setup.Logd(TAG, "The bmpbuffer after decoding - Length: "+bmpBuffer.length+
					", is multiple of 48....> "+x);
			Setup.Logd(TAG, "~~~~~~~~~~~~~~~~~~~~");
			rev = finalPacketFnNew(bmpBuffer,bmpInfo,bLogoPacket);

		} catch (FileNotFoundException e) {
			Setup.Logd(TAG, "Exception File IO occoured");
			iReturnCode = FILE_ERROR;
			return null;
		} catch (IOException ex) {
			ex.printStackTrace();
			iReturnCode = FAILURE;
			return null;
			// return PR_FAIL;
		}
		iReturnCode = SUCCESS;
		return rev;
		//return null;
	}
	private static final int iBmpPacketMaxSize = 240;
	private byte[][] finalPacketFnNew(int[] bmpBuffer,BmpInfo bmpInfo,byte bLogoPacket) {
		Setup.Logd(TAG,"finalPacketFnNew...");
		int asd=1;
		byte[][] oridata = new byte[2000][];
		int packetCount=0;
		int i, j, prevValue, k = 1, l = 0;
		int oneByte;
		//byte[][] ret=null;
		int[] headerdata = new int[4];
		headerdata[0] = 0X8A;
		headerdata[1] = 0XC6;
		headerdata[2] = 0X94;
		int lrcHeader = 0xD;
		int[] footerdata = new int[2];
		int lrcVal = 0;
		int count_of_00 = 0;
		int count_of_ff = 0;
		int count_of_actual = 0;
		int[] rawLine = new int[48];
		int[] tempLine = new int[48];
		int[] oldLine = new int[iBmpPacketMaxSize+50]; //75];
		int[] currentLine = new int[iBmpPacketMaxSize+50]; //75];

		int equalflag = 0;
		int current_Line_Length = 0;
		int old_Line_Length = 0;
		int dataIndex = 0;
		int[] dataBuffer = new int[iBmpPacketMaxSize+50]; //200];
		byte[] packet7 = null;
		byte[] packet8 = null;
		byte[] packet9 = null;
		// packetCount = 0;
		for (i = (bmpInfo.infoHieght - 1); i >= 0; i--) {
			byte[] packet1 = null;
			byte[] packet2 = null;
			byte[] packet3 = null;
			byte[] packet4 = null;
			byte[] packet5 = null;
			byte[] packet6 = null;

			for (j = 0; j < (bmpInfo.infoWidth / 8); j++) {
				rawLine[j] = bmpBuffer[(i * (bmpInfo.infoWidth / 8)) + j];
			}
			count_of_00 = 0;
			count_of_ff = 0;
			count_of_actual = 0;
			prevValue = 0;
			k = 1;
			for (j = 0; j < (bmpInfo.infoWidth / 8); j++) {
				oneByte = rawLine[j];
				if (oneByte == 0x00) {
					if (prevValue == 0) {
						count_of_actual = 0;
						count_of_00 = 1;
						count_of_ff = 0;
					} else if (prevValue == 1) {
						if ((count_of_actual != 0) && (count_of_00 == 2)) {
							count_of_00++;
							currentLine[k++] = '$';
							currentLine[k++] = count_of_actual;
							for (l = 0; l < count_of_actual; l++) {
								currentLine[k++] = tempLine[l];
							}
							count_of_actual = 0;
							count_of_ff = 0;
						} else {
							count_of_00++;
						}
					} else if (prevValue == 2) {
						if (count_of_actual == 0) {
							if (count_of_ff < 3) {
								count_of_00 = 1;
								tempLine[0] = 0xFF;
								tempLine[1] = 0xFF;
								count_of_actual = count_of_ff;
								count_of_ff = 0;
							} else {
								currentLine[k++] = '@';
								currentLine[k++] = count_of_ff;
								count_of_00 = 1;
								count_of_ff = 0;
							}
						} else {
							if (count_of_ff > 3) {
								currentLine[k++] = '$';
								currentLine[k++] = count_of_actual;
								for (l = 0; l < count_of_actual; l++) {
									currentLine[k++] = tempLine[l];
								}
								count_of_actual = 0;
								currentLine[k++] = '@';
								currentLine[k++] = count_of_ff;
								count_of_ff = 0;
								count_of_00 = 1;
							} else {
								for (l = 0; l < count_of_ff; l++) {
									tempLine[count_of_actual + l] = 0xFF;
								}
								count_of_actual = count_of_actual + count_of_ff;
								count_of_ff = 0;
								count_of_00 = 1;
							}
						}
					} else {
						count_of_00 = 1;
					}
					prevValue = 1;
				} else if (oneByte == 0xFF) {
					if (prevValue == 0) {
						count_of_actual = 0;
						count_of_ff = 1;
						count_of_00 = 0;
					} else if (prevValue == 2) {
						if ((count_of_actual != 0) && (count_of_ff == 2)) {
							count_of_ff++;
							currentLine[k++] = '$';
							currentLine[k++] = count_of_actual;
							for (l = 0; l < count_of_actual; l++) {
								currentLine[k++] = tempLine[l];
							}
							count_of_actual = 0;
							count_of_00 = 0;
						} else {
							count_of_ff++;
						}
					} else if (prevValue == 1) {
						if (count_of_actual == 0) {
							if (count_of_00 < 3) {
								count_of_ff = 1;
								tempLine[0] = 0x00;
								tempLine[1] = 0x00;
								count_of_actual = count_of_00;
								count_of_00 = 0;
							} else {
								currentLine[k++] = '^';
								currentLine[k++] = count_of_00;
								count_of_ff = 1;
								count_of_00 = 0;
							}
						} else {
							if (count_of_00 > 3) {
								currentLine[k++] = '$';
								currentLine[k++] = count_of_actual;
								for (l = 0; l < count_of_actual; l++) {
									currentLine[k++] = tempLine[l];
								}
								count_of_actual = 0;
								currentLine[k++] = '^';
								currentLine[k++] = count_of_00;
								count_of_00 = 0;
								count_of_ff = 1;
							} else {
								for (l = 0; l < count_of_00; l++) {
									tempLine[count_of_actual + l] = 0x00;
								}
								count_of_actual = count_of_actual + count_of_00;
								count_of_00 = 0;
								count_of_ff = 1;
							}
						}
					} else {
						count_of_ff = 1;
					}
					prevValue = 2;
				} else {
					if (prevValue == 0) {
						count_of_actual = 1;
						count_of_ff = 0;
						count_of_00 = 0;
						tempLine[0] = oneByte;
					} else if (prevValue == 1) {
						if (count_of_actual == 0) {
							if (count_of_00 < 3) {
								tempLine[0] = 0x00;
								if (count_of_00 == 2) {
									tempLine[1] = 0x00;
									tempLine[2] = oneByte;
									count_of_actual = 3;
								} else {
									tempLine[1] = oneByte;
									count_of_actual = 2;
								}
								count_of_00 = 0;
								count_of_ff = 0;
							} else {
								currentLine[k++] = '^';
								currentLine[k++] = count_of_00;
								tempLine[0] = oneByte;
								count_of_00 = 0;
								count_of_ff = 0;
								count_of_actual = 1;
							}
						} else {
							if (count_of_00 > 3) {
								currentLine[k++] = '$';
								currentLine[k++] = count_of_actual;
								for (l = 0; l < count_of_actual; l++) {
									currentLine[k++] = tempLine[l];
								}
								currentLine[k++] = '^';
								currentLine[k++] = count_of_00;
								tempLine[0] = oneByte;
								count_of_00 = 0;
								count_of_ff = 0;
								count_of_actual = 1;
							} else {
								tempLine[count_of_actual++] = 0x00;
								if (count_of_00 == 2) {
									tempLine[count_of_actual++] = 0x00;
								}
								tempLine[count_of_actual++] = oneByte;
								count_of_00 = 0;
								count_of_ff = 0;
							}
						}
					} else if (prevValue == 2) {
						if (count_of_actual == 0) {
							if (count_of_ff < 3) {
								tempLine[0] = 0xFF;
								if (count_of_ff == 2) {
									tempLine[1] = 0xFF;
									tempLine[2] = oneByte;
									count_of_actual = 3;
								} else {
									tempLine[1] = oneByte;
									count_of_actual = 2;
								}
								count_of_00 = 0;
								count_of_ff = 0;
							} else {
								currentLine[k++] = '@';
								currentLine[k++] = count_of_ff;
								tempLine[0] = oneByte;
								count_of_00 = 0;
								count_of_ff = 0;
								count_of_actual = 1;
							}
						} else {
							if (count_of_ff > 3) {
								currentLine[k++] = '$';
								currentLine[k++] = count_of_actual;
								for (l = 0; l < count_of_actual; l++) {
									currentLine[k++] = tempLine[l];
								}
								currentLine[k++] = '@';
								currentLine[k++] = count_of_ff;
								tempLine[0] = oneByte;
								count_of_00 = 0;
								count_of_ff = 0;
								count_of_actual = 1;
							} else {
								tempLine[count_of_actual++] = 0xFF;
								if (count_of_ff == 2) {
									tempLine[count_of_actual++] = 0xFF;
								}
								tempLine[count_of_actual++] = oneByte;
								count_of_00 = 0;
								count_of_ff = 0;
							}
						}
					} else if (prevValue == 3) {
						tempLine[count_of_actual++] = oneByte;
						count_of_ff = 0;
						count_of_00 = 0;
					}
					prevValue = 3;
				}
			}
			if (prevValue == 1) {
				if (count_of_actual == 0) {
					if (count_of_00 > 2) {
						currentLine[k++] = '^';
						currentLine[k++] = count_of_00;
					} else {
						currentLine[k++] = '$';
						currentLine[k++] = count_of_00;
						currentLine[k++] = 0x00;
						if (count_of_00 == 2) {
							currentLine[k++] = 0x00;
						}
					}
				} else {
					if (count_of_00 > 2) {
						currentLine[k++] = '$';
						currentLine[k++] = count_of_actual;
						for (l = 0; l < count_of_actual; l++) {
							currentLine[k++] = tempLine[l];
						}
						currentLine[k++] = '^';
						currentLine[k++] = count_of_00;
					} else {
						currentLine[k++] = '$';
						currentLine[k++] = count_of_actual + count_of_00;
						for (l = 0; l < count_of_actual; l++) {
							currentLine[k++] = tempLine[l];
						}
						currentLine[k++] = 0x00;
						if (count_of_00 == 2) {
							currentLine[k++] = 0x00;
						}
					}
				}
			} else if (prevValue == 2) {
				if (count_of_actual == 0) {
					if (count_of_ff > 2) {
						currentLine[k++] = '@';
						currentLine[k++] = count_of_ff;
					} else {
						currentLine[k++] = '$';
						currentLine[k++] = count_of_ff;
						currentLine[k++] = 0xFF;
						if (count_of_ff == 2) {
							currentLine[k++] = 0xFF;
						}
					}
				} else {
					if (count_of_ff > 2) {
						currentLine[k++] = '$';
						currentLine[k++] = count_of_actual;
						for (l = 0; l < count_of_actual; l++) {
							currentLine[k++] = tempLine[l];
						}
						currentLine[k++] = '@';
						currentLine[k++] = count_of_ff;
					} else {
						currentLine[k++] = '$';
						currentLine[k++] = count_of_actual + count_of_ff;
						for (l = 0; l < count_of_actual; l++) {
							currentLine[k++] = tempLine[l];
						}
						currentLine[k++] = 0xFF;
						if (count_of_ff == 2) {
							currentLine[k++] = 0xFF;
						}
					}
				}
			} else {
				currentLine[k++] = '$';
				currentLine[k++] = count_of_actual;
				for (l = 0; l < count_of_actual; l++) {
					currentLine[k++] = tempLine[l];
				}
			}
			currentLine[k++] = 0x25;
			current_Line_Length = k;
			/************************************************************************/
			equalflag = 0;
			if (old_Line_Length == current_Line_Length) {

				equalflag = 1;
				for (l = 1; l < current_Line_Length; l++) {
					if (oldLine[l] != currentLine[l]) {

						equalflag = 0;
						break;
					}
				}
			}
			/***************************************************************************/
			if (equalflag != 0) {
				old_Line_Length = current_Line_Length;
				if (oldLine[0] >= 60) {
					if ((dataIndex + old_Line_Length) > iBmpPacketMaxSize){ //112) {
						headerdata[3] = dataIndex;
						try {
							packet1 = new byte[headerdata.length];
							for (int w = 0; w < headerdata.length; w++) {
								packet1[w] = (byte) headerdata[w];
							}
							lrcVal = CalculateLRC(dataBuffer, dataIndex);
							lrcVal = lrcVal ^ lrcHeader;
							lrcVal = lrcVal ^ dataIndex;
							lrcVal = lrcVal ^ 0x04;

							packet2 = new byte[dataIndex];
							for (int w = 0; w < dataIndex; w++) {
								packet2[w] = (byte) dataBuffer[w];
							}
							footerdata[0] = 0x04;
							footerdata[1] = lrcVal;

							packet3 = new byte[2];
							for (int w = 0; w < 2; w++) {
								packet3[w] = (byte) footerdata[w];
							}
						} catch (Exception e) {
							// return -1;
						}
						dataIndex = 0;
					}
					System.arraycopy(oldLine, 0, dataBuffer, dataIndex,
							old_Line_Length);
					dataIndex += old_Line_Length;
					oldLine[0] = 0;
				}
				(oldLine[0])++;
			} else {
				if (old_Line_Length != 0) {
					if ((dataIndex + old_Line_Length) > iBmpPacketMaxSize){ //112) {
						headerdata[3] = dataIndex;
						try {
							packet4 = new byte[headerdata.length];
							for (int w = 0; w < headerdata.length; w++) {
								packet4[w] = (byte) headerdata[w];
							}
							lrcVal = CalculateLRC(dataBuffer, dataIndex);
							lrcVal = lrcVal ^ lrcHeader;
							lrcVal = lrcVal ^ dataIndex;
							lrcVal = lrcVal ^ 0x04;

							packet5 = new byte[dataIndex];
							for (int w = 0; w < dataIndex; w++) {
								packet5[w] = (byte) dataBuffer[w];
							}
							footerdata[0] = 0x04;
							footerdata[1] = lrcVal;

							packet6 = new byte[2];
							for (int w = 0; w < 2; w++) {
								packet6[w] = (byte) footerdata[w];
							}
						} catch (Exception e) {
							// return -1;
						}
						dataIndex = 0;
					}
					System.arraycopy(oldLine, 0, dataBuffer, dataIndex,
							old_Line_Length);
					dataIndex += old_Line_Length;
				}
				old_Line_Length = current_Line_Length;
				for (l = 0; l < current_Line_Length; l++) {
					oldLine[l] = currentLine[l];
				}
				oldLine[0] = 1;
			}
			int count = 0;
			if (packet1 != null && packet1.length > 0) {
				count = count + packet1.length;
			}
			if (packet2 != null && packet2.length > 0) {
				count = count + packet2.length;
			}
			if (packet3 != null && packet3.length > 0) {
				count = count + packet3.length;
			}
			if (packet4 != null && packet4.length > 0) {
				count = count + packet4.length;
			}
			if (packet5 != null && packet5.length > 0) {
				count = count + packet5.length;
			}
			if (packet6 != null && packet6.length > 0) {
				count = count + packet6.length;
			}
			byte[] packet = new byte[count];
			count = 0;
			if (packet1 != null && packet1.length > 0) {
				System.arraycopy(packet1, 0, packet, count, packet1.length);
				count = count + packet1.length;
			}
			if (packet2 != null && packet2.length > 0) {
				System.arraycopy(packet2, 0, packet, count, packet2.length);
				count = count + packet2.length;
			}
			if (packet3 != null && packet3.length > 0) {
				System.arraycopy(packet3, 0, packet, count, packet3.length);
				count = count + packet3.length;
			}
			if (packet4 != null && packet4.length > 0) {
				System.arraycopy(packet4, 0, packet, count, packet4.length);
				count = count + packet4.length;
			}
			if (packet5 != null && packet5.length > 0) {
				System.arraycopy(packet5, 0, packet, count, packet5.length);
				count = count + packet5.length;
			}
			if (packet6 != null && packet6.length > 0) {
				System.arraycopy(packet6, 0, packet, count, packet6.length);
				count = count + packet6.length;
			}
			if (packet != null && packet.length > 0) {
				Setup.Logd(TAG,"==================================================");
				Setup.Logd(TAG,"Packet :"+(asd++)+" Length : "+packet.length);
				Setup.Logd(TAG,"Packet Data :\n"+HexString.bufferToHex(packet,0,packet.length));
				oridata[packetCount]=new byte[packet.length];
				System.arraycopy(packet, 0, oridata[packetCount], 0, packet.length);
				packetCount++;
			}

		}

		System.arraycopy(oldLine, 0, dataBuffer, dataIndex, old_Line_Length);
		dataIndex += old_Line_Length;
		headerdata[3] = dataIndex;
		try {
			packet7 = new byte[headerdata.length];
			for (int w = 0; w < headerdata.length; w++) {
				packet7[w] = (byte) headerdata[w];
			}
			lrcVal = CalculateLRC(dataBuffer, dataIndex);
			lrcVal = lrcVal ^ lrcHeader;
			lrcVal = lrcVal ^ dataIndex;
			lrcVal = lrcVal ^ 0x04;
			packet8 = new byte[dataIndex];
			for (int w = 0; w < dataIndex; w++) {
				packet8[w] = (byte) dataBuffer[w];
			}
			footerdata[0] = 0x04;
			footerdata[1] = lrcVal;
			packet9 = new byte[2];
			for (int w = 0; w < 2; w++) {
				packet9[w] = (byte) footerdata[w];
			}
		} catch (Exception e) {
			// return -1;
		}
		int count = 0;
		if (packet7 != null && packet7.length > 0) {
			count = count + packet7.length;
		}
		if (packet8 != null && packet8.length > 0) {
			count = count + packet8.length;
		}
		if (packet9 != null && packet9.length > 0) {
			count = count + packet9.length;
		}
		byte[] packet = new byte[count];
		count = 0;
		if (packet7 != null && packet7.length > 0) {
			System.arraycopy(packet7, 0, packet, count, packet7.length);
			count = count + packet7.length;
		}
		if (packet8 != null && packet8.length > 0) {
			System.arraycopy(packet8, 0, packet, count, packet8.length);
			count = count + packet8.length;
		}
		if (packet9 != null && packet9.length > 0) {
			try{
				System.arraycopy(packet9, 0, packet, count, packet9.length);
				count = count + packet9.length;
			}catch (Exception e) {
				Setup.Logd(TAG,"Error in logo packet...");
				e.printStackTrace();
			}
		}
		if (packet != null && packet.length > 0) {
			//Setup.Logd(TAG,"==================================================1");
			//Setup.Logd(TAG,"Packet :"+(asd++)+" Length : "+packet.length);
			//Setup.Logd(TAG,"Packet Data :\n"+HexString.bufferToHex(packet,0,packet.length));
			oridata[packetCount]=new byte[packet.length];
			System.arraycopy(packet, 0, oridata[packetCount], 0, packet.length);
			packetCount++;
		}
		Setup.Logd(TAG,"Total packets : "+packetCount);
		asd=0;
		/*for (int row=0;row<packetCount;row++){
			Setup.Logd(TAG,"<><><><><><><><><><><><><><><><><><><><><><><><>");
			Setup.Logd(TAG,"NewPacket :"+(asd++)+" Length : "+oridata[row].length);
			Setup.Logd(TAG,"Packet Data :\n"+HexString.bufferToHex(oridata[row],0,oridata[row].length));
		}*/
		byte[][] dataToSend=null;
		try {
			dataToSend = new byte[packetCount][];
			Setup.Logd(TAG,"\n<><><><><><><><><><><><><><><><><><><><><><><>");
			Setup.Logd(TAG,"Total Packets: "+dataToSend.length);
			/*for (int row=0;row<packetCount;row++){
				int len = oridata[row].length-6;
				dataToSend[row] = new byte[len];
				Setup.Logd(TAG,"Row : "+row+" Curr Len : "+len);
				System.arraycopy(oridata[row], 4, dataToSend[row], 0, len);
				Setup.Logd(TAG,"Content : "+HexString.bufferToHex(dataToSend[row]));
			}*/
			for (int row=0;row<packetCount;row++){
				int len = oridata[row].length-5;
				dataToSend[row] = new byte[len];
				Setup.Logd(TAG,"Row : "+row+" Curr Len : "+len);
				System.arraycopy(oridata[row], 3, dataToSend[row], 0, len);
				Setup.Logd(TAG,"Content : "+HexString.bufferToHex(dataToSend[row]));
			}
			asd=1;
			Setup.Logd(TAG,"\n************************************************");
			//Setup.Logd(TAG,"Total packets: "+dataToSend.length);
			/*for (int row=0;row<packetCount;row++){
				dataToSend[row] = makeCommand(GraphicsPrint, dataToSend[row]);
				Setup.Logd(TAG,"NewPacket: "+(asd++)+", Length: "+dataToSend[row].length+
						"\n Aft Packet Data: "+HexString.bufferToHex(dataToSend[row],0,dataToSend[row].length));
			}*/
			int lim=0,rem=0;
			lim = dataToSend.length/2;
			rem = dataToSend.length%2;
			if(rem!=0){
				lim++;
			}
			byte[][] bOutData = new byte[lim][];
			for (int ix=0,kx=0;kx<lim;ix+=2,kx++){
				if(rem!=0&&kx==lim-1) {
					bOutData[kx] = new byte[512];//+dataToSend[ix+1].length];
					System.arraycopy(dataToSend[ix], 0, bOutData[kx], 0, dataToSend[ix].length);
					bOutData[kx][256]=(byte)0xFF;
					//System.arraycopy(dataToSend[ix+1], 0, bOutData[kx], 256, dataToSend[ix+1].length);
					//bOutData[kx] = makeCommand(GraphicsPrint, bOutData[kx]);
					bOutData[kx] = makeCommand(bLogoPacket, bOutData[kx]);
					break;
				}
				bOutData[kx] = new byte[256 + 256];//dataToSend[ix+1].length];
				System.arraycopy(dataToSend[ix], 0, bOutData[kx], 0, dataToSend[ix].length);
				System.arraycopy(dataToSend[ix+1], 0, bOutData[kx], 256, dataToSend[ix+1].length);
				//bOutData[kx] = makeCommand(GraphicsPrint, bOutData[kx]);
				bOutData[kx] = makeCommand(bLogoPacket, bOutData[kx]);
			}
			/*for (int ix=0;ix<lim;ix++){
				Setup.Loge(TAG, "Packet ix:"+(ix+1)+">> "+HexString.bufferToHex(bOutData[ix]));
			}*/
			iReturnCode = SUCCESS;
			return bOutData;
		} catch (Exception e) {
			e.printStackTrace();
			iReturnCode = FAILURE;
			return null;
		}
		/*iReturnCode = SUCCESS;
		return dataToSend;*/
	}

	private int CalculateLRC(int[] ucBuffer, int ibufflen) {
		int lrcValue = 0;
		int i;
		for (i=0; i<ibufflen; i++) {
			lrcValue = lrcValue ^ ucBuffer[i];
		}
		return lrcValue;
	}

	private static class Setup{
		public static void Logd(String s1,String s2){
			Log.d(s1, s2);
		}
		public static void Loge(String s1,String s2){
			Log.e(s1, s2);
		}
	}

	private static Timer timer;
	private TimerTask task;
	private static volatile boolean toWaitTillTimeOutForStopRecv = false;

	private TimerTask timeOutTask() {
		TimerTask timer = new TimerTask() {
			public void run() {
				Log.d(TAG, "<<<<<<<<<<<<<<<<Timeout Happened>>>>>>>>>>>>>>>");
				FalconDev.toWaitTillTimeOutForStopRecv = true;
				cancelTimer();
			}
		};
		return timer;
	}

	private void startTimer(long lTimeOut) {
		try {
			FalconDev.toWaitTillTimeOutForStopRecv = false;
			timer = new Timer();
			this.task = timeOutTask();
			Log.d(TAG, "<<<<<<<<<<<<<<<<Timer Started "+lTimeOut+" >>>>>>>>>>>>>>>");
			timer.schedule(this.task, lTimeOut);
		} catch (Exception e) { }
	}

	private void cancelTimer() {
		try {
			timer.cancel();
			this.task = null;
			timer = null;
			Log.d(TAG, "<<<<<<<<<<<<<<<<Timer canceled>>>>>>>>>>>>>>>");
		} catch (Exception e) { }
	}



}
