package com.eps.falconplus.epsfpcustomerapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eps.falconplus.server.EPSConstant;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by satish_kota on 15-05-2016.
 */
public class ImageListAdapter extends BaseAdapter {
    private Context mContext;
    String mSelBankCodes;
    ArrayList<Integer> mSelected = new ArrayList<Integer>();
    HashMap<String,String> mAccounts = new HashMap<>();
    ArrayList<String> mBC;
    ArrayList<String> alBC;
    String[] ibcs;
    String mShowType;

    public ImageListAdapter(Context c, String bankCodes, String bankaccs, String showType) {
        mContext = c;
        mSelBankCodes=bankCodes;
        ibcs = mSelBankCodes.split(",");
        String[] ibas = bankaccs.split(",");
//        bShowAll = showAll
        for(int i=0;i<ibcs.length;i++)
        {
            mAccounts.put(ibcs[i],ibas[i]);
        }

        alBC =  new ArrayList<String>(Arrays.asList(EPSConstant.mBankCodes));
        mShowType = showType;
        if(showType.equalsIgnoreCase("display") || showType.equalsIgnoreCase("popup"))
        {
            mBC = new ArrayList<String>(Arrays.asList(ibcs));
        }
        else
        {
            mBC = new ArrayList<String>(Arrays.asList(EPSConstant.mBankCodes));
            if(ibcs.length > 0)
                mBC.removeAll(Arrays.asList(ibcs));
        }
//        if(ibcs.length > 0 && showAll)
//        {
//            for(int i=0;i<ibcs.length;i++) {
//                for(int j=0;j<mBankCodes.length;j++) {
//                   if(ibcs[i].equalsIgnoreCase(mBankCodes[j])) {
//                       mSelected.add(j);
//                       break;
//                   }
//                }
//            }
//        }
    }

    public int getCount() {
        int x = 0;
        x= mBC.size();
        return x;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;

}
    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        RelativeLayout rowView;
        if(convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            rowView = (RelativeLayout) inflater.inflate(R.layout.list_item, null, true);
            rowView.setTag(0);
        }
        else
        {
            rowView = (RelativeLayout)convertView;
        }
        TextView txtTitle = (TextView) rowView.findViewById(R.id.lst_text);
        TextView txtacc = (TextView) rowView.findViewById(R.id.lst_acc);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.lst_img);

//        txtTitle.setText(web[position]);
//        imageView.setImageResource(imageId[position]);

//        if (convertView == null) {
//            // if it's not recycled, initialize some attributes
//            imageView = new ImageView(mContext);
//            //imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
//            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//            //imageView.setPadding(8, 8, 8, 8);
//        } else {
//            imageView = (ImageView) convertView;
//        }
        String bc = mBC.get(position);
        int actPos = alBC.indexOf(bc);
        txtTitle.setText(EPSConstant.mBankNames[actPos]);
        if(mShowType.equalsIgnoreCase("select") || mShowType.equals("popup")) {
            //imageView.setImageResource(mThumbIds[actPos]);
            if (!mSelected.contains(position)) {
                imageView.setImageResource(mThumbIds[actPos]);
                txtTitle.setBackgroundColor(Color.WHITE);
                txtTitle.setTextColor(Color.parseColor("#333333"));
                txtacc.setBackgroundColor(Color.WHITE);
                txtacc.setTextColor(Color.parseColor("#333333"));
                txtacc.setText("");
            } else {
                imageView.setImageResource(mSelThumbIds[actPos]);
                txtTitle.setBackgroundColor(Color.parseColor("#00a654"));
                txtTitle.setTextColor(Color.WHITE);
                txtacc.setBackgroundColor(Color.parseColor("#00a654"));
                txtacc.setTextColor(Color.WHITE);
                if(mAccounts.containsKey(bc))
                    txtacc.setText(mAccounts.get(bc));
            }
        }
        else
        {
            imageView.setImageResource(mThumbIds[actPos]);
            if(mAccounts.containsKey(bc))
                txtacc.setText(mAccounts.get(bc));

        }
        return rowView;
    }
    public void onItemSelect(AdapterView<?> parent, View v, int pos, long id) {

        if(mShowType.equalsIgnoreCase("select")) {

            final Integer position = new Integer(pos);
            final View fv = v;
            final String bc = mBC.get(position);
            final int actPos = alBC.indexOf(bc);
            if(mAccounts.containsKey(bc))
            {
                ImageView imageView = (ImageView) fv.findViewById(R.id.lst_img);
                TextView textView = (TextView) fv.findViewById(R.id.lst_text);
                TextView textView2 = (TextView) fv.findViewById(R.id.lst_acc);
                int atpos = mSelected.indexOf(position);
                mSelected.remove(atpos);
                if (mAccounts.containsKey(bc))
                    mAccounts.remove(bc);
                imageView.setImageResource(mThumbIds[actPos]);
                textView.setBackgroundColor(Color.WHITE);
                textView.setTextColor(Color.parseColor("#333333"));
                textView2.setBackgroundColor(Color.WHITE);
                textView2.setTextColor(Color.parseColor("#333333"));
                textView2.setText("");
                fv.setTag(0);
            }
            else {
                final EditText etacc = new EditText(mContext);
                etacc.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        mContext);
                alertDialogBuilder.setView(etacc);
                alertDialogBuilder.setTitle("Enter Last 4 digits of your Bank Account Number");
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        ImageView imageView = (ImageView) fv.findViewById(R.id.lst_img);
                                        TextView textView = (TextView) fv.findViewById(R.id.lst_text);
                                        TextView textView2 = (TextView) fv.findViewById(R.id.lst_acc);
                                        String acc = etacc.getText().toString();
                                        mSelected.add(position); // add item to list
                                        mAccounts.put(bc, acc);
                                        imageView.setImageResource(mSelThumbIds[actPos]);
                                        textView.setBackgroundColor(Color.parseColor("#00a654"));
                                        textView.setTextColor(Color.WHITE);
                                        textView2.setBackgroundColor(Color.parseColor("#00a654"));
                                        textView2.setTextColor(Color.WHITE);
                                        textView2.setText(acc);
                                        fv.setTag(1);
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }
        else
        {
            final int removePos = pos;
            final ListView lv = (ListView) parent;
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            if(mBC.size() > 1) {
                                String item = mBC.get(removePos);
                                if(mAccounts.containsKey(item))
                                    mAccounts.remove(item);
                                mBC.remove(removePos);
                                lv.invalidateViews();
                            }
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            if(mBC.size()==1)
                builder.setMessage(mContext.getResources().getString(R.string.Bank_Remove_last_bank)).setPositiveButton("OK", dialogClickListener).show();
            else {
                String rbc = mBC.get(removePos);
                List<String> bc = Arrays.asList(EPSConstant.mBankCodes);
                String bn = EPSConstant.mBankNames[bc.indexOf(rbc)];
                String msg = String.format(mContext.getResources().getString(R.string.Bank_Remove_Bank_item),bn);
                builder.setMessage(msg).setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        }
    }
    public String getBankCode(int pos)
    {
        return mBC.get(pos);
    }

    public String getSelectedBankCodes()
    {
        String mcode = "";
        Boolean bflag = true;
        if(mShowType.equalsIgnoreCase("select")) {
            for (Integer pos : mSelected) {
                String mbcode = mBC.get(pos);
                if (bflag)
                    mcode = mbcode;
                else
                    mcode = mcode + "," + mbcode;
                bflag = false;
            }
        }else
        {
            for (String bc : mBC) {
                String mbcode = bc;
                if (bflag)
                    mcode = mbcode;
                else
                    mcode = mcode + "," + mbcode;
                bflag = false;
            }
        }
        return mcode;
    }
    public String getSelectedBankAccounts()
    {
        String mcode = "";
        Boolean bflag = true;
        if(mShowType.equalsIgnoreCase("select")) {
            for (Integer pos : mSelected) {
                String mbcode = mBC.get(pos);
                if(mAccounts.containsKey(mbcode)) {
                    if (bflag)
                        mcode = mAccounts.get(mbcode);
                    else
                        mcode = mcode + "," + mAccounts.get(mbcode);
                }
                bflag = false;
            }
        }else
        {
            for (String bc : mBC) {
                String mbcode = bc;
                if(mAccounts.containsKey(mbcode)) {
                    if (bflag)
                        mcode = mAccounts.get(mbcode);
                    else
                        mcode = mcode + "," + mAccounts.get(mbcode);
                    bflag = false;
                }
            }
        }
        return mcode;
    }
    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.bc41,R.drawable.bc42,R.drawable.bc43,R.drawable.bc44,R.drawable.bc45,R.drawable.bc46,R.drawable.bc47,R.drawable.bc48,R.drawable.bc49,R.drawable.bc50,R.drawable.bc51,R.drawable.bc52,R.drawable.bc53,R.drawable.bc54,R.drawable.bc55,R.drawable.bc56,R.drawable.bc57,R.drawable.bc58,R.drawable.bc59,R.drawable.bc60,R.drawable.bc61,R.drawable.bc62,R.drawable.bc63,R.drawable.bc64,R.drawable.bc65,R.drawable.bc66,R.drawable.bc67,R.drawable.bc68,R.drawable.bc69,R.drawable.bc70,R.drawable.bc71,R.drawable.bc72,R.drawable.bc73,R.drawable.bc74,R.drawable.bc75,R.drawable.bc76,R.drawable.bc77,R.drawable.bc78,R.drawable.bc79,R.drawable.bc80,R.drawable.bc81,R.drawable.bc82,R.drawable.bc83,R.drawable.bc84,R.drawable.bc85,R.drawable.bc86,R.drawable.bc87,R.drawable.bc88,R.drawable.bc89,R.drawable.bc90,R.drawable.bc91
    };
    private Integer[] mSelThumbIds = {
            R.drawable.bc41sel,R.drawable.bc42sel,R.drawable.bc43sel,R.drawable.bc44sel,R.drawable.bc45sel,R.drawable.bc46sel,R.drawable.bc47sel,R.drawable.bc48sel,R.drawable.bc49sel,R.drawable.bc50sel,R.drawable.bc51sel,R.drawable.bc52sel,R.drawable.bc53sel,R.drawable.bc54sel,R.drawable.bc55sel,R.drawable.bc56sel,R.drawable.bc57sel,R.drawable.bc58sel,R.drawable.bc59sel,R.drawable.bc60sel,R.drawable.bc61sel,R.drawable.bc62sel,R.drawable.bc63sel,R.drawable.bc64sel,R.drawable.bc65sel,R.drawable.bc66sel,R.drawable.bc67sel,R.drawable.bc68sel,R.drawable.bc69sel,R.drawable.bc70sel,R.drawable.bc71sel,R.drawable.bc72sel,R.drawable.bc73sel,R.drawable.bc74sel,R.drawable.bc75sel,R.drawable.bc76sel,R.drawable.bc77sel,R.drawable.bc78sel,R.drawable.bc79sel,R.drawable.bc80sel,R.drawable.bc81sel,R.drawable.bc82sel,R.drawable.bc83sel,R.drawable.bc84sel,R.drawable.bc85sel,R.drawable.bc86sel,R.drawable.bc87sel,R.drawable.bc88sel,R.drawable.bc89sel,R.drawable.bc90sel,R.drawable.bc91sel
    };
    private Integer[] mDelThumbIds = {
            R.drawable.bc41del,R.drawable.bc42del,R.drawable.bc43del,R.drawable.bc44del,R.drawable.bc45del,R.drawable.bc46del,R.drawable.bc47del,R.drawable.bc48del,R.drawable.bc49del,R.drawable.bc50del,R.drawable.bc51del,R.drawable.bc52del,R.drawable.bc53del,R.drawable.bc54del,R.drawable.bc55del,R.drawable.bc56del,R.drawable.bc57del,R.drawable.bc58del,R.drawable.bc59del,R.drawable.bc60del,R.drawable.bc61del,R.drawable.bc62del,R.drawable.bc63del,R.drawable.bc64del,R.drawable.bc65del,R.drawable.bc66del,R.drawable.bc67del,R.drawable.bc68del,R.drawable.bc69del,R.drawable.bc70del,R.drawable.bc71del,R.drawable.bc72del,R.drawable.bc73del,R.drawable.bc74del,R.drawable.bc75del,R.drawable.bc76del,R.drawable.bc77del,R.drawable.bc78del,R.drawable.bc79del,R.drawable.bc80del,R.drawable.bc81del,R.drawable.bc82del,R.drawable.bc83del,R.drawable.bc84del,R.drawable.bc85del,R.drawable.bc86del,R.drawable.bc87del,R.drawable.bc88del,R.drawable.bc89del,R.drawable.bc90del,R.drawable.bc91del
    };
}