package com.eps.falconplus.epsfpcustomerapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.eps.falconplus.server.EPSAuthenticator;
import com.eps.falconplus.server.EPSServerCaller;
import com.eps.falconplus.server.EPSWebURL;
import com.eps.falconplus.server.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class BankSetup extends AppCompatActivity {

    TextView tvText,tvHeading;
    ImageView ivBack,ivSubmit;
    Boolean bSetup;
    String pbankCodes;
    String pbankAccs;
    SharedPreferences pref;
    //ImageAdapter adapter;
    ImageListAdapter ladapter;
    String phone;
    String mShowType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_bank_setup);
        pref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);
        pbankCodes = pref.getString("BankCodes","");
        pbankAccs = pref.getString("BankAccs","");
        phone = pref.getString("Phone","");
        mShowType = getIntent().getStringExtra("showType");

        ladapter = new ImageListAdapter(this,pbankCodes,pbankAccs,mShowType);
        //adapter = new ImageAdapter(this,pbankCodes,mShowType);

        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkSelection();
            }
        });

        ivSubmit = (ImageView) findViewById(R.id.ivSubmit);
        ivSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateBack();
            }
        });

        tvText = (TextView)findViewById(R.id.tvText);
        tvHeading = (TextView)findViewById(R.id.tvHeading);
        if(mShowType.equalsIgnoreCase("select")) {
            tvText.setText(getResources().getString(R.string.Bank_AY_Display_Message) +" " +phone);
            tvHeading.setText(getResources().getString(R.string.Bank_AY_Screen_Heading));
        }
        else {
            tvText.setText(getResources().getString(R.string.Bank_Display_Message) + " " + phone);
            tvHeading.setText(getResources().getString(R.string.Bank_Screen_Heading));
        }

        bSetup = getIntent().getBooleanExtra("SETUP",false);
        if(bSetup)
        {
            ivBack.setBackgroundResource(R.drawable.btn_exit);
        }

//        GridView gridview = (GridView) findViewById(R.id.gridView);
//        gridview.setAdapter(adapter);
//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View v,
//                                    int position, long id) {
//                adapter.onItemSelect(parent, v, position, id);
//            }
//        });

        ListView listview = (ListView) findViewById(R.id.listView);
        listview.setAdapter(ladapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                ladapter.onItemSelect(parent, v, position, id);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        checkSelection();
    }

    private void checkSelection() {
        String codes = ladapter.getSelectedBankCodes();
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        if(!bSetup)
                            overridePendingTransition( R.anim.right_in, R.anim.right_out);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        if(codes.isEmpty()||codes.equalsIgnoreCase(pbankCodes)) {
            if(bSetup)
            {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(BankSetup.this);
                builder.setMessage(getResources().getString(R.string.Bank_Exit_While_Registration)).setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
            else {
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        }
//        else if(!Utility.isNetWorkAvailable(BankSetup.this))
//        {
//            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(BankSetup.this);
//            builder.setMessage("Exiting the App without saving information to the server. No Internet found")
//                    .setPositiveButton("OK", dialogClickListener).show();
//        }
        else{
            String msg = getResources().getString(R.string.Bank_AY_Exit_without_saving);
            if (mShowType.equalsIgnoreCase("display"))
                msg=getResources().getString(R.string.Bank_Exit_without_saving);
            else if(codes.contains(","))
                msg = getResources().getString(R.string.Bank_Exit_Without_saving_multiple_banks);

            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(BankSetup.this);
            builder.setMessage(msg).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

    }
    private void validateBack() {
        //Check for atleast one bank. if not throw error
        String codes = ladapter.getSelectedBankCodes();
        String accs = ladapter.getSelectedBankAccounts();

        if (codes.isEmpty()) {
            Utility.alert(getResources().getString(R.string.Bank_Save_without_selection_head),getResources().getString(R.string.Bank_Save_without_selection)+" "+phone,BankSetup.this);
        } else if (codes.equalsIgnoreCase(pbankCodes)) {
            if (bSetup) {
                // Go to transaction
                startActivity(new Intent(BankSetup.this, Transaction.class));
            }
            finish();
            if(!bSetup)
                overridePendingTransition( R.anim.right_in, R.anim.right_out);
        } else {
            if(!pbankCodes.isEmpty() && mShowType.equalsIgnoreCase("select")) {
                codes = pbankCodes.concat(",").concat(codes);
                accs = pbankAccs.concat(",").concat(accs);
            }
            if(Utility.isNetWorkAvailable(BankSetup.this))
            {
                new AsyncTaskSaveCustomer(phone, codes,accs).execute();
            }
            else
            {
                Utility.alert(getResources().getString(R.string.Bank_Internet_Failure_error_head),getResources().getString(R.string.Bank_Internet_Failure_error),BankSetup.this);
            }
        }
    }

    class AsyncTaskSaveCustomer extends AsyncTask<String, String, String>
    {
        String mPhone;
        String mBankCodes;
        String mBankAccs;
        String mCode;
        String mStatus;
        String mMessage;
        EPSServerCaller api;
        String mCustId;
        ProgressDialog mpd = null;

        AsyncTaskSaveCustomer(String phone, String bankCodes, String bankAccs){
            this.mpd = new ProgressDialog(BankSetup.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.setMessage("Connecting to Server. Please wait...");
            this.mpd.show();
            mPhone = phone;
            mBankCodes = bankCodes;
            mBankAccs = bankAccs;
        }
        @Override
        protected String doInBackground(String... params) {
            //parseResponse("{\"status\": \"SUCCESS\",\"code\": \"SUCCESS\",\"message\": \"SUCCESS\",\"CustomerID\": \"12323523452\"}");
            try {
                HashMap<String,String> reqparams = new HashMap<>();
                HashMap<String,String> headers = new HashMap<>();
                headers.put("User-Agent",EPSWebURL.FP_USER_AGENT);
                reqparams.put("phoneNumber",mPhone);
                reqparams.put("bankCodes",mBankCodes);
                api = new EPSServerCaller(getAssets());
                String result = api.doPost(EPSWebURL.SAVE_CUSTOMER,reqparams,headers);
                int responseCode = api.getLastResponseCode();
                if (responseCode == 200) {
                    mCode = "";
                    mStatus = "";
                    mMessage="";
                    parseResponse(result);
                } else {
                    Utility.alert(getResources().getString(R.string.Bank_Internet_Failure_error_head),getResources().getString(R.string.Bank_Internet_Failure_error),BankSetup.this);
                }
            } catch (Exception ex) {
                Utility.alert(getResources().getString(R.string.Bank_Unknown_error_head),getResources().getString(R.string.Bank_Unknown_error),BankSetup.this);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (this.mpd.isShowing())
                this.mpd.dismiss();
            if(mStatus.equalsIgnoreCase("SUCCESS")){
                SharedPreferences.Editor edit = pref.edit();
                edit.putString("BankCodes",mBankCodes);
                edit.putString("BankAccs",mBankAccs);
                edit.putString("CustID",mCustId);
                edit.commit();
                if (bSetup) {
                    // Go to transaction
                    startActivity(new Intent(BankSetup.this, Transaction.class));
                }
                finish();
                if(!bSetup)
                    overridePendingTransition( R.anim.right_in, R.anim.right_out);
            }else if(mCode.equalsIgnoreCase("FORMAT_ERROR"))
                Utility.alert(getResources().getString(R.string.Bank_Data_Format_Error_head),mMessage,BankSetup.this);
            else if(mCode.equalsIgnoreCase("SAVE_ERROR"))
                Utility.alert(getResources().getString(R.string.Bank_Data_Format_Error_head),mMessage,BankSetup.this);
            else if(mCode.equalsIgnoreCase("UNKNOWN"))
                Utility.alert(getResources().getString(R.string.Bank_Unknown_error_head),getResources().getString(R.string.Bank_Unknown_error),BankSetup.this);
            else
                Utility.alert(getResources().getString(R.string.Bank_Unknown_error_head),getResources().getString(R.string.Bank_Unknown_error),BankSetup.this);
        }
        private void parseResponse(String resp)
    {
        try {
            JSONObject json = new JSONObject(resp);
            mStatus = json.optString("status");
            mCode = json.optString("code");
            mMessage = json.optString("message");
            mCustId = json.optString("customerID");
        } catch (JSONException e) {e.printStackTrace();}
    }

    }
}
