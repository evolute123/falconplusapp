package com.eps.falconplus.Bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import com.eps.falconplus.Bluetooth.FalconDev.Font;
import com.eps.falconplus.Bluetooth.FalconDev.ImageType;
import com.eps.falconplus.Bluetooth.FalconDev.Merchant_Stat;
import com.eps.falconplus.Bluetooth.FalconDev.Select;
import com.eps.falconplus.Bluetooth.FalconDev.Terminal_Stat;
import com.eps.falconplus.Bluetooth.FalconDev.Trans_Stat;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.SystemClock;
import android.util.Log;

@SuppressLint("NewApi")
public class BluetoothComm {
	/**Service UUID*/
	public final static String UUID_STR = "00001101-0000-1000-8000-00805F9B34FB";
	/**Bluetooth address code*/
	private String msMAC;
	/**Bluetooth connection status*/
	private boolean mbConectOk = false;

	/* Get Default Adapter */
	private BluetoothAdapter mBT = BluetoothAdapter.getDefaultAdapter();
	/**Bluetooth serial port connection object*/
	private BluetoothSocket mbsSocket = null;
	/** Input stream object */
	private InputStream misIn = null;
	/** Output stream object */
	private OutputStream mosOut = null;
	/**Constant: The current Android SDK version number*/
	/*private static final int SDK_VER;
	static {
		SDK_VER = Build.VERSION.SDK_INT;
	};*/

	public BluetoothComm(String sMAC){
		this.msMAC = sMAC;
	}

	public void closeConn(){
		if ( this.mbConectOk ){
			try{
				if (null != this.misIn)
					this.misIn.close();
				if (null != this.mosOut)
					this.mosOut.close();
				if (null != this.mbsSocket)
					this.mbsSocket.close();
				this.mbConectOk = false;//Mark the connection has been closed
			}catch (IOException e){
				//Any part of the error, will be forced to close socket connection
				this.misIn = null;
				this.mosOut = null;
				this.mbsSocket = null;
				this.mbConectOk = false;//Mark the connection has been closed
			}
		}
		Log.e(TAG, " Closed connection");
	}
	private static final String TAG = "Prowess BT-Comm";
	FalconDev fDev =null;
	final public boolean createConn(){
		if (! mBT.isEnabled())
			return false;
		//If a connection already exists, disconnect
		if (mbConectOk)
			this.closeConn();
		/*Start Connecting a Bluetooth device*/
		final BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(this.msMAC);
		final UUID uuidComm = UUID.fromString(UUID_STR);
		try{
			Log.d(TAG, ">>>>>>           Try 1  ................!");
			this.mbsSocket = device.createInsecureRfcommSocketToServiceRecord(uuidComm);
			/*this.mbsSocket = device.createRfcommSocketToServiceRecord(uuidComm);*/
			Thread.sleep(500);
			Log.e(TAG, ">>> Connecting 1 ");
			this.mbsSocket.connect();
			Log.e(TAG, ">>> CONNECTED SUCCESSFULLY");
			Thread.sleep(1500);
			this.mosOut = this.mbsSocket.getOutputStream();//Get global output stream object
			this.misIn = this.mbsSocket.getInputStream(); //Get global streaming input object
			this.mbConectOk = true; //Device is connected successfully
			fDev = new FalconDev(this.misIn, this.mosOut);
			Log.d(TAG, ">>> fDev Instance Created 1 ");
		}catch (Exception e){
			try {
				Thread.sleep(500);
				Log.d(TAG, ">>>>>>           Try 2  ................!");
				this.mbsSocket = device.createRfcommSocketToServiceRecord(uuidComm);
				/*this.mbsSocket = device.createInsecureRfcommSocketToServiceRecord(uuidComm);*/
				Thread.sleep(500);
				Log.e(TAG, " Connecting againg 2 ");
				this.mbsSocket.connect();
				Log.e(TAG, " Successful connection 2nd time....... ");
				Thread.sleep(1500);
				this.mosOut = this.mbsSocket.getOutputStream();//Get global output stream object
				this.misIn = this.mbsSocket.getInputStream(); //Get global streaming input object
				this.mbConectOk = true;
				fDev = new FalconDev(this.misIn, this.mosOut);
				Log.d(TAG, ">>> fDev Instance Created 2 ");
			} catch (IOException e1) {
				Log.e(TAG, " Connection Failed by trying both ways....... ");
				e1.printStackTrace();
				this.closeConn();//Disconnect
				Log.e(TAG, " Returning False");
				this.mbConectOk = false;
				return false;
			} catch (Exception ee){
				Log.e(TAG, " Connection Failed due to other reasons....... ");
				ee.printStackTrace();
				this.closeConn();//Disconnect
				Log.e(TAG, " Returning False");
				this.mbConectOk = false;
				return false;
			}
		}
		return true;
	}

	/**
	 * If the communication device has been established
	 * @return Boolean true: communication has been established / false: communication lost
	 * */
	public boolean isConnect()	{
		return this.mbConectOk;
	}

	/*public String postTransactionUpdate(String transactionID,String Status,String paymentDetails,String paymentDateTime){
		return "";
	}*/
	private int returnCode = -999;

	private boolean blTransactionInitiated = false;
	private static volatile boolean blMerchantCancelationMonitor = false;
	public boolean isTransactionInitiated() {
		return blTransactionInitiated;
	}
	public void setTransactionInitiated(boolean blTransactionInitiated) {
		this.blTransactionInitiated = blTransactionInitiated;
	}
	public boolean isMerchantCancelationMonitorActive() {
		return blMerchantCancelationMonitor;
	}
	private void setMerchantCancelationMonitor(boolean blMerchantCancelationMonitor) {
		BluetoothComm.blMerchantCancelationMonitor = blMerchantCancelationMonitor;
	}
	public void vSTOP_MerchantCancelMonitor(){
		setMerchantCancelationMonitor(false);
	}
	public static interface MercantCancelListner {
		public void onCancelled(int iResponse);
	}
	private MercantCancelListner localListener = null;
	public void vSTART_MerchantCancelMonitor(MercantCancelListner listner, long lListnerTimeOut){
		setMerchantCancelationMonitor(true);
		this.localListener = listner;
		doMonitoring(lListnerTimeOut);
	}
	@SuppressLint("DefaultLocale")
	private void doMonitoring(long lListnerTimeOut) {
		try {
			synchronized (misIn) {
				if (localListener==null || lListnerTimeOut==0) {
					Log.e(TAG, "Improper Listner is set or timer is zero : "+lListnerTimeOut);
					localListener.onCancelled(INVALID_LISTNER);
				}
				if (!isTransactionInitiated() ){
					Log.e(TAG, "Transaction is not initiated");
					localListener.onCancelled(TRANSACTION_NOT_INITIATED);
				}
				if (!isMerchantCancelationMonitorActive()){
					Log.e(TAG, "Monitor is not started");
					localListener.onCancelled(MERCHANT_CANCEL_MONITOR_NOT_STARTED);
				}
				//////////////
				byte[] bRet=null;
				try {
					byte[] bRespBytes = new byte[100];
					int iResLen=0;
					//synchronized (misIn){
					SystemClock.sleep(200);
					startTimer(lListnerTimeOut);
					Log.d(TAG, "Response Length : "+misIn.available());
					do {
						if (BluetoothComm.toWaitTillTimeOutForStopRecv){
							cancelTimer();
							//return new byte[] {(byte)0x05};
							localListener.onCancelled(FalconDev.NO_RESPONSE);
							return;
						}
						if (BluetoothComm.blMerchantCancelationMonitor==false){
							cancelTimer();
							//return new byte[] {(byte)0x08};
							localListener.onCancelled(FalconDev.MONITOR_CANCELED_BY_APP);
							return;
						}
						SystemClock.sleep(100);
					} while(misIn.available()<=0);
					cancelTimer();
					iResLen=misIn.read(bRespBytes);
					//}
					Log.d(TAG, "Response Read : "+HexString.bufferToHex(bRespBytes,0,iResLen));
					if (iResLen>1) {
						if (bRespBytes[0]!=(byte)0x8a || bRespBytes[1]!=(byte)0x92) {
							Log.e(TAG, "Invalid Data");
							localListener.onCancelled(FalconDev.FAILURE);
							return;
						}
						byte[] bOut = new byte[iResLen];
						System.arraycopy(bRespBytes, 0, bOut, 0, iResLen);
//						return bOut;
						bRet = bOut;
					} else {
//						return new byte[] {(byte)0x07};
						localListener.onCancelled(FalconDev.FAILURE);
						return;
					}
				} catch (Exception e) {
					e.printStackTrace();
//					return new byte[] {(byte)0x06};
					localListener.onCancelled(FalconDev.SEND_RECV_EXCEPTION);
					return;
				}
				if (bRet != null && bRet.length==1) {
					if (bRet[0]==0x05)
						localListener.onCancelled(FalconDev.NO_RESPONSE);
					if (bRet[0]==0x06)
						localListener.onCancelled(FalconDev.SEND_RECV_EXCEPTION);
					if (bRet[0]==0x07)
						localListener.onCancelled(FalconDev.FAILURE);
					if (bRet[0]==0x08)
						localListener.onCancelled(FalconDev.MONITOR_CANCELED_BY_APP);
					return;
				} else if (bRet.length>0) {
					String sCancelData = "8A922B03000004";
					//String sCancelData = "8A922B05000004";
					String sRet;
					try {
						sRet = HexString.bufferToHex(bRet).toUpperCase();
					} catch (Exception e) {
						sRet="Error"; e.printStackTrace();
						localListener.onCancelled(FalconDev.FAILURE);
						return;
					} //TODO
					if (sRet.equals(sCancelData)){
						localListener.onCancelled(MERCHANT_CANCELED_THE_TRANSACTION);//
					}else{
						localListener.onCancelled(FalconDev.FAILURE);
					}
					return;
				} else {
					localListener.onCancelled(FalconDev.FAILURE);
					return;
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "Monitor Exception ");
			e.printStackTrace();
			localListener.onCancelled(FalconDev.FAILURE);
			return;
		}
	}

	////////////////////////////////////////////////
	private static Timer timer;
	private TimerTask task;
	private static volatile boolean toWaitTillTimeOutForStopRecv = false;

	private TimerTask timeOutTask() {
		TimerTask timer = new TimerTask() {
			public void run() {
				Log.d(TAG, "<<<<<<<<<<<<<<<<Timeout Happened>>>>>>>>>>>>>>>");
				BluetoothComm.toWaitTillTimeOutForStopRecv = true;
				cancelTimer();
			}
		};
		return timer;
	}

	private void startTimer(long lTimeOut) {
		try {
			BluetoothComm.toWaitTillTimeOutForStopRecv = false;
			timer = new Timer();
			this.task = timeOutTask();
			Log.d(TAG, "<<<<<<<<<<<<<<<<Timer Started "+lTimeOut+" >>>>>>>>>>>>>>>");
			timer.schedule(this.task, lTimeOut);
		} catch (Exception e) { }
	}

	private void cancelTimer() {
		try {
			timer.cancel();
			this.task = null;
			timer = null;
			Log.d(TAG, "<<<<<<<<<<<<<<<<Timer canceled>>>>>>>>>>>>>>>");
		} catch (Exception e) { }
	}

	////////////////////////////////////////////////

	public static final int TRANSACTION_NOT_INITIATED = -21;
	public static final int INVALID_LISTNER = -22;
	public static final int MERCHANT_CANCEL_MONITOR_STARTED = -23;
	public static final int MERCHANT_CANCEL_MONITOR_NOT_STARTED = -24;
	public static final int MERCHANT_CANCELED_THE_TRANSACTION = -25;

	public byte[] getDummyDetails() {
		try {
			//String sDeviceDetailsForTransaction = "TerminalID;InvoiceID:Amount";
			String sDeviceDetailsForTransaction = "132456;253614:700.00";
			return sDeviceDetailsForTransaction.getBytes();
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return null;
		}
	}

	public int iDiagnose() {
		try {
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iDiagnose();
				return iRet;
				//String sDeviceDetailsForTransaction = "TerminalID:Invoice:Amount";
				//return sDeviceDetailsForTransaction.getBytes();
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	public String sGetFirmwareVersion() {
		try {
			returnCode = -999;
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive()){
					returnCode = MERCHANT_CANCEL_MONITOR_STARTED;
					return null;
				}
				byte[] bOut = fDev.bGetFirmwareNo();
				if (bOut!=null&&bOut.length>0) {
					returnCode = FalconDev.SUCCESS;
					return new String(bOut);
				} else {
					int iRet = fDev.iGetReturnCode();
					returnCode = iRet;
					return null;
				}
			} else {
				returnCode = FalconDev.FAILURE;
				return null;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			returnCode = FalconDev.FAILURE;
			return null;
		}
	}

	public byte[] bInitiateTransaction() {
		try {
			returnCode = -999;
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive()){
					returnCode = MERCHANT_CANCEL_MONITOR_STARTED;
					return null;
				}
				byte[] bOut = fDev.bInitiateTransaction();
				if (bOut!=null&&bOut.length>0) {
					returnCode = FalconDev.SUCCESS;
					return bOut;
				} else {
					int iRet = fDev.iGetReturnCode();
					returnCode = iRet;
					return null;
				}
			} else {
				returnCode = FalconDev.FAILURE;
				return null;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			returnCode = FalconDev.FAILURE;
			return null;
		}
	}

	public int iGetReturnCode(){
		return returnCode ;
	}

	public int iSendTransactionStatus(Trans_Stat tStat,byte[] bTransID) {
		try { //TODO
			if (fDev!=null) {
				if (!isTransactionInitiated())
					return TRANSACTION_NOT_INITIATED;
				/*if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;*/
				switch(tStat){
					case CANCEL_Accepted:
					case CANCEL_Rejected:
						return FalconDev.INPUT_ERROR;
					default:
						if (isMerchantCancelationMonitorActive())
							return MERCHANT_CANCEL_MONITOR_STARTED;
						break;
				}
				int iRet = fDev.iSendTransactionStatus(tStat, bTransID);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	public int iSendCancellationFeedback(Trans_Stat tStat,byte[] bTransID) {
		try {//TODO
			if (fDev!=null) {
				if (!isTransactionInitiated())
					return TRANSACTION_NOT_INITIATED;
				/*if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;*/
				switch(tStat){
					case CANCEL_Accepted:
					case CANCEL_Rejected:
						if (isMerchantCancelationMonitorActive())
							return MERCHANT_CANCEL_MONITOR_STARTED;
						break;
					default:
						return FalconDev.INPUT_ERROR;
				}
				int iRet = fDev.iSendTransactionStatus(tStat, bTransID);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	public int iSendTerminalStatus(Terminal_Stat tStat,byte[] bTerminalID) {
		try {
			if (fDev!=null) {
				if (!isTransactionInitiated())
					return TRANSACTION_NOT_INITIATED;
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;

				int iRet = fDev.iSendTerminalStatus(tStat, bTerminalID);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	public int iDisplayMerchantStatus(Merchant_Stat tStat,byte[] bMerchantID) {
		try {
			if (fDev!=null) {
				if (!isTransactionInitiated())
					return TRANSACTION_NOT_INITIATED;
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iDisplayMerchentStatus(tStat, bMerchantID);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	public int iConfigMerchantID(byte[] bMerchantID) {
		try {
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iConfigMerchantID(bMerchantID);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	public int iStoreTerminalID(byte[] bTerminalID) {
		try {
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iStoreTerminalID(bTerminalID);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	public int iChangeDisplayName(byte[] bDispName) {
		try {
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iChangeDispName(bDispName);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	public int iSetMaximumAmountLimit(byte[] bMaxAmount) {
		try {
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iSetAmountLimit(bMaxAmount);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	/**
	 * Clears the printer buffer
	 * @return {@linkplain FalconDev#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iFlushPrinterBuffer() {
		try {
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iFlushBuf();
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " + e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	/**
	 * Add data to printer buffer whit selected input font, can be used in
	 * multiple succession with different font selection.
	 * @param font Specifies the font type. The supported fonts are,<br>
	 * {@linkplain Font#LARGE_BOLD}<br>{@linkplain Font#LARGE_NORMAL}<br>
	 * {@linkplain Font#SMALL_BOLD}<br>{@linkplain Font#SMALL_NORMAL}<br>
	 * {@linkplain Font#UL_LARGE_BOLD}<br>{@linkplain Font#UL_LARGE_NORMAL}<br>
	 * {@linkplain Font#UL_SMALL_BOLD}<br>{@linkplain Font#UL_SMALL_NORMAL}<br>
	 * @param sData data to print
	 * @return {@linkplain FalconDev#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iAddPrintData(Font font, String sData) {
		try {
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iAddData(font, sData);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " + e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	/**
	 * Prints the current data from the printer buffer
	 * @return {@linkplain FalconDev#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iPrintData() {
		try {
			if (fDev!=null){
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iStartPrinting(1);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " + e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}
	/**
	 * This API prints 24bit BMP image.
	 * The width of image must be 384 pixcels and height mustbe multiples of two.
	 * @param infileis Input stream of the BMP file
	 * @param imgType type of Input image
	 * <br>{@linkplain ImageType#BLACK_AND_WHITE}
	 * <br>{@linkplain ImageType#GREYSCALE}
	 * @return {@linkplain FalconDev#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iPrintImage(InputStream infileis, ImageType imgType) {
		try {
			if (fDev!=null) {
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iImagePrint(infileis, imgType);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}
	/**
	 * This API prints the stored logos in the device.
	 * @param select to select the stored logo
	 * <br>{@linkplain Select#Logo_1}
	 * <br>{@linkplain Select#Logo_2}
	 * <br>{@linkplain Select#Logo_3}
	 * <br>{@linkplain Select#Logo_4}
	 * @return {@linkplain FalconDev#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iPrintStoredLogo(Select select) {
		try {
			if (fDev!=null) {
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				int iRet = fDev.iPrintStoredLogo(select);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}

	/**
	 * This API prints the stored logos in the device.
	 * @param isInFile Input stream of the BMP file
	 * @param select to select the stored logo
	 * <br>{@linkplain Select#Logo_1}
	 * <br>{@linkplain Select#Logo_2}
	 * <br>{@linkplain Select#Logo_3}
	 * <br>{@linkplain Select#Logo_4}
	 * @return {@linkplain FalconDev#SUCCESS} on successful operation else appropriate error codes.
	 */
	public int iStoreLogo(InputStream isInFile, Select select) {
		try {
			if (fDev!=null) {
				if (isMerchantCancelationMonitorActive())
					return MERCHANT_CANCEL_MONITOR_STARTED;
				fDev.iReset();
				int iRet = fDev.iLoadBmpPackets(isInFile, select);
				return iRet;
			} else {
				return FalconDev.FAILURE;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception Occourred " +e);
			e.printStackTrace();
			return FalconDev.FAILURE;
		}
	}
}
