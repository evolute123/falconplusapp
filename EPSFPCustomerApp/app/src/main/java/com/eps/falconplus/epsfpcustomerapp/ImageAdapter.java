package com.eps.falconplus.epsfpcustomerapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.eps.falconplus.server.EPSConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by satish_kota on 15-05-2016.
 */
public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    String mSelBankCodes;
    ArrayList<Integer> mSelected = new ArrayList<Integer>();
    ArrayList<String> mBC;
    ArrayList<String> alBC;
    String[] ibcs;
    String mShowType;

    public ImageAdapter(Context c,String bankCodes,String showType) {
        mContext = c;
        mSelBankCodes=bankCodes;
        ibcs = mSelBankCodes.split(",");
//        bShowAll = showAll
        alBC =  new ArrayList<String>(Arrays.asList(EPSConstant.mBankCodes));
        mShowType = showType;
        if(showType.equalsIgnoreCase("display") || showType.equalsIgnoreCase("popup"))
        {
            mBC = new ArrayList<String>(Arrays.asList(ibcs));
        }
        else
        {
            mBC = new ArrayList<String>(Arrays.asList(EPSConstant.mBankCodes));
            if(ibcs.length > 0)
                mBC.removeAll(Arrays.asList(ibcs));
        }
//        if(ibcs.length > 0 && showAll)
//        {
//            for(int i=0;i<ibcs.length;i++) {
//                for(int j=0;j<mBankCodes.length;j++) {
//                   if(ibcs[i].equalsIgnoreCase(mBankCodes[j])) {
//                       mSelected.add(j);
//                       break;
//                   }
//                }
//            }
//        }
    }

    public int getCount() {
        int x = 0;
        x= mBC.size();
        return x;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;

}
    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            //imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
           imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }
        String bc = mBC.get(position);
        int actPos = alBC.indexOf(bc);
        if(mShowType.equalsIgnoreCase("select") || mShowType.equals("popup")) {
            imageView.setImageResource(mThumbIds[actPos]);
        }
        else
        {
            imageView.setImageResource(mDelThumbIds[actPos]);
        }
        return imageView;
    }
    public void onItemSelect(AdapterView<?> parent, View v, int pos, long id) {

        if(mShowType.equalsIgnoreCase("select")) {
            Integer position = new Integer(pos);
            ImageView imageView = (ImageView) v;
            String bc = mBC.get(position);
            int actPos = alBC.indexOf(bc);
            if (mSelected.contains(position)) {
                mSelected.remove(position); // remove item from list
                imageView.setImageResource(mThumbIds[actPos]);
            } else {
                mSelected.add(position); // add item to list
                imageView.setImageResource(mSelThumbIds[actPos]);
            }
        }
        else
        {
            final int removePos = pos;
            final GridView gv = (GridView) parent;
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            if(mBC.size() > 1) {
                                mBC.remove(removePos);
                                gv.invalidateViews();
                            }
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            if(mBC.size()==1)
                builder.setMessage(mContext.getResources().getString(R.string.Bank_Remove_last_bank)).setPositiveButton("OK", dialogClickListener).show();
            else
                builder.setMessage(mContext.getResources().getString(R.string.Bank_Remove_Bank_item)).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();


        }
    }
    public String getBankCode(int pos)
    {
        return mBC.get(pos);
    }

    public String getSelectedBankCodes()
    {
        String mcode = "";
        Boolean bflag = true;
        if(mShowType.equalsIgnoreCase("select")) {
            for (Integer pos : mSelected) {
                String mbcode = mBC.get(pos);
                if (bflag)
                    mcode = mbcode;
                else
                    mcode = mcode + "," + mbcode;
                bflag = false;
            }
        }else
        {
            for (String bc : mBC) {
                String mbcode = bc;
                if (bflag)
                    mcode = mbcode;
                else
                    mcode = mcode + "," + mbcode;
                bflag = false;
            }
        }
        return mcode;
    }
    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.bc41,R.drawable.bc42,R.drawable.bc43,R.drawable.bc44,R.drawable.bc45,R.drawable.bc46,R.drawable.bc47,R.drawable.bc48,R.drawable.bc49,R.drawable.bc50,R.drawable.bc51,R.drawable.bc52,R.drawable.bc53,R.drawable.bc54,R.drawable.bc55,R.drawable.bc56,R.drawable.bc57,R.drawable.bc58,R.drawable.bc59,R.drawable.bc60,R.drawable.bc61,R.drawable.bc62,R.drawable.bc63,R.drawable.bc64,R.drawable.bc65,R.drawable.bc66,R.drawable.bc67,R.drawable.bc68,R.drawable.bc69,R.drawable.bc70,R.drawable.bc71,R.drawable.bc72,R.drawable.bc73,R.drawable.bc74,R.drawable.bc75,R.drawable.bc76,R.drawable.bc77,R.drawable.bc78,R.drawable.bc79,R.drawable.bc80,R.drawable.bc81,R.drawable.bc82,R.drawable.bc83,R.drawable.bc84,R.drawable.bc85,R.drawable.bc86,R.drawable.bc87,R.drawable.bc88,R.drawable.bc89,R.drawable.bc90,R.drawable.bc91
    };
    private Integer[] mSelThumbIds = {
            R.drawable.bc41sel,R.drawable.bc42sel,R.drawable.bc43sel,R.drawable.bc44sel,R.drawable.bc45sel,R.drawable.bc46sel,R.drawable.bc47sel,R.drawable.bc48sel,R.drawable.bc49sel,R.drawable.bc50sel,R.drawable.bc51sel,R.drawable.bc52sel,R.drawable.bc53sel,R.drawable.bc54sel,R.drawable.bc55sel,R.drawable.bc56sel,R.drawable.bc57sel,R.drawable.bc58sel,R.drawable.bc59sel,R.drawable.bc60sel,R.drawable.bc61sel,R.drawable.bc62sel,R.drawable.bc63sel,R.drawable.bc64sel,R.drawable.bc65sel,R.drawable.bc66sel,R.drawable.bc67sel,R.drawable.bc68sel,R.drawable.bc69sel,R.drawable.bc70sel,R.drawable.bc71sel,R.drawable.bc72sel,R.drawable.bc73sel,R.drawable.bc74sel,R.drawable.bc75sel,R.drawable.bc76sel,R.drawable.bc77sel,R.drawable.bc78sel,R.drawable.bc79sel,R.drawable.bc80sel,R.drawable.bc81sel,R.drawable.bc82sel,R.drawable.bc83sel,R.drawable.bc84sel,R.drawable.bc85sel,R.drawable.bc86sel,R.drawable.bc87sel,R.drawable.bc88sel,R.drawable.bc89sel,R.drawable.bc90sel,R.drawable.bc91sel
    };
    private Integer[] mDelThumbIds = {
            R.drawable.bc41del,R.drawable.bc42del,R.drawable.bc43del,R.drawable.bc44del,R.drawable.bc45del,R.drawable.bc46del,R.drawable.bc47del,R.drawable.bc48del,R.drawable.bc49del,R.drawable.bc50del,R.drawable.bc51del,R.drawable.bc52del,R.drawable.bc53del,R.drawable.bc54del,R.drawable.bc55del,R.drawable.bc56del,R.drawable.bc57del,R.drawable.bc58del,R.drawable.bc59del,R.drawable.bc60del,R.drawable.bc61del,R.drawable.bc62del,R.drawable.bc63del,R.drawable.bc64del,R.drawable.bc65del,R.drawable.bc66del,R.drawable.bc67del,R.drawable.bc68del,R.drawable.bc69del,R.drawable.bc70del,R.drawable.bc71del,R.drawable.bc72del,R.drawable.bc73del,R.drawable.bc74del,R.drawable.bc75del,R.drawable.bc76del,R.drawable.bc77del,R.drawable.bc78del,R.drawable.bc79del,R.drawable.bc80del,R.drawable.bc81del,R.drawable.bc82del,R.drawable.bc83del,R.drawable.bc84del,R.drawable.bc85del,R.drawable.bc86del,R.drawable.bc87del,R.drawable.bc88del,R.drawable.bc89del,R.drawable.bc90del,R.drawable.bc91del
    };
}