package com.eps.falconplus.epsfpcustomerapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eps.falconplus.server.EPSAuthenticator;
import com.eps.falconplus.server.EPSServerCaller;
import com.eps.falconplus.server.EPSWebURL;
import com.eps.falconplus.server.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class SendOTP extends AppCompatActivity implements View.OnClickListener{

    Button btnSubmit,btnResend;
    ImageView ivBack;
    EditText etOTP;
    String phone;
    ArrayList<String> otps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_send_otp);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

        btnResend = (Button) findViewById(R.id.btnResendOTP);
        btnResend.setOnClickListener(this);

        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);

        etOTP = (EditText) findViewById(R.id.etOTP);
        etOTP.setInputType(InputType.TYPE_CLASS_NUMBER);

        Bundle b = getIntent().getExtras();
        phone = b.getString("Phone");
        otps = new ArrayList<>();
        otps.add(b.getString("OTP"));
    }

    @Override
    public void onClick(View view) {
        if(view == ivBack)
        {
            finish();
            overridePendingTransition( R.anim.right_in, R.anim.right_out);
        }
        else if(view == btnSubmit)
        {
            //Validate OTP
            String otpText = etOTP.getText().toString();
            //Go to Bank Setup
            if(otps.contains(otpText))
            {
                SharedPreferences pref = getSharedPreferences("CustPreferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = pref.edit();
                edit.putString("Phone",phone);
                edit.commit();
                setResult(121);
                finish();
                Intent in = new Intent(SendOTP.this, BankSetup.class);
                in.putExtra("SETUP",true);
                in.putExtra("showType","select");
                startActivity(in);
                overridePendingTransition( R.anim.left_in, R.anim.left_out);
            }
            else
            {
                if(otpText.isEmpty())
                    Utility.alert(getResources().getString(R.string.OTP_Empty_OTP_Error_head),getResources().getString(R.string.OTP_Empty_OTP_Error),SendOTP.this);
                else
                    Utility.alert(getResources().getString(R.string.OTP_Wrong_OTP_Error_head),getResources().getString(R.string.OTP_Wrong_OTP_Error),SendOTP.this);
            }
        }
        else if(view == btnResend)
        {
            //GENERATE 6 DIGIT OTP
            Random ran = new Random();
            int nOTP= (100000 + ran.nextInt(900000));
            String strOTP = ""+nOTP;
            //SEND OTP TO SERVER
            otps.add(strOTP);
            //Toast.makeText(getApplicationContext(),strOTP,Toast.LENGTH_LONG).show();
            if(Utility.isNetWorkAvailable(SendOTP.this))
            {
                new ATSendOTP(phone,strOTP).execute();
            }
            else
            {
                Utility.alert(getResources().getString(R.string.OTP_Internet_Failure_error_head),getResources().getString(R.string.OTP_Internet_Failure_error),SendOTP.this);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.right_in, R.anim.right_out);

    }

    class ATSendOTP extends AsyncTask<String, String, String>
    {
        String mPhone;
        String mOTP;
        String mCode;
        String mStatus;
        String mMessage;
        EPSServerCaller api;
        ProgressDialog mpd = null;

        ATSendOTP(String phone, String otp){
            this.mpd = new ProgressDialog(SendOTP.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.setMessage("Connecting to Server. Please wait...");
            this.mpd.show();
            mPhone = phone;
            mOTP = otp;
        }
        @Override
        protected String doInBackground(String... params) {
           // parseResponse("{\"status\": \"SUCCESS\",\"code\": \"SUCCESS\",\"message\": \"SUCCESS\"}");
            try {
                mCode = "";
                mStatus = "";
                mMessage="";
                HashMap<String,String> reqparams = new HashMap<String,String>();
                HashMap<String,String> headers = new HashMap<String,String>();
                headers.put("User-Agent",EPSWebURL.FP_USER_AGENT);
                reqparams.put("phoneNumber",mPhone);
                reqparams.put("otpCode",mOTP);
                api = new EPSServerCaller(getAssets());
                String result = api.doPost(EPSWebURL.SEND_OTP,reqparams,headers);
                int responseCode = api.getLastResponseCode();
                if (responseCode == 200) {
                    parseResponse(result);
                } else {
                    mCode = "INTERNET_FAILED";
                }
            } catch (Throwable ex) {
               mCode = "UNKNOWN";
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (this.mpd.isShowing())
                this.mpd.dismiss();
            if(mStatus.equalsIgnoreCase("success")){
                Utility.alert(getResources().getString(R.string.OTP_Resent_head),getResources().getString(R.string.OTP_Resent),SendOTP.this);
            }else if(mCode.equalsIgnoreCase("FORMAT_ERROR"))
                Utility.alert(getResources().getString(R.string.OTP_Data_Format_Error_head),getResources().getString(R.string.OTP_Data_Format_Error),SendOTP.this);
            else if(mCode.equalsIgnoreCase("SMS_FAILED"))
                Utility.alert(getResources().getString(R.string.OTP_SMS_sending_error_head),getResources().getString(R.string.OTP_SMS_sending_error),SendOTP.this);
            else if(mCode.equalsIgnoreCase("GATEWAY_ERROR"))
                Utility.alert(getResources().getString(R.string.OTP_SMS_Gateway_error_head),getResources().getString(R.string.OTP_SMS_Gateway_error),SendOTP.this);
            else if(mCode.equalsIgnoreCase("INTERNET_FAILED"))
                Utility.alert(getResources().getString(R.string.OTP_Internet_Failure_error_head),getResources().getString(R.string.OTP_Internet_Failure_error),SendOTP.this);
            else if(mCode.equalsIgnoreCase("UNKNOWN"))
                Utility.alert(getResources().getString(R.string.OTP_Unknown_error_head),getResources().getString(R.string.OTP_Unknown_error),SendOTP.this);
            else
                Utility.alert(getResources().getString(R.string.OTP_Unknown_error_head),getResources().getString(R.string.OTP_Unknown_error),SendOTP.this);

        }
        private void parseResponse(String resp)
        {
            try {
                JSONObject json = new JSONObject(resp);
                mStatus = json.optString("status");
                mCode = json.optString("code");
                mMessage = json.optString("message");
            } catch (JSONException e) {e.printStackTrace();}
        }

    }
}
