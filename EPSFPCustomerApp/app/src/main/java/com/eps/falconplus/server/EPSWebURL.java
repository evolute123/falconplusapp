package com.eps.falconplus.server;

/**
 * Created by satish_kota on 14-05-2016.
 */
public class EPSWebURL {
    //public static final String WEBSITE = "http://eps.heurion.com/api/v1/falconplus";
    public static final String WEBSITE = "https://www.spirituspay.com/api/v1/falconplus";
    public static final String SEND_OTP= WEBSITE + "/send_otp";
    public static final String SAVE_CUSTOMER= WEBSITE + "/save_customer";
    public static final String CREATE_TRANSACTION= WEBSITE + "/create_transaction";
    public static final String GET_TRANSACTION= WEBSITE + "/get_transaction_by_id";
    public static final String UPDATE_TRANSACTION = WEBSITE + "/update_transaction_by_id";
    public static final String CLIENT_CERTIFICATE_PASSWORD = "";
    public static final String CLIENT_CERTIFICATE_NAME ="";
    public static final String CERTIFICATE_NAME="spirituspay.crt";
    public static final String FP_USER_AGENT="EPSFalconPlus/1.0";
}
